namespace Open.Aids {
    public enum EnCode{
        Ascii,
        Unicode,
        Utf7,
        Utf8
    }
}