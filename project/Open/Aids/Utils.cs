﻿namespace Open.Aids
{
    public class Utils
    {
        public static bool IsNull(object o) { return ReferenceEquals(null, o); }
        public static bool IsTesting(bool ignore = false) {
            return !ignore && IsNamespace.Running("Microsoft.VisualStudio.QualityTools.UnitTestFramework");
        }
    }
}
