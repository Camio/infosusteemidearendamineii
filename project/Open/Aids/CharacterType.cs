namespace Open.Aids {
    public enum CharacterType{
        Letter,
        NotLetter,
        Number,
        LetterOrSpace
    }
}