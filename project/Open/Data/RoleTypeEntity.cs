﻿using System;
using System.Data.Entity.Migrations;
using Open.Archetypes.RoleClasses;

namespace Open.Data {
    public class RoleTypeEntity {
        public virtual void Create(RoleType roleType) {
            using (var context = new OpenDbContext()) {
                context.RoleTypes.Add(roleType);
                context.SaveChanges();
            }
        }

        public virtual void Update(RoleType roleType) {
            using (var context = new OpenDbContext()) {
                context.RoleTypes.AddOrUpdate(roleType);
                context.SaveChanges();
            }
        }

        public virtual void Delete(RoleType roleType) {
            using (var context = new OpenDbContext()) {
                roleType.Valid.To = DateTime.Now;
                context.RoleTypes.AddOrUpdate(roleType);
                context.SaveChanges();
            }
        }
    }
}