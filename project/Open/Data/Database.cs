﻿using Open.Archetypes.PartyClasses;
using Open.Archetypes.RoleClasses;

namespace Open.Data
{
    public class Database
    {
        public static void Initilaize()
        {
            using (var context = new OpenDbContext()) {
                RoleTypes.Instance.AddRange(context.RoleTypes);
                Roles.Instance.AddRange(context.Roles);
                Parties.Instance.AddRange(context.Parties);
            }
        }

        public static void RefreshData() {
            RoleTypes.Instance.Clear();
            Roles.Instance.Clear();
            Parties.Instance.Clear();
            Initilaize();
        }
    }
}
