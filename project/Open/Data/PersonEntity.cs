﻿using Open.Archetypes.PartyClasses;
using Open.Archetypes.RoleClasses;

namespace Open.Data {
    public class PersonEntity {
        public virtual void Create(Person person, Roles roles) {
            if (person == null || roles == null) return;
            using (var context = new OpenDbContext()) {
                context.Parties.Add(person);
                context.SaveChanges();
            }

            updateRolesPerformer(roles);
        }

        public virtual void Update(Person person, Roles roles) {
            if (person == null || roles == null) return;
            updatePersonData(person);
            updateRolesPerformer(roles);
        }

        private static void updateRolesPerformer(Roles roles) {
            using (var context = new OpenDbContext()) {
                foreach (var role in roles) {
                    var entity = context.Roles.Find(role.UniqueId);
                    entity.PerformerId = role.PerformerId;
                }
                context.SaveChanges();
            }
        }

        private static void updatePersonData(Person person) {
            using (var context = new OpenDbContext()) {
                var entity = context.Parties.Find(person.UniqueId) as Person;
                entity.Name = person.Name;
                entity.Surname = person.Surname;
                entity.DateOfBirth = person.DateOfBirth;
                entity.Valid.To = person.Valid.To;
                context.SaveChanges();
            }
        }
    }
}