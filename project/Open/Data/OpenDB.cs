﻿using System.Data.Entity;
using Open.Archetypes.PartyClasses;
using Open.Archetypes.RoleClasses;

namespace Open.Data {
    internal class OpenDbContext : DbContext {
        internal OpenDbContext() : base("name=OpenDbContext") {
            Configuration.LazyLoadingEnabled = false;
        }
        
        public virtual DbSet<Party> Parties { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RoleType> RoleTypes { get; set; }
    }
}