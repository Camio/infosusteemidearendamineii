﻿using System;
using System.Data.Entity.Migrations;
using Open.Archetypes.RoleClasses;

namespace Open.Data {
    public class RoleEntity {
        public virtual void Create(Role role) {
            using (var context = new OpenDbContext()) {
                context.Roles.Add(role);
                context.SaveChanges();
            }
        }

        public virtual void Update(Role role) {
            using (var context = new OpenDbContext()) {
                context.Roles.AddOrUpdate(role);
                context.SaveChanges();
            }
        }

        public virtual void Delete(Role role) {
            using (var context = new OpenDbContext()) {
                role.Valid.To = DateTime.Now;

                context.Roles.AddOrUpdate(role);
                context.SaveChanges();
            }
        }
    }
}