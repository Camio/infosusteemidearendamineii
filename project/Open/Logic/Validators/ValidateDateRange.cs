﻿using System;
using System.ComponentModel.DataAnnotations;
namespace Open.Logic.Validators {
    public class ValidateDateRange : ValidationAttribute {
        public DateTime FirstDate => DateTime.Now - TimeSpan.FromDays(365 * 200);
        public DateTime SecondDate => DateTime.Now;

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext) {
            var date = (DateTime) value;
            if (date <= FirstDate) { return new ValidationResult("Are you older that 200 years?"); }
            if (date > SecondDate) { return new ValidationResult("Date of birth can't be in future."); }
            return ValidationResult.Success;
        }
    }
}