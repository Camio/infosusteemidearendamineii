﻿using System;
using Open.Archetypes.ContactClasses;
namespace Open.Logic.ContactClasses {
    public class ContactViewModel {
        public ContactViewModel(AddressUsage a) {
            Type = a.Address.GetType().Name;
            UniqueId = a.UniqueId;
            Content = a.Address.Content;
            Usages = a.Uses.Content;
            ValidFrom = dateToString(a.Valid.From, DateTime.MinValue);
            ValidTo = dateToString(a.Valid.To, DateTime.MaxValue);
        }
        private static string dateToString(DateTime d, DateTime noValue) {
            return d.Equals(noValue) ? string.Empty : d.ToShortDateString();
        }
        public string UniqueId { get; set; }
        public string Type { get; set; }
        public string Content { get; set; }
        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }
        public string Usages { get; set; }
        public string UsagesColor { get; set; }
    }
}
