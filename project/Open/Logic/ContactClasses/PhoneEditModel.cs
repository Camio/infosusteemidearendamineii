﻿using System;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.ContactClasses;
namespace Open.Logic.ContactClasses {
    public class PhoneEditModel {
        public PhoneEditModel() { }
        public PhoneEditModel(AddressUsage a) {
            var adr = a?.Address as Phone;
            if (adr == null) return;
            UniqueId = a.UniqueId;
            Usages = a.Uses.Content;
            CountryCode = adr.CountryCode;
            DirectDialingPrefix = adr.DirectDialingPrefix;
            AreaCode = adr.AreaCode;
            Number = adr.Number;
            Extension = adr.Extension;
            ValidFrom = a.Valid.From;
            ValidTo = a.Valid.To;
        }
        public uint Number { get; set; }
        public uint Extension { get; set; }
        public uint AreaCode { get; set; }
        public uint DirectDialingPrefix { get; set; }
        public uint CountryCode { get; set; }
        public string UniqueId { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public string Usages { get; set; }
        public void Update(AddressUsage adr) {
            var phone = adr.Address as Phone;
            phone.AreaCode = AreaCode;
            phone.CountryCode = CountryCode;
            phone.DirectDialingPrefix = DirectDialingPrefix;
            phone.Number = Number;
            phone.Extension = Extension;
            adr.Valid.From = ValidFrom;
            adr.Valid.To = ValidTo;
            adr.Uses.Clear();
            adr.Uses.AddRange(Strings.ToList(Usages));
        }
    }
}
