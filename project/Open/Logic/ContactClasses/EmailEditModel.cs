﻿using System;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.ContactClasses;
namespace Open.Logic.ContactClasses
{
    public class EmailEditModel
    {
        public EmailEditModel() { }
        public EmailEditModel(AddressUsage a)
        {
            var adr = a?.Address as EmailAddress;
            if (adr == null) return;
            UniqueId = a.UniqueId;
            Usages = a.Uses.Content;
            Email = adr.Address;
            ValidFrom = a.Valid.From;
            ValidTo = a.Valid.To;
        }
        public string Email { get; set; }
        public string UniqueId { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public string Usages { get; set; }
        public void Update(AddressUsage adr) {
            var mail = adr.Address as EmailAddress;
            mail.Address = Email;
            adr.Valid.From = ValidFrom;
            adr.Valid.To = ValidTo;
            adr.Uses.Clear();
            adr.Uses.AddRange(Strings.ToList(Usages));
        }
    }
}
