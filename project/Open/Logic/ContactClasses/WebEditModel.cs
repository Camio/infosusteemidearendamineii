﻿using System;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.ContactClasses;
namespace Open.Logic.ContactClasses
{
    public class WebEditModel
    {
        public WebEditModel() { }
        public WebEditModel(AddressUsage a)
        {
            var adr = a?.Address as WebAddress;
            if (adr == null) return;
            UniqueId = a.UniqueId;
            Usages = a.Uses.Content;
            Url = adr.Url;
            ValidFrom = a.Valid.From;
            ValidTo = a.Valid.To;
        }
        public string Url { get; set; }
        public string UniqueId { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public string Usages { get; set; }
        public void Update(AddressUsage adr) {
            var web = adr.Address as WebAddress;
            web.Url = Url;
            adr.Valid.From = ValidFrom;
            adr.Valid.To = ValidTo;
            adr.Uses.Clear();
            adr.Uses.AddRange(Strings.ToList(Usages));
        }
    }
}
