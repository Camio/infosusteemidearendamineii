﻿using System;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.ContactClasses;
namespace Open.Logic.ContactClasses {
    public class AddressEditModel {
        public AddressEditModel() { }
        public AddressEditModel(AddressUsage a) {
            var adr = a?.Address as Address;
            if (adr == null) return;
            UniqueId = a.UniqueId;
            Address = adr.AddressLines.Content;
            ZipCode = adr.ZipCode;
            Location = adr.Location;
            Region = adr.Region;
            Country = adr.Country.Name;
            Usages = a.Uses.Content;
            ValidFrom = a.Valid.From;
            ValidTo = a.Valid.To;
        }
        public string Country { get; set; }
        public string Region { get; set; }
        public string Location { get; set; }
        public string UniqueId { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public string Usages { get; set; }
        public void Update(AddressUsage adr) {
            var a = adr.Address as Address;
            a.AddressLines.Clear();
            a.AddressLines.AddRange(Strings.ToList(Address, ','));
            a.Country.Name = Country;
            a.Location = Location;
            a.Region = Region;
            a.ZipCode = ZipCode;
            adr.Valid.From = ValidFrom;
            adr.Valid.To = ValidTo;
            adr.Uses.Clear();
            adr.Uses.AddRange(Strings.ToList(Usages));
        }
    }
}
