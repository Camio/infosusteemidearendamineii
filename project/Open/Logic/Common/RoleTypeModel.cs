﻿using System.ComponentModel.DataAnnotations;
using Open.Archetypes.RoleClasses;

namespace Open.Logic.Common
{
    public class RoleTypeModel
    {
        public virtual RoleType FindById(string id)
        {
            var roleType = RoleTypes.Find(id);
            if (roleType == null)
                throw new ValidationException($"Role type with id '{id}' not found.");
            return roleType;
        }
    }
}
