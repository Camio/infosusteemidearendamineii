﻿using System.ComponentModel.DataAnnotations;
using Open.Archetypes.PartyClasses;

namespace Open.Logic.Common
{
    public class PersonModel
    {
        public virtual Person FindById(string id)
        {
            var party = Parties.Find(id) as Person;
            if (party == null)
                throw new ValidationException($"Party with id '{id}' not found.");
            return party;
        }
    }
}
