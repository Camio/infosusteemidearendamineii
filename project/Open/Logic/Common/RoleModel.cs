﻿using System.ComponentModel.DataAnnotations;
using Open.Archetypes.RoleClasses;

namespace Open.Logic.Common
{
    public class RoleModel
    {
        public virtual Role FindById(string id)
        {
            var role = Roles.Find(id);
            if (role == null) throw new ValidationException($"Role with id '{id}' not found.");
            return role;
        }
    }
}
