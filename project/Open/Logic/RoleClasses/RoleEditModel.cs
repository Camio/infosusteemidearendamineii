﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Open.Archetypes.RoleClasses;
using Open.Data;
using Open.Logic.Common;

namespace Open.Logic.RoleClasses {
    public sealed class RoleEditModel : RoleModel {
        private readonly RoleEntity roleEntity;

        public RoleEditModel() : this(null) { }

        public RoleEditModel(RoleEntity roleEntity = null) {
            if(this.roleEntity == null)
                this.roleEntity = roleEntity ?? new RoleEntity();
        }

        public RoleEditModel(Role role, RoleEntity roleEntity = null) : this(roleEntity) { initialize(role); }

        public RoleEditModel(string id, RoleEntity roleEntity = null) : this(roleEntity)
        {
            var role = FindById(id);
            initialize(role);
        }

        [DisplayName("Id")]
        public string UniqueId { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
        public string Description { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Type { get; set; }

        public SelectList ActiveRoleTypes {
            get {
                return new SelectList(
                    RoleTypes.Instance.Where(type => type.IsActive || Type == type.UniqueId)
                             .ToList(), "UniqueId", "Name", Type);
            }
        }

        public void Create() {
            var role = new Role(Name, Description, Type);
            roleEntity.Create(role);
            Roles.Instance.Add(role);
        }

        public void Update(string id) {
            var role = FindById(id);
            role.Name = Name;
            role.Description = Description;
            role.TypeId = Type;
            roleEntity.Update(role);
        }

        public void Delete() {
            var role = FindById(UniqueId);
            roleEntity.Delete(role);
        }

        private void initialize(Role role) {
            UniqueId = role.UniqueId;
            Name = role.Name;
            Description = role.Description;
            Type = role.TypeId;
        }
    }
}