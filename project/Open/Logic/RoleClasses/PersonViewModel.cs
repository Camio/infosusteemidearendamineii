﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Open.Archetypes.PartyClasses;
using Open.Archetypes.RoleClasses;
using Open.Logic.Common;

namespace Open.Logic.RoleClasses {
    public sealed class PersonViewModel : PersonModel {
        public PersonViewModel() { }

        public PersonViewModel(Party party) {
            var person = party as Person;
            initialize(person);
        }

        public PersonViewModel(string id) {
            var person = FindById(id);
            initialize(person);
        }

        [DisplayName("Id")]
        public string UniqueId { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public Roles Roles { get; set; }

        [DisplayName("Date of birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateOfBirth { get; set; }

        [DisplayName("Registration date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime RegistrationDate { get; set; }

        private void initialize(Person person) {
            UniqueId = person.UniqueId;
            Name = person.Name;
            Surname = person.Surname;
            DateOfBirth = person.DateOfBirth;
            if(Roles==null) Roles = new Roles();
            Roles.AddRange(person.Roles);
            RegistrationDate = person.Valid.From;
        }
    }
}