﻿using System.ComponentModel;
using Open.Archetypes.RoleClasses;
using Open.Logic.Common;

namespace Open.Logic.RoleClasses {
    public sealed class RoleTypeViewModel : RoleTypeModel {
        public RoleTypeViewModel() { }

        public RoleTypeViewModel(RoleType roleType) { initialize(roleType); }

        public RoleTypeViewModel(string id) {
            var roleType = FindById(id);
            initialize(roleType);
        }

        [DisplayName("Id")]
        public string UniqueId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        [DisplayName("Is active?")]
        public bool IsActive { get; set; }

        private void initialize(RoleType roleType) {
            UniqueId = roleType.UniqueId;
            Name = roleType.Name;
            Description = roleType.Description;
            IsActive = roleType.IsActive;
        }
    }
}