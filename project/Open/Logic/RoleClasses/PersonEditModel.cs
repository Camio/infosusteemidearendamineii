﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Open.Archetypes.RoleClasses;
using System.ComponentModel.DataAnnotations;
using Open.Archetypes.PartyClasses;
using Open.Data;
using Open.Logic.Common;
using Open.Logic.Validators;

namespace Open.Logic.RoleClasses {
    public sealed class PersonEditModel : PersonModel {
        private Roles partyRoles;
        private readonly PersonEntity personEntity;

        public PersonEditModel() : this(null) { }

        public PersonEditModel(PersonEntity personEntity = null) {
            if (this.personEntity == null) this.personEntity = personEntity ?? new PersonEntity();
        }

        public PersonEditModel(Party party, PersonEntity personEntity = null) : this(personEntity) {
            initialize(party as Person);
        }

        public PersonEditModel(string id, PersonEntity personEntity = null) : this(personEntity) {
            var person = FindById(id);
            initialize(person);
        }

        [DisplayName("Id")]
        public string UniqueId { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Surname { get; set; }

        [DisplayName("Roles")]
        [Required]
        public  Roles PartyRoles {
            get { return partyRoles ?? (partyRoles = new Roles()); }
            set { partyRoles = value; }
        }

        [DisplayName("Date of birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [ValidateDateRange]
        public DateTime DateOfBirth { get; set; }

        [DisplayName("Available roles")]
        public MultiSelectList AvailableRoles {
            get {
                var activeRoles = Roles
                    .Instance.Where(role => role.IsActive && string.IsNullOrEmpty(role.PerformerId))
                    .ToList();
                return new MultiSelectList(activeRoles, "UniqueId", "Name", PartyRoles);
            }
        }
        public List<string> NewRoles { get; set; }

        public void Create() {
            var person = new Person(Name, Surname, DateOfBirth);
            var roles = getRolesWithPerformertId(person.UniqueId);
            personEntity.Create(person, roles);
            Parties.Instance.Add(person);
        }

        public void Update(string id) {
            var person = FindById(id);
            person.Name = Name;
            person.Surname = Surname;
            person.DateOfBirth = DateOfBirth;
            var roles = getRolesWithPerformertId(id);
            personEntity.Update(person, roles);
        }

        public void Delete() {
            var person = FindById(UniqueId);
            var roles = person.Roles;
            person.Valid.To = DateTime.Now;
            foreach (var role in person.Roles) { role.PerformerId = ""; }            
            personEntity.Update(person, roles);
        }

        private Roles getRolesWithPerformertId(string id) {
            var roles = new Roles();
            if (NewRoles == null) return roles;
            foreach (var newRole in NewRoles)
            {
                var role = Roles.Find(newRole);
                role.PerformerId = id;
                roles.Add(role);
            }
            return roles;
        }

        private void initialize(Person person) {
            UniqueId = person.UniqueId;
            Name = person.Name;
            Surname = person.Surname;
            DateOfBirth = person.DateOfBirth;
            PartyRoles = person.Roles;
        }
    }
}