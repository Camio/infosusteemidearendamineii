﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Open.Archetypes.RoleClasses;
using Open.Data;
using Open.Logic.Common;

namespace Open.Logic.RoleClasses {
    public sealed class RoleTypeEditModel : RoleTypeModel {
        private readonly RoleTypeEntity roleTypeEntity;

        public RoleTypeEditModel() : this(null) { }

        public RoleTypeEditModel(RoleTypeEntity roleTypeEntity = null) {
            if (this.roleTypeEntity == null)
                this.roleTypeEntity = roleTypeEntity ?? new RoleTypeEntity();
        }

        public RoleTypeEditModel(RoleType roleType,
            RoleTypeEntity roleTypeEntity = null) : this(roleTypeEntity) {
            initialize(roleType);
        }

        public RoleTypeEditModel(string id, RoleTypeEntity roleTypeEntity = null) : this(roleTypeEntity) {
            var roleType = FindById(id);
            initialize(roleType);
        }

        [DisplayName("Id")]
        public string UniqueId { get; set; }

        public string Description { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [DisplayName("Is active?")]
        public bool IsActive { get; set; }

        public void Create() {
            var roleType = new RoleType(Name, Description);
            roleTypeEntity.Create(roleType);
            RoleTypes.Instance.Add(roleType);
        }

        public void Update(string id) {
            var roleType = FindById(id);
            roleType.Name = Name;
            roleType.Description = Description;
            roleTypeEntity.Update(roleType);
        }

        public void Delete() {
            var roleType = FindById(UniqueId);
            roleTypeEntity.Delete(roleType);
        }

        private void initialize(RoleType roleType) {
            UniqueId = roleType.UniqueId;
            Name = roleType.Name;
            Description = roleType.Description;
            IsActive = roleType.IsActive;
        }
    }
}