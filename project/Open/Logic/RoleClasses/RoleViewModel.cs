﻿using System.ComponentModel;
using Open.Archetypes.RoleClasses;
using Open.Logic.Common;

namespace Open.Logic.RoleClasses {
    public sealed class RoleViewModel : RoleModel{
        public RoleViewModel() { }

        public RoleViewModel(Role role) { initialize(role); }

        public RoleViewModel(string id) {
            var role = FindById(id);
            initialize(role);
        }

        [DisplayName("Id")]
        public string UniqueId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }

        [DisplayName("Is active?")]
        public bool IsActive { get; set; }

        private void initialize(Role role)
        {
            UniqueId = role.UniqueId;
            Name = role.Name;
            Description = role.Description;
            Type = role.Type.Name;
            IsActive = role.IsActive;
        }
    }
}