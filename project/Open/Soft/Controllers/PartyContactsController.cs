﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using Open.Archetypes.ContactClasses;
using Open.Logic.ContactClasses;
namespace Open.Soft.Controllers {
    public class PartyContactsController : Controller {
        private static bool isCreated;
        public ActionResult Index() {
            if (!isCreated) AddressUsages.Instance.AddRange(AddressUsages.Random(40, 100));
            isCreated = true;
            var m = new List<ContactViewModel>();
            foreach (var e in AddressUsages.Instance) {
                var x = new ContactViewModel(e);
                m.Add(x);
            }
            return View(m);
        }
        public ActionResult Details(string id) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var address = AddressUsages.Instance.Find(x => x.IsThisUniqueId(id));
            if (address == null) return HttpNotFound();
            return View(address);
        }
        public ActionResult CreateAddress() {
            var a = new AddressEditModel();
            return View("CreateAddress", a);
        }
        public ActionResult CreateEmail() {
            var e = new EmailEditModel();
            return View("CreateEmail", e);
        }
        public ActionResult CreateWeb() {
            var w = new WebEditModel();
            return View("CreateWeb", w);
        }
        public ActionResult CreatePhone() {
            var p = new PhoneEditModel();
            return View("CreatePhone", p);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePhone(
        [Bind(
            Include =
                "UniqueID,Number,Extension,AreaCode,DirectDialingPrefix,CountryCode,ValidFrom, ValidTo,Usages"
        )] PhoneEditModel p)
        {
            if (!ModelState.IsValid) return View("CreatePhone", p);
            var adr = new AddressUsage {Address = new Phone()};
            p.Update(adr);
            AddressUsages.Instance.Add(adr);
            return RedirectToAction("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateWeb(
            [Bind(Include = "UniqueID,Url,ValidFrom,ValidTo,Usages")] WebEditModel w)
        {
            if (!ModelState.IsValid) return View("CreateWeb", w);
            var adr = new AddressUsage { Address = new WebAddress() };
            w.Update(adr);
            AddressUsages.Instance.Add(adr);
            return RedirectToAction("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmail(
            [Bind(Include = "UniqueID, Email, Usages, ValidFrom, ValidTo")] EmailEditModel e)
        {
            if (!ModelState.IsValid) return View("CreateEmail", e);
            var adr = new AddressUsage { Address = new EmailAddress() };
            e.Update(adr);
            AddressUsages.Instance.Add(adr);
            return RedirectToAction("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAddress(
        [Bind(
            Include = "UniqueId,Address,ZipCode,Location,Region,Country,Usages,ValidFrom,ValidTo")] AddressEditModel a)
        {
            if (!ModelState.IsValid) return View("CreateAddress", a);
            var adr = new AddressUsage { Address = new Address() };
            a.Update(adr);
            AddressUsages.Instance.Add(adr);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(string id) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var adr = AddressUsages.Instance.Find(x => x.IsThisUniqueId(id));
            if (adr == null) return HttpNotFound();
            if (adr.Address is Phone) return View("EditPhone", new PhoneEditModel(adr));
            if (adr.Address is WebAddress) return View("EditWeb", new WebEditModel(adr));
            if (adr.Address is Address) return View("EditAddress", new AddressEditModel(adr));
            if (adr.Address is EmailAddress) return View("EditEmail", new EmailEditModel(adr));
            return View("Index");
        }

        // POST: Movies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost] [ValidateAntiForgeryToken] public ActionResult EditPhone(
        [Bind(
            Include =
                "UniqueID,Number,Extension,AreaCode,DirectDialingPrefix,CountryCode,ValidFrom, ValidTo,Usages"
        )] PhoneEditModel p) {
            if (!ModelState.IsValid) return View("EditPhone", p);
            var adr = AddressUsages.Instance.Find(x => x.IsThisUniqueId(p.UniqueId));
            if (adr == null) return HttpNotFound();
            p.Update(adr);
            return RedirectToAction("Index");
        }
        [HttpPost] [ValidateAntiForgeryToken] public ActionResult EditWeb(
            [Bind(Include = "UniqueID,Url,ValidFrom,ValidTo,Usages")] WebEditModel w) {
            if (!ModelState.IsValid) return View("EditWeb", w);
            var adr = AddressUsages.Instance.Find(x => x.IsThisUniqueId(w.UniqueId));
            if (adr == null) return HttpNotFound();
            w.Update(adr);
            return RedirectToAction("Index");
        }
        [HttpPost] [ValidateAntiForgeryToken] public ActionResult EditEmail(
            [Bind(Include = "UniqueID, Email, Usages, ValidFrom, ValidTo")] EmailEditModel e) {
            if (!ModelState.IsValid) return View("EditEmail", e);
            var adr = AddressUsages.Instance.Find(x => x.IsThisUniqueId(e.UniqueId));
            if (adr == null) return HttpNotFound();
            e.Update(adr);
            return RedirectToAction("Index");
        }
        [HttpPost] [ValidateAntiForgeryToken] public ActionResult EditAddress(
        [Bind(
            Include = "UniqueId,Address,ZipCode,Location,Region,Country,Usages,ValidFrom,ValidTo")] AddressEditModel a) {
            if (!ModelState.IsValid) return View("EditAddress", a);
            var adr = AddressUsages.Instance.Find(x => x.IsThisUniqueId(a.UniqueId));
            if (adr == null) return HttpNotFound();
            a.Update(adr);
            return RedirectToAction("Index");
        }
        public ActionResult Delete(string id) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var address = AddressUsages.Instance.Find(x => x.IsThisUniqueId(id));
            if (address == null) return HttpNotFound();
            return View(new ContactViewModel(address));
        }
        [HttpPost] public ActionResult Delete(string id, FormCollection collection) {
            try {
                var address = AddressUsages.Instance.Find(x => x.IsThisUniqueId(id));
                address.Valid.To = DateTime.Now;
            } catch {
                ;
            }
            return RedirectToAction("Index");
        }
    }
}
