﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using Open.Archetypes.PartyClasses;
using Open.Archetypes.RoleClasses;
using Open.Data;
using Open.Logic.RoleClasses;

namespace Open.Soft.Controllers {
    [HandleError]
    public class PeopleController : Controller {
        public ActionResult Index() {
            var personViewModels = Parties.Instance.Where(person => person.IsActive)
                                          .Select(person => new PersonViewModel(person))
                                          .ToList();
            return View(personViewModels);
        }

        public ActionResult Create() { return View(new PersonEditModel()); }

        public ActionResult Refresh() {
            Database.RefreshData();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "Name, Surname, DateOfBirth, NewRoles")]
            PersonEditModel personEditModel) {
            if (!ModelState.IsValid) return View("Create", personEditModel);
            personEditModel.Create();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(string id) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View(new PersonEditModel(id));
        }

        public ActionResult Details(string id) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View(new PersonViewModel(id));
        }

        public ActionResult Delete(string id) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View(new PersonViewModel(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id, FormCollection collection) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var personEditModel = new PersonEditModel(id);
            personEditModel.Delete();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "UniqueId, Name, Surname, DateOfBirth, NewRoles")]
            PersonEditModel personEditModel) {
            if (!ModelState.IsValid) return View("Edit", personEditModel);
            personEditModel.Update(personEditModel.UniqueId);
            return RedirectToAction("Index");
        }

        public ActionResult RemoveRole(string id, string roleid) {
            if (id == null || roleid == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var role = Roles.Find(roleid);
            if (role.IsPerformerId(id)) role.PerformerId = "";
            new RoleEntity().Update(role);
            return RedirectToAction("Edit", new { id });
        }
    }
}