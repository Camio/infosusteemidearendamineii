﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using Open.Archetypes.RoleClasses;
using Open.Logic.RoleClasses;

namespace Open.Soft.Controllers {
    [HandleError]
    public class RolesController : Controller {
        public ActionResult Index() {
            var roleViewModels = (from role in Roles.Instance
                                  where role.IsActive
                                  select new RoleViewModel(role)).ToList();
            return View(roleViewModels);
        }

        public ActionResult Create() { return View(new RoleEditModel()); }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "Name,Description,Type")] RoleEditModel roleEditModel) {
            if (!ModelState.IsValid) return View("Create", roleEditModel);
            roleEditModel.Create();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(string id) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View(new RoleEditModel(id));
        }
        public ActionResult Details(string id) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View(new RoleViewModel(id));
        }

        public ActionResult Delete(string id) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View(new RoleViewModel(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id, FormCollection collection) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var roleEditModel = new RoleEditModel(id);
            roleEditModel.Delete();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "UniqueId,Name,Description,Type")] RoleEditModel roleEditModel) {
            if (!ModelState.IsValid) return View("Edit", roleEditModel);
            roleEditModel.Update(roleEditModel.UniqueId);
            return RedirectToAction("Index");
        }
    }
}