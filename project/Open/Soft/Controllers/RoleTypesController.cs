﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using Open.Archetypes.RoleClasses;
using Open.Logic.RoleClasses;

namespace Open.Soft.Controllers {
    [HandleError]
    public class RoleTypesController : Controller {

        public ActionResult Index() {
            var roleTypeViewModels = (from roleType in RoleTypes.Instance
                                      where roleType.IsActive
                                      select new RoleTypeViewModel(roleType)).ToList();
            return View(roleTypeViewModels);
        }

        public ActionResult Create() { return View(new RoleTypeEditModel()); }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "Name, Description")] RoleTypeEditModel roleTypeEditModel) {
            if (!ModelState.IsValid) return View("Create", roleTypeEditModel);
            roleTypeEditModel.Create();
            return RedirectToAction("Index");
        }

        public ActionResult Details(string id) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View(new RoleTypeViewModel(id));
        }

        public ActionResult Edit(string id) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View(new RoleTypeEditModel(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "UniqueId, Name, Description")] RoleTypeEditModel roleTypeEditModel) {
            if (!ModelState.IsValid) return View("Edit", roleTypeEditModel);
            roleTypeEditModel.Update(roleTypeEditModel.UniqueId);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(string id) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View(new RoleTypeViewModel(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id, FormCollection collection) {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var roleTypeEditModel = new RoleTypeEditModel(id);
            roleTypeEditModel.Delete();
            return RedirectToAction("Index");
        }
    }
}