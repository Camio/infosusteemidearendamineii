﻿using Microsoft.Owin;
using Open.Data;
using Open.Soft;
using Owin;
[assembly: OwinStartup(typeof(Startup))]
namespace Open.Soft
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            Database.Initilaize();
        }
    }
}
