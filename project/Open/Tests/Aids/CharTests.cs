﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
namespace Open.Tests.Aids {
    [TestClass] public class CharTests : ClassTests<Char> {
        [TestMethod] public void IsDotTest() {
            for (var i = 0; i < 10; i++) {
                var c = GetRandom.Char();
                if (c == '.') Assert.IsTrue(Char.IsDot('.'));
                else Assert.IsFalse(Char.IsDot(','));
            }
        }
        [TestMethod] public void SpaceTest() { Assert.AreEqual(' ', Char.Space); }
        [TestMethod] public void EolTest() { Assert.AreEqual('\n', Char.Eol); }
        [TestMethod] public void ZeroTest() { Assert.AreEqual('0', Char.Zero); }
        [TestMethod] public void UnderscoreTest() { Assert.AreEqual('_', Char.Underscore); }
        [TestMethod] public void TabTest() { Assert.AreEqual('\t', Char.Tab); }
        [TestMethod] public void CommaTest() { Assert.AreEqual(',', Char.Comma); }
        [TestMethod] public void IsLetterTest() {
            Assert.IsTrue(Char.IsLetter(GetRandom.Char('a', 'z')));
            Assert.IsTrue(Char.IsLetter(GetRandom.Char('A', 'Z')));
            Assert.IsFalse(Char.IsLetter(GetRandom.Char('0', '9')));
            Assert.IsFalse(Char.IsLetter(GetRandom.Char(max: '/')));
        }
        [TestMethod] public void IsNumberTest() {
            Assert.IsFalse(Char.IsNumber(GetRandom.Char('a', 'z')));
            Assert.IsFalse(Char.IsNumber(GetRandom.Char('A', 'Z')));
            Assert.IsTrue(Char.IsNumber(GetRandom.Char('0', '9')));
            Assert.IsFalse(Char.IsNumber(GetRandom.Char(max: '/')));
        }
    }
}
