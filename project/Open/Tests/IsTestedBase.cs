﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
namespace Open.Tests {
    public class IsTestedBase {
        protected const string isNotTestedMessage = "<{0}> is not tested";
        private const string noClassesInAssemblyMessage = "No classes in assembly {0}";
        private const string noClassesInNamespaceMessage = "No classes in namespace {0}";
        private const string testAssembly = "Open.Tests";
        private const string assembly = "Open";
        private const char genericsChar = '`';
        private const char internalClass = '+';
        private const string displayClass = "<>";
        private const string shell32 = "Shell32.";
        protected List<string> list;
        private void removeSurrogateClasses() {
            list.RemoveAll(o => o.Contains(shell32));
            list.RemoveAll(o => o.Contains(internalClass));
        }
        protected void getMembers(string assemblyName, string namesapecName = null) {
            var l = loadClasses(assemblyName);
            removeInterfacs(l);
            removeNotInNamespace(l, namesapecName);
            removeSurrogateClasses();
            //RemoveAbstractClasses(l);
            removeTested();
        }
        private void removeNotInNamespace(List<Type> classes, string namesapecName) {
            list = classes.Select(o => o.FullName).ToList();
            if (string.IsNullOrEmpty(namesapecName)) return;
            if (!string.IsNullOrEmpty(namesapecName))
                list.RemoveAll(o => !o.StartsWith(namesapecName));
            if (list.Count > 0) return;
            Assert.Inconclusive(noClassesInNamespaceMessage, namesapecName);
        }
        private void removeTested() {
            var tests = GetSolution.ClassesNames(testAssembly);
            for (var i = list.Count; i > 0; i--) {
                var member = list[i - 1];
                member = member.Substring(assembly.Length);
                var idx = member.IndexOf(genericsChar);
                if (idx > 0)
                    member = member.Substring(0, idx);
                member = member + "Tests";
                var t = tests.Find(o => o.EndsWith(member));
                if (!ReferenceEquals(null, t))
                    list.RemoveAt(i - 1);
                if (member.Contains(displayClass))
                    list.RemoveAt(i - 1);
            }
        }
        protected static List<Type> loadClasses(string assemblyName) {
            var l = GetSolution.Classes(assemblyName);
            if (l.Count == 0)
                Assert.Inconclusive(noClassesInAssemblyMessage, assemblyName);
            return l;
        }
        protected static void removeAbstractClasses(IList<Type> types) {
            for (var i = types.Count; i > 0; i--) {
                var e = types[i - 1];
                if (!e.IsAbstract) continue;
                types.Remove(e);
            }
        }
        protected static void removeInterfacs(IList<Type> types) {
            for (var i = types.Count; i > 0; i--) {
                var e = types[i - 1];
                if (!e.IsInterface) continue;
                types.Remove(e);
            }
        }
        [TestInitialize] public void CreateList() { list = new List<string>(); }
        [TestCleanup] public void AreNotTested() {
            if (list.Count == 0)
                return;
            Assert.Inconclusive(isNotTestedMessage, list[0]);
        }
        protected virtual string Namespace(string name) { return $"{assembly}.{name}"; }
    }
}
