﻿//using System.Collections.Generic;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Open.Archetypes.CommonClasses;
//using Open.Archetypes.MoneyClasses;
//using Open.Tests.Archetypes.CommonClassses;
//namespace Open.Tests.Archetypes.MoneyClasses
//{
//    [TestClass]
//    public class CurrenciesBookTests: AnyBookGenericTests
//    {
//        [TestInitialize]
//        public override void TestInitialize()
//        {
//            TestItemType = typeof(CurrenciesBook);
//            CurrenciesBook.Instance.Clear();
//        }
//        [TestMethod]
//        public void AppendRatesTest() { }
//        [TestMethod] public void RatesTest() {
//            Assert.IsNotNull(CurrenciesBook.Instance.NominalRates);
//        }
//        [TestMethod]
//        public void GetRateTest()
//        {
//            var c = Currency.Random();
//            var dt = RandomValue.DateTime();
//            var d = RandomValue.Decimal();
//            Assert.AreEqual(0, CurrenciesBook.Instance.GetNominalRate(c, dt));
//            CurrenciesBook.Instance.SetNominalRate(c, dt, d);
//            Assert.AreEqual(d, CurrenciesBook.Instance.GetNominalRate(c, dt));
//            CurrenciesBook.Instance.Clear();
//            const decimal rate1 = 1.2345M;
//            const decimal rate2 = 5.4321M;
//            const decimal rate3 = 9.8765M;
//            const decimal rate4 = 5.6789M;
//            CurrenciesBook.Instance.SetNominalRate(c, dt, rate1);
//            CurrenciesBook.Instance.SetNominalRate(c, dt.AddDays(1), rate2);
//            CurrenciesBook.Instance.SetNominalRate(c, dt.AddDays(-300), rate3);
//            CurrenciesBook.Instance.SetNominalRate(c, dt.AddDays(300), rate4);
//            Assert.AreEqual(rate1, CurrenciesBook.Instance.GetNominalRate(c, dt));
//            Assert.AreEqual(rate2, CurrenciesBook.Instance.GetNominalRate(c, dt.AddDays(1)));
//            Assert.AreEqual(rate1, CurrenciesBook.Instance.GetNominalRate(c, dt.AddDays(-1)));
//            Assert.AreEqual(rate1, CurrenciesBook.Instance.GetNominalRate(c, dt.AddDays(-149)));
//            Assert.AreEqual(rate3, CurrenciesBook.Instance.GetNominalRate(c, dt.AddDays(-151)));
//            Assert.AreEqual(rate2, CurrenciesBook.Instance.GetNominalRate(c, dt.AddDays(2)));
//            Assert.AreEqual(rate2, CurrenciesBook.Instance.GetNominalRate(c, dt.AddDays(149)));
//            Assert.AreEqual(rate4, CurrenciesBook.Instance.GetNominalRate(c, dt.AddDays(151)));
//        }
//        [TestMethod]
//        public void SetRateTest() { GetRateTest(); }
//        [TestMethod] public void AddTest() {
//            CurrenciesBook.Instance.Clear();
//            Assert.AreEqual(0, CurrenciesBook.Instance.Items.Count);
//            var many = Currencies.Random();
//            foreach (var m in many) CurrenciesBook.Instance.Add(m);
//            Assert.AreEqual(many.Count, CurrenciesBook.Instance.Items.Count);
//        }
//        [TestMethod]
//        public void AddUsingParametersTest()
//        {
//            AddTest();
//            var m1 = Currency.Random();
//            var m = Currency.Empty;
//            Assert.AreEqual(m, CurrenciesBook.Instance.Find(m1.Name));
//            CurrenciesBook.Instance.Add(m1.Id, m1.Name, m1.Code,
//                m1.Description, m1.Valid, m1.LastModified, m1.ModifiedBy, m1.IsReadOnly);
//            Assert.AreEqual(m1, CurrenciesBook.Instance.Find(m1.Name));
//        }
//        [TestMethod] public void ChangeTest() {
//            CurrenciesBook.Instance.Clear();
//            Assert.AreEqual(0, CurrenciesBook.Instance.Items.Count);
//            AddSimilar();
//            Assert.IsTrue(CurrenciesBook.Instance.Items.Count < randomUnits.Count);
//            var l = CurrenciesBook.Instance.FindAll(commonIdentifier);
//            Assert.AreEqual(4, l.Count);
//            foreach (var m in randomUnits) CurrenciesBook.Instance.Change(m);
//            Assert.AreEqual(randomUnits.Count, CurrenciesBook.Instance.Items.Count);
//            l = CurrenciesBook.Instance.FindAll(commonIdentifier);
//            Assert.AreEqual(0, l.Count);
//        }
//        [TestMethod]
//        public void ChangeUsingAttributesTest()
//        {
//            CurrenciesBook.Instance.Clear();
//            Assert.AreEqual(0, CurrenciesBook.Instance.Items.Count);
//            randomUnits = new Currencies();
//            var count = RandomValue.UInt8(10, 20);
//            for (var i = 0; i < count; i++) randomUnits.Add(Currency.Random());
//            AddSimilar(randomUnits);
//            Assert.AreEqual(4, CurrenciesBook.Instance.Items.Count);
//            var l = CurrenciesBook.Instance.FindAll(commonIdentifier);
//            Assert.AreEqual(4, l.Count);
//            foreach (var m in randomUnits) CurrenciesBook.Instance.Change(m.Id, 
//                m.Name, m.Code, m.Description, m.Valid, m.LastModified, m.ModifiedBy,
//                m.IsReadOnly);
//            Assert.AreEqual(count, CurrenciesBook.Instance.Items.Count);
//            l = CurrenciesBook.Instance.FindAll(commonIdentifier);
//            Assert.AreEqual(0, l.Count);
//        }
//        [TestMethod]
//        public void FindTest()
//        {
//            AddTest();
//            var m1 = Currency.Random();
//            var m = Currency.Empty;
//            Assert.AreEqual(m, CurrenciesBook.Instance.Find(m1));
//            CurrenciesBook.Instance.Add(m1);
//            Assert.AreEqual(m1, CurrenciesBook.Instance.Find(m1));
//        }
//        [TestMethod] public void FindAllTest() {
//            AddTest();
//            var s = RandomValue.String(50);
//            var m1 = Currency.Random();
//            var m2 = Currency.Random();
//            var m3 = Currency.Random();
//            var m4 = Currency.Random();
//            m1.Name = m1.Name + s;
//            m2.Name = m2.Name + s;
//            m3.Name = m3.Name + s;
//            m4.Name = m4.Name + s;
//            var l = CurrenciesBook.Instance.FindAll(o => o.Name.Contains(s));
//            Assert.AreEqual(0, l.Count);
//            CurrenciesBook.Instance.Add(m1);
//            CurrenciesBook.Instance.Add(m2);
//            CurrenciesBook.Instance.Add(m3);
//            CurrenciesBook.Instance.Add(m4);
//            l = CurrenciesBook.Instance.FindAll(o => o.Name.Contains(s));
//            Assert.AreEqual(4, l.Count);
//            Assert.AreNotEqual(-1, l.IndexOf(m1));
//            Assert.AreNotEqual(-1, l.IndexOf(m2));
//            Assert.AreNotEqual(-1, l.IndexOf(m3));
//            Assert.AreNotEqual(-1, l.IndexOf(m4));
//        }
//        [TestMethod]
//        public void FindAllByAttributesTest()
//        {
//            AddTest();
//            var s = RandomValue.String();
//            var m1 = Currency.Random();
//            var m2 = Currency.Random();
//            var m3 = Currency.Random();
//            var m = Currency.Random();
//            m1.Name = s;
//            m2.Code = s;
//            m3.Description = s;
//            var m4 = new Currency(s, m.Name, m.Code, m.Description, m.Valid, m.LastModified,
//                m.ModifiedBy, m.IsReadOnly);
//            var l = CurrenciesBook.Instance.FindAll(s);
//            Assert.AreEqual(0, l.Count);
//            CurrenciesBook.Instance.Add(m1);
//            CurrenciesBook.Instance.Add(m2);
//            CurrenciesBook.Instance.Add(m3);
//            CurrenciesBook.Instance.Add(m4);
//            l = CurrenciesBook.Instance.FindAll(s);
//            Assert.AreEqual(4, l.Count);
//            Assert.AreNotEqual(-1, l.IndexOf(m1));
//            Assert.AreNotEqual(-1, l.IndexOf(m2));
//            Assert.AreNotEqual(-1, l.IndexOf(m3));
//            Assert.AreNotEqual(-1, l.IndexOf(m4));
//        }
//        [TestMethod]
//        public void ClearTest()
//        {
//            AddTest();
//            Assert.AreNotEqual(0, CurrenciesBook.Instance.Items.Count);
//            CurrenciesBook.Instance.Clear();
//            Assert.AreEqual(0, CurrenciesBook.Instance.Items.Count);
//        }
//        [TestMethod]
//        public void ItemsTest() { AddTest(); }
//        [TestMethod]
//        public void FindByIdTest()
//        {
//            AddTest();
//            var m1 = Currency.Random();
//            var m = Currency.Empty;
//            Assert.AreEqual(m, CurrenciesBook.Instance.Find(m1.Id));
//            CurrenciesBook.Instance.Add(m1);
//            Assert.AreEqual(m1, CurrenciesBook.Instance.Find(m1.Id));
//        }
//        [TestMethod]
//        public void FindByNameTest()
//        {
//            AddTest();
//            var m1 = Currency.Random();
//            var m = Currency.Empty;
//            Assert.AreEqual(m, CurrenciesBook.Instance.Find(m1.Name));
//            CurrenciesBook.Instance.Add(m1);
//            Assert.AreEqual(m1, CurrenciesBook.Instance.Find(m1.Name));
//        }
//        [TestMethod]
//        public void FindByCodeTest()
//        {
//            AddTest();
//            var m1 = Currency.Random();
//            var m = Currency.Empty;
//            Assert.AreEqual(m, CurrenciesBook.Instance.Find(m1.Code));
//            CurrenciesBook.Instance.Add(m1);
//            Assert.AreEqual(m1, CurrenciesBook.Instance.Find(m1.Code));
//        }
//        [TestMethod]
//        public void FindByDescriptionTest()
//        {
//            AddTest();
//            var m1 = Currency.Random();
//            var m = Currency.Empty;
//            Assert.AreEqual(m, CurrenciesBook.Instance.Find(m1.Description));
//            CurrenciesBook.Instance.Add(m1);
//            Assert.AreEqual(m1, CurrenciesBook.Instance.Find(m1.Description));
//        }
//        [TestMethod]
//        public void FindByManyTest()
//        {
//            AddTest();
//            var m1 = Currency.Random();
//            var m = Currency.Empty;
//            Assert.AreEqual(m, CurrenciesBook.Instance.Find(m1.Id, m1.Name, m1.Code, m1.Description));
//            CurrenciesBook.Instance.Add(m1);
//            Assert.AreEqual(m1, CurrenciesBook.Instance.Find(m1.Id, m1.Name, m1.Code, m1.Description));
//        }

//        private string commonIdentifier;
//        private Currencies randomUnits;
//        private void AddSimilar()
//        {
//            randomUnits = Currencies.Random();
//            AddSimilar(randomUnits);
//        }
//        private void AddSimilar(IEnumerable<Currency> units)
//        {
//            if (ReferenceEquals(null, units)) return;
//            var s = RandomValue.String();
//            commonIdentifier = s;
//            var i = 0;
//            foreach (var m in units)
//            {
//                switch (i % 4)
//                {
//                    case 0:
//                        AddChangeName(m, s);
//                        break;
//                    case 1:
//                        AddChangeCode(m, s);
//                        break;
//                    case 2:
//                        AddChangeDescription(m, s);
//                        break;
//                    default:
//                        AddChangeId(m, s);
//                        break;
//                }
//                i++;
//            }
//        }
//        private static void AddChangeId(Currency u, string s)
//        {
//            if (ReferenceEquals(null, u)) return;
//            s = StringValue.Trim(s);
//            var unit = new Currency(s, u.Name, u.Code, u.Description, 
//                 u.Valid, u.LastModified, u.ModifiedBy, u.IsReadOnly);
//            CurrenciesBook.Instance.Change(unit);
//        }
//        private static void AddChangeDescription(Currency u, string s)
//        {
//            if (ReferenceEquals(null, u)) return;
//            s = StringValue.Trim(s);
//            var unit = new Currency(u.Id,  u.Name, u.Code, s, u.Valid, u.LastModified, u.ModifiedBy, u.IsReadOnly);
//            CurrenciesBook.Instance.Change(unit);
//        }
//        private static void AddChangeCode(Currency u, string s)
//        {
//            if (ReferenceEquals(null, u)) return;
//            s = StringValue.Trim(s);
//            var unit = new Currency(u.Id, u.Name, s, u.Description, u.Valid, u.LastModified, u.ModifiedBy, u.IsReadOnly);
//            CurrenciesBook.Instance.Change(unit);
//        }
//        private static void AddChangeName(Currency u, string s)
//        {
//            if (ReferenceEquals(null, u)) return;
//            s = StringValue.Trim(s);
//            var unit = new Currency(u.Id, s, u.Code, u.Description, u.Valid, u.LastModified, u.ModifiedBy, u.IsReadOnly);
//            CurrenciesBook.Instance.Change(unit);
//        }
//    }
//}
