﻿//using System;
//using System.Collections.Generic;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Open.Archetypes.CommonClasses;
//using Open.Archetypes.MoneyClasses;
//using Open.Tests.Archetypes.CommonClassses;
//namespace Open.Tests.Archetypes.MoneyClasses {
//    [TestClass] public class CurrenciesTests : ManyGenericTests<Currency> {
//        protected override Currency GetRandomItem() { return Currency.Random(); }
//        protected override byte GetRandomCount() { return RandomValue.UInt8(10, 20); }
//        protected override Many<Currency> GenerateRandomItem(DateTime lastModified, string modifiedBy) {
//            var l = new List<Currency>();
//            var c = GetRandomCount();
//            for (var i = 0; i < c; i++) l.Add(GetRandomItem());
//            return new Currencies(l, false, false, lastModified, modifiedBy);
//        }
//        [TestInitialize] public override void TestInitialize() {
//            TestItemType = typeof (Currencies);
//            var l = new List<Currency>();
//            var c = GetRandomCount();
//            for (var i = 0; i < c; i++) l.Add(GetRandomItem());
//            TestItem = new Currencies(l);
//            TestList = new List<Currency>();
//        }
//        [TestMethod] public override void ParseTest() {
//            var s = XmlSerialization.Serialize(TestItem);
//            var a = Any.Parse<Currencies>(s);
//            Assert.IsTrue(TestItem.Equals(a));
//        }
//        [TestMethod] public override void TryParseTest() {
//            var s = XmlSerialization.Serialize(TestItem);
//            Currencies a;
//            var b = Any.TryParse(s, out a);
//            Assert.IsTrue(b);
//            Assert.IsTrue(a.Equals(TestItem));
//        }
//        [TestMethod] public override void ReadXmlTest() {
//            var p1 = GenerateRandomItem();
//            var s = XmlSerialization.Serialize(p1);
//            var p2 = XmlSerialization.Deserialize<Currencies>(s);
//            Assert.IsTrue(p1.Equals(p2));
//        }
//        [TestMethod] public void RandomTest() {
//            RandomTest(Currencies.Random(), Currencies.Random(), typeof (Currencies));
//        }
//    }
//}
