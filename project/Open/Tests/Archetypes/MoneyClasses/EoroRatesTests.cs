﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.MoneyClasses;
namespace Open.Tests.Archetypes.MoneyClasses
{
    [TestClass]
    public class EoroRatesTests : BaseTests
    {
        [TestInitialize] public override void TestInitialize() {
            base.TestInitialize();
            type = typeof(EuroRates);
        }
        [TestMethod]
        public void FromXmlTest()
        {
            ExchangeRates.Instance.Clear();
            EuroRates.ReadXml(EuroRates.example);
            Assert.AreEqual(31, ExchangeRates.Instance.Count);
            var fromDate = new DateTime(2015, 03, 05);
            var toDate = new DateTime(2015, 03, 05, 23, 59, 59);
            Action<decimal, string> test = (rate, currency) =>
            {
                var r = ExchangeRates.Find(currency);
                Assert.IsNotNull(r);
                Assert.AreEqual(rate, r.Rate);
                Assert.AreEqual(fromDate.ToString("G"), r.Valid.From.ToString("G"));
                Assert.AreEqual(toDate.ToString("G"), r.Valid.To.ToString("G"));
            };
            test(1.1069M, "USD");
            test(133.10M, "JPY");
            test(1.9558M, "BGN");
            test(27.422M, "CZK");
            test(7.4542M, "DKK");
        }
    }
}
