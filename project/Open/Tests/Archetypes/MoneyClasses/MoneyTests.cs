﻿//using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Open.Archetypes.CommonClasses;
//using Open.Archetypes.MoneyClasses;
//using Open.Archetypes.RuleClasses;
//namespace Open.Tests.Archetypes.MoneyClasses
//{
//    [TestClass]
//    public class MoneyTests: MoneyGenericTests<Money> {
//        private Currency eek;
//        private Currency usd;
//        private Currency eur;
//        private DateTime dt1;
//        private DateTime dt2;
//        [TestInitialize]
//        public override void TestInitialize()
//        {
//            base.TestInitialize();
//            CurrenciesBook.Instance.Clear();
//            CurrencyUtils.RegisterCurrencies();
//            usd = CurrenciesBook.Instance.Find("USD");
//            eur = CurrenciesBook.Instance.Find("EUR");
//            eek = CurrenciesBook.Instance.Find("EEK");
//            Assert.IsNotNull(usd);
//            Assert.IsNotNull(eur);
//            Assert.IsNotNull(eek);
//            dt1 = DateTime.Now.AddDays(-1);
//            dt2 = DateTime.Now.AddDays(-2);
//            CurrenciesBook.Instance.SetNominalRate(eur, dt1, 1M);
//            CurrenciesBook.Instance.SetNominalRate(usd, dt1, 2M);
//            CurrenciesBook.Instance.SetNominalRate(usd, dt2, 3M);
//            CurrenciesBook.Instance.SetNominalRate(eek, dt1, 10M);
//            CurrenciesBook.Instance.SetNominalRate(eek, dt2, 15M);
//        }
//        protected override Money GenerateRandomItem(DateTime lastModified, string modifiedBy)
//        { return Money.Random(); }
//        [TestMethod]
//        public override void LastModifiedTest() { }
//        [TestMethod]
//        public override void ModifiedByTest() { }
//        [TestMethod]
//        public override void IsReadOnlyTest() { }
//        [TestMethod]
//        public void RandomTest()
//        {
//            var a1 = Money.Random();
//            var a2 = Money.Random();
//            RandomTest(a1, a2, typeof(Money));
//        }
//        [TestMethod]
//        public void RoundTest()
//        {
//            Func<decimal, RoundingStrategyCode, sbyte, byte, double, decimal>
//                round = (x, strategy, percition, digits, step) =>
//                {
//                    var p = new RoundingPolicy(strategy, percition, digits, step);
//                    var v = new Money(x, usd, dt1);
//                    return v.Round(p).Amount;
//                };
//            Assert.AreEqual(4.5M, round(4.45M, RoundingStrategyCode.Up, 1, RandomValue.UInt8(), RandomValue.Double()));
//            Assert.AreEqual(-4.45M, round(-4.456M, RoundingStrategyCode.Down, 2, RandomValue.UInt8(), RandomValue.Double()));
//            Assert.AreEqual(-4.5M, round(-4.45M, RoundingStrategyCode.UpByStep, RandomValue.Int8(), RandomValue.UInt8(), 0.25));
//            Assert.AreEqual(4.25M, round(4.45M, RoundingStrategyCode.DownByStep, RandomValue.Int8(), RandomValue.UInt8(), 0.25));
//            Assert.AreEqual(-4.5M, round(-4.45M, RoundingStrategyCode.TowardsNegative, 1, RandomValue.UInt8(), RandomValue.Double()));
//            Assert.AreEqual(4.46M, round(4.456M, RoundingStrategyCode.TowardsPositive, 2, RandomValue.UInt8(), RandomValue.Double()));
//            Assert.AreEqual(4.45M, round(4.456M, RoundingStrategyCode.Round, 2, 7, RandomValue.Double()));
//        }
//        [TestMethod]
//        public void ConvertToTest()
//        {
//            Action<Money, Currency, decimal, DateTime> test = (a, b, c, d) => {
//                var x = a.ConvertTo(b);
//                Assert.AreEqual(b, x.Currency);
//                Assert.AreEqual(d, x.Date);
//                Assert.AreEqual(c, x.Amount);
//            };
//            test(new Money(10M, eur, dt1), eek, 100, dt1);
//            test(new Money(10M, eur, dt2), eek, 150, dt2);
//            test(new Money(200M, eek, dt1), eur, 20, dt1);
//            test(new Money(300M, eek, dt2), eur, 20, dt2);
//            test(new Money(10M, eur, dt1), usd, 20, dt1);
//            test(new Money(10M, eur, dt2), usd, 30, dt2);
//        }
//        [TestMethod]
//        public void CompareToTest()
//        {
//            Action<CommonMoney, CommonMoney, int> test = (a, b, c) =>
//            {
//                var q = a.CompareTo(b);
//                Assert.AreEqual(c, q);
//            };
//            var q1 = new Money(10M, eur, dt1);
//            var q2 = new Money(2M, usd, dt1);
//            var q3 = new Money(100M, eek, dt1);
//            test(q1, q2, Operand.Greater);
//            test(q2, q1, Operand.Less);
//            test(q1, q3, Operand.Equal);
//            test(q3, q1, Operand.Equal);
//        }
//        [TestMethod]
//        public void AddTest()
//        {
//            Action<Money, Money, decimal, Currency, DateTime> test = (a, b, c, d, e) =>
//            {
//                var q = a.Add(b);
//                Assert.AreEqual(d, q.Currency);
//                Assert.AreEqual(e, q.Date);
//                Assert.AreEqual(c, q.Amount);
//            };
//            var q1 = new Money(10M, eur, dt1);
//            var q2 = new Money(2M, usd, dt1);
//            var q3 = new Money(100M, eek, dt1);
//            var q4 = new Money(1M, eur, dt2);
//            test(q1, q2, 22, usd, dt1);
//            test(q2, q1, 11, eur, dt1);
//            test(q1, q3, 200, eek, dt1);
//            test(q3, q1, 20, eur, dt1);
//            test(q4, q3, 165, eek, dt1);
//            test(q3, q4, 11, eur, dt2);
//        }
//        [TestMethod]
//        public void SubtractTest()
//        {
//            Action<Money, Money, decimal, Currency, DateTime> test = (a, b, c, d, e) =>
//            {
//                var q = a.Subtract(b);
//                Assert.AreEqual(d, q.Currency);
//                Assert.AreEqual(e, q.Date);
//                Assert.AreEqual(c, q.Amount);
//            };
//            var q1 = new Money(10M, eur, dt1);
//            var q2 = new Money(2M, usd, dt1);
//            var q3 = new Money(100M, eek, dt1);
//            var q4 = new Money(1M, eur, dt2);
//            test(q1, q2, 18, usd, dt1);
//            test(q2, q1, -9, eur, dt1);
//            test(q1, q3, 0, eek, dt1);
//            test(q3, q1, 0, eur, dt1);
//            test(q4, q3, -135, eek, dt1);
//            test(q3, q4, 9, eur, dt2);
//        }
//        [TestMethod]
//        public void MultiplyTest()
//        {
//            Action<Money, decimal, decimal, Currency, DateTime> test = (a, b, c, d, e) =>
//            {
//                var q = a.Multiply(b);
//                Assert.AreEqual(d, q.Currency);
//                Assert.AreEqual(e, q.Date);
//                Assert.AreEqual(c, q.Amount);
//            };
//            var q1 = new Money(10M, eur, dt1);
//            var q2 = new Money(2M, usd, dt1);
//            var q3 = new Money(100M, eek, dt1);
//            var q4 = new Money(1M, eur, dt2);
//            var q5 = new Money(0, eur, dt2);
//            test(q1, -1, -10, eur, dt1);
//            test(q2, 0, 0, usd, dt1);
//            test(q3, 1, 100, eek, dt1);
//            test(q4, 2, 2M, eur, dt2);
//            test(q5, 3, 0, eur, dt2);
//        }
//        [TestMethod]
//        public void DivideTest()
//        {
//            Action<Money, Money, decimal> test1 = (a, b, c) =>
//            {
//                var q = a.Divide(b);
//                Assert.AreEqual(c, q);
//            };
//            Action<Money, decimal, decimal, Currency, DateTime> test2 = (a, b, c, d, e) =>
//            {
//                var q = a.Divide(b);
//                Assert.AreEqual(d, q.Currency);
//                Assert.AreEqual(e, q.Date);
//                Assert.AreEqual(c, q.Amount);
//            };
//            var q1 = new Money(10M, eur, dt1);
//            var q2 = new Money(2M, usd, dt1);
//            var q3 = new Money(100M, eek, dt1);
//            var q4 = new Money(1M, eur, dt2);
//            var q5 = new Money(0, eur, dt2);
//            test1(q1, q2, 10);
//            test1(q2, q1, 0.1M);
//            test1(q1, q3, 1);
//            test1(q3, q1, 1);
//            test1(q4, q3, 0.1M);
//            test1(q3, q4, 10);
//            test1(q3, null, 0);
//            test1(q3, q5, 0);
//            test2(q1, -1, -10, eur, dt1);
//            test2(q2, 0, 0, usd, dt1);
//            test2(q3, 1, 100, eek, dt1);
//            test2(q4, 2, 0.5M, eur, dt2);
//            test2(q5, 3, 0, eur, dt2);
//        }
//        [TestMethod]
//        public void AmountTest()
//        {
//            TestItem = GenerateRandomItem();
//            TestIsNotChanged();
//            TestItem.Amount = TestItem.Amount;
//            TestIsNotChanged();
//            var d = RandomValue.Decimal();
//            Assert.AreNotEqual(d, TestItem.Amount);
//            TestItem.Amount = d;
//            Assert.AreEqual(d, TestItem.Amount);
//            TestIsChanged();
//        }
//        [TestMethod]
//        public void CurrencyTest()
//        {
//            TestItem = GenerateRandomItem();
//            TestIsNotChanged();
//            TestItem.Currency = TestItem.Currency;
//            TestIsNotChanged();
//            var currency = Currency.Random();
//            Assert.IsFalse(currency.Equals(TestItem.Currency));
//            TestItem.Currency = currency;
//            Assert.IsTrue(currency.Equals(TestItem.Currency));
//            TestIsChanged();
//            TestItem = GenerateRandomItem();
//            TestIsNotChanged();
//            TestItem.Currency.Name = RandomValue.String();
//            TestIsChanged();
//        }
//        [TestMethod]
//        public void DateTest()
//        {
//            TestItem = GenerateRandomItem();
//            TestIsNotChanged();
//            TestItem.Date = TestItem.Date;
//            TestIsNotChanged();
//            var date = RandomValue.DateTime();
//            Assert.AreNotEqual(date, TestItem.Date);
//            TestItem.Date = date;
//            Assert.AreEqual(date, TestItem.Date);
//            TestIsChanged();
//        }
//        [TestMethod]
//        public void EmptyTest() {
//            Assert.IsTrue(new Money().IsEmpty());
//            Assert.IsTrue(CommonMoney.Empty.IsEmpty());
//            Assert.IsFalse(Money.Random().IsEmpty());
//        }
//    }
//}
