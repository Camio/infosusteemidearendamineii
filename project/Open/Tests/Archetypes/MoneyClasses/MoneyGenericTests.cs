﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Open.Archetypes.MoneyClasses;
//using Open.Tests.Archetypes.CommonClassses;
//namespace Open.Tests.Archetypes.MoneyClasses {
//    [TestClass] public abstract class MoneyGenericTests <T>: AnyGenericTests<T> where T: CommonMoney, new(){
//        //private Currency eek;
//        //private Currency usd;
//        //private Currency eur;
//        //private DateTime dt1;
//        //private DateTime dt2;
//        //[TestInitialize] public override void CreatItem() {
//        //    base.CreatItem();
//        //    CurrenciesBook.Instance.Clear();
//        //    CurrencyUtils.RegisterCurrencies();
//        //    usd = CurrenciesBook.Instance.Find("USD");
//        //    eur = CurrenciesBook.Instance.Find("EUR");
//        //    eek = CurrenciesBook.Instance.Find("EEK");
//        //    Assert.IsNotNull(usd);
//        //    Assert.IsNotNull(eur);
//        //    Assert.IsNotNull(eek);
//        //    dt1 = DateTime.Now.AddDays(-1);
//        //    dt2 = DateTime.Now.AddDays(-2);
//        //    CurrenciesBook.Instance.SetNominalRate(eur, dt1, 1M);
//        //    CurrenciesBook.Instance.SetNominalRate(usd, dt1, 2M);
//        //    CurrenciesBook.Instance.SetNominalRate(usd, dt2, 3M);
//        //    CurrenciesBook.Instance.SetNominalRate(eek, dt1, 10M);
//        //    CurrenciesBook.Instance.SetNominalRate(eek, dt2, 15M);
//        //}
//        //protected override CommonMoney GenerateRandomItem(DateTime lastModified, string modifiedBy) { return CommonMoney.Random(); }
//        //[TestMethod] public override void LastModifiedTest() { }
//        //[TestMethod] public override void ModifiedByTest() { }
//        //[TestMethod] public override void IsReadOnlyTest() { }
//        //[TestMethod] public void RandomTest() {
//        //    var a1 = CommonMoney.Random();
//        //    var a2 = CommonMoney.Random();
//        //    RandomTest(a1, a2, typeof (CommonMoney));
//        //}
//        //[TestMethod] public void RoundTest() {
//        //    Func<decimal, Strategy, sbyte, byte, double, decimal>
//        //        round = (x, strategy, percition, digits, step) =>
//        //        {
//        //            var p = new Policy(strategy, percition, digits, step);
//        //            var v = new CommonMoney(x, usd, dt1 );
//        //            return v.Round(p).Amount;
//        //        };
//        //    Assert.AreEqual(4.5M, round(4.45M, Strategy.Up, 1, MyRandom.UInt8(), MyRandom.Double()));
//        //    Assert.AreEqual(-4.45M, round(-4.456M, Strategy.Down, 2, MyRandom.UInt8(), MyRandom.Double()));
//        //    Assert.AreEqual(-4.5M, round(-4.45M, Strategy.UpByStep, MyRandom.Int8(), MyRandom.UInt8(), 0.25));
//        //    Assert.AreEqual(4.25M, round(4.45M, Strategy.DownByStep, MyRandom.Int8(), MyRandom.UInt8(), 0.25));
//        //    Assert.AreEqual(-4.5M, round(-4.45M, Strategy.TowardsNegative, 1, MyRandom.UInt8(), MyRandom.Double()));
//        //    Assert.AreEqual(4.46M, round(4.456M, Strategy.TowardsPositive, 2, MyRandom.UInt8(), MyRandom.Double()));
//        //    Assert.AreEqual(4.45M, round(4.456M, Strategy.Round, 2, 7, MyRandom.Double()));
//        //}
//        //[TestMethod] public void ConvertToTest() {
//        //    Action<CommonMoney, Currency, decimal, DateTime> test = (a, b, c, d) => {
//        //        var x = a.ConvertTo(b);
//        //        Assert.AreEqual(b, x.Currency);
//        //        Assert.AreEqual(d, x.Date);
//        //        Assert.AreEqual(c, x.Amount);
//        //    };
//        //    test(new CommonMoney(10M, eur, dt1), eek, 100, dt1);
//        //    test(new CommonMoney(10M, eur, dt2), eek, 150, dt2);
//        //    test(new CommonMoney(200M, eek, dt1), eur, 20, dt1);
//        //    test(new CommonMoney(300M, eek, dt2), eur, 20, dt2);
//        //    test(new CommonMoney(10M, eur, dt1), usd, 20, dt1);
//        //    test(new CommonMoney(10M, eur, dt2), usd, 30, dt2);
//        //}
//        //[TestMethod] public void CompareToTest() {
//        //    Action<CommonMoney, CommonMoney, int> test = (a, b, c) =>
//        //    {
//        //        var q = a.CompareTo(b);
//        //        Assert.AreEqual(c, q);
//        //    };
//        //    var q1 = new CommonMoney(10M, eur, dt1);
//        //    var q2 = new CommonMoney(2M, usd, dt1);
//        //    var q3 = new CommonMoney(100M, eek, dt1);
//        //    test(q1, q2, Operand.Greater);
//        //    test(q2, q1, Operand.Less);
//        //    test(q1, q3, Operand.Equal);
//        //    test(q3, q1, Operand.Equal);
//        //}
//        //[TestMethod] public void AddTest() {
//        //    Action<CommonMoney, CommonMoney, decimal, Currency, DateTime> test = (a, b, c, d, e) =>
//        //    {
//        //        var q = a.Add(b);
//        //        Assert.AreEqual(d, q.Currency);
//        //        Assert.AreEqual(e, q.Date);
//        //        Assert.AreEqual(c, q.Amount);
//        //    };
//        //    var q1 = new CommonMoney(10M, eur, dt1);
//        //    var q2 = new CommonMoney(2M, usd, dt1);
//        //    var q3 = new CommonMoney(100M, eek, dt1);
//        //    var q4 = new CommonMoney(1M, eur, dt2);
//        //    test(q1, q2, 22, usd, dt1);
//        //    test(q2, q1, 11, eur, dt1);
//        //    test(q1, q3, 200, eek, dt1);
//        //    test(q3, q1, 20, eur, dt1);
//        //    test(q4, q3, 165, eek, dt1);
//        //    test(q3, q4, 11, eur, dt2);
//        //}
//        //[TestMethod] public void SubtractTest() {
//        //    Action<CommonMoney, CommonMoney, decimal, Currency, DateTime> test = (a, b, c, d, e) =>
//        //    {
//        //        var q = a.Subtract(b);
//        //        Assert.AreEqual(d, q.Currency);
//        //        Assert.AreEqual(e, q.Date);
//        //        Assert.AreEqual(c, q.Amount);
//        //    };
//        //    var q1 = new CommonMoney(10M, eur, dt1);
//        //    var q2 = new CommonMoney(2M, usd, dt1);
//        //    var q3 = new CommonMoney(100M, eek, dt1);
//        //    var q4 = new CommonMoney(1M, eur, dt2);
//        //    test(q1, q2, 18, usd, dt1);
//        //    test(q2, q1, -9, eur, dt1);
//        //    test(q1, q3, 0, eek, dt1);
//        //    test(q3, q1, 0, eur, dt1);
//        //    test(q4, q3, -135, eek, dt1);
//        //    test(q3, q4, 9, eur, dt2);
//        //}
//        //[TestMethod] public void MultiplyTest() {
//        //    Action<CommonMoney, decimal, decimal, Currency, DateTime> test = (a, b, c, d, e) =>
//        //    {
//        //        var q = a.Multiply(b);
//        //        Assert.AreEqual(d, q.Currency);
//        //        Assert.AreEqual(e, q.Date);
//        //        Assert.AreEqual(c, q.Amount);
//        //    };
//        //    var q1 = new CommonMoney(10M, eur, dt1);
//        //    var q2 = new CommonMoney(2M, usd, dt1);
//        //    var q3 = new CommonMoney(100M, eek, dt1);
//        //    var q4 = new CommonMoney(1M, eur, dt2);
//        //    var q5 = new CommonMoney(0, eur, dt2);
//        //    test(q1, -1, -10, eur, dt1);
//        //    test(q2, 0, 0, usd, dt1);
//        //    test(q3, 1, 100, eek, dt1);
//        //    test(q4, 2, 2M, eur, dt2);
//        //    test(q5, 3, 0, eur, dt2);
//        //}
//        //[TestMethod] public void DivideTest() {
//        //    Action<CommonMoney, CommonMoney, decimal> test1 = (a, b, c) =>
//        //    {
//        //        var q = a.Divide(b);
//        //        Assert.AreEqual(c, q);
//        //    };
//        //    Action<CommonMoney, decimal, decimal, Currency, DateTime> test2 = (a, b, c, d, e) =>
//        //    {
//        //        var q = a.Divide(b);
//        //        Assert.AreEqual(d, q.Currency);
//        //        Assert.AreEqual(e, q.Date);
//        //        Assert.AreEqual(c, q.Amount);
//        //    };
//        //    var q1 = new CommonMoney(10M, eur, dt1);
//        //    var q2 = new CommonMoney(2M, usd, dt1);
//        //    var q3 = new CommonMoney(100M, eek, dt1);
//        //    var q4 = new CommonMoney(1M, eur, dt2);
//        //    var q5 = new CommonMoney(0, eur, dt2);
//        //    test1(q1, q2, 10);
//        //    test1(q2, q1, 0.1M);
//        //    test1(q1, q3, 1);
//        //    test1(q3, q1, 1);
//        //    test1(q4, q3, 0.1M);
//        //    test1(q3, q4, 10);
//        //    test1(q3, null, 0);
//        //    test1(q3, q5, 0);
//        //    test2(q1, -1, -10, eur, dt1);
//        //    test2(q2, 0, 0, usd, dt1);
//        //    test2(q3, 1, 100, eek, dt1);
//        //    test2(q4, 2, 0.5M, eur, dt2);
//        //    test2(q5, 3, 0, eur, dt2);
//        //}
//        //[TestMethod] public void AmountTest() {
//        //    TestItem = GenerateRandomItem();
//        //    TestIsNotChanged();
//        //    TestItem.Amount = TestItem.Amount;
//        //    TestIsNotChanged();
//        //    var d = MyRandom.Decimal();
//        //    Assert.AreNotEqual(d, TestItem.Amount);
//        //    TestItem.Amount = d;
//        //    Assert.AreEqual(d, TestItem.Amount);
//        //    TestIsChanged();
//        //}
//        //[TestMethod] public void CurrencyTest() {
//        //    TestItem = GenerateRandomItem();
//        //    TestIsNotChanged();
//        //    TestItem.Currency = TestItem.Currency;
//        //    TestIsNotChanged();
//        //    var currency = Currency.Random();
//        //    Assert.IsFalse(currency.Equals(TestItem.Currency));
//        //    TestItem.Currency = currency;
//        //    Assert.IsTrue(currency.Equals(TestItem.Currency));
//        //    TestIsChanged();
//        //    TestItem = GenerateRandomItem();
//        //    TestIsNotChanged();
//        //    TestItem.Currency.Name = MyRandom.String();
//        //    TestIsChanged();
//        //}
//        //[TestMethod] public void DateTest() {
//        //    TestItem = GenerateRandomItem();
//        //    TestIsNotChanged();
//        //    TestItem.Date = TestItem.Date;
//        //    TestIsNotChanged();
//        //    var date = MyRandom.DateTime();
//        //    Assert.AreNotEqual(date, TestItem.Date);
//        //    TestItem.Date = date;
//        //    Assert.AreEqual(date, TestItem.Date);
//        //    TestIsChanged();
//        //}
//        //[TestMethod] public void EmptyTest() {
//        //    Assert.IsTrue(new CommonMoney().IsEmpty());
//        //    Assert.IsTrue(CommonMoney.Empty.IsEmpty());
//        //    Assert.IsFalse(CommonMoney.Random().IsEmpty());
//        //}
//    }
//}
