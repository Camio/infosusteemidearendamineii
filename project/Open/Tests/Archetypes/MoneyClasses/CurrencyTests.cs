﻿//using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Open.Archetypes.CommonClasses;
//using Open.Archetypes.MoneyClasses;
//using Open.Tests.Archetypes.CommonClassses;
//namespace Open.Tests.Archetypes.MoneyClasses {
//    [TestClass] public class CurrencyTests : ClassifierGenericTests<Currency> {
//        protected override Currency GenerateRandomItem(DateTime lastModified, string modifiedBy) {
//            var o = Currency.Random();
//            return new Currency(o.Id, o.Name, o.Code, o.Description, o.Valid, lastModified, modifiedBy);
//        }
//        [TestMethod] public void ToLocalTest() {
//            CurrenciesBook.Instance.Clear();
//            var dt = RandomValue.DateTime();
//            const decimal d = 10000M;
//            Assert.AreEqual(0, TestItem.ToNominal(d, dt));
//            const decimal rate1 = 1.2345M;
//            const decimal rate2 = 5.4321M;
//            const decimal rate3 = 9.8765M;
//            const decimal rate4 = 5.6789M;
//            CurrenciesBook.Instance.SetNominalRate(TestItem, dt, rate1);
//            CurrenciesBook.Instance.SetNominalRate(TestItem, dt.AddDays(1), rate2);
//            CurrenciesBook.Instance.SetNominalRate(TestItem, dt.AddDays(-300), rate3);
//            CurrenciesBook.Instance.SetNominalRate(TestItem, dt.AddDays(300), rate4);
//            Assert.AreEqual(d/rate1, TestItem.ToNominal(d, dt));
//            Assert.AreEqual(d/rate2, TestItem.ToNominal(d, dt.AddDays(1)));
//            Assert.AreEqual(d/rate1, TestItem.ToNominal(d, dt.AddDays(-1)));
//            Assert.AreEqual(d/rate1, TestItem.ToNominal(d, dt.AddDays(-149)));
//            Assert.AreEqual(d/rate3, TestItem.ToNominal(d, dt.AddDays(-151)));
//            Assert.AreEqual(d/rate2, TestItem.ToNominal(d, dt.AddDays(2)));
//            Assert.AreEqual(d/rate2, TestItem.ToNominal(d, dt.AddDays(149)));
//            Assert.AreEqual(d/rate4, TestItem.ToNominal(d, dt.AddDays(151)));
//        }
//        [TestMethod] public void FromLocalTest() {
//            CurrenciesBook.Instance.Clear();
//            var dt = RandomValue.DateTime();
//            const decimal d = 10000M;
//            Assert.AreEqual(0, TestItem.ToNominal(d, dt));
//            const decimal rate1 = 1.2345M;
//            const decimal rate2 = 5.4321M;
//            const decimal rate3 = 9.8765M;
//            const decimal rate4 = 5.6789M;
//            CurrenciesBook.Instance.SetNominalRate(TestItem, dt, rate1);
//            CurrenciesBook.Instance.SetNominalRate(TestItem, dt.AddDays(1), rate2);
//            CurrenciesBook.Instance.SetNominalRate(TestItem, dt.AddDays(-300), rate3);
//            CurrenciesBook.Instance.SetNominalRate(TestItem, dt.AddDays(300), rate4);
//            Assert.AreEqual(d*rate1, TestItem.FromNominal(d, dt));
//            Assert.AreEqual(d*rate2, TestItem.FromNominal(d, dt.AddDays(1)));
//            Assert.AreEqual(d*rate1, TestItem.FromNominal(d, dt.AddDays(-1)));
//            Assert.AreEqual(d*rate1, TestItem.FromNominal(d, dt.AddDays(-149)));
//            Assert.AreEqual(d*rate3, TestItem.FromNominal(d, dt.AddDays(-151)));
//            Assert.AreEqual(d*rate2, TestItem.FromNominal(d, dt.AddDays(2)));
//            Assert.AreEqual(d*rate2, TestItem.FromNominal(d, dt.AddDays(149)));
//            Assert.AreEqual(d*rate4, TestItem.FromNominal(d, dt.AddDays(151)));
//        }
//        [TestMethod] public override void IsEmptyTest() {
//            Assert.IsTrue(Currency.Empty.IsEmpty());
//            Assert.IsTrue((new Currency()).IsEmpty());
//            Assert.IsFalse(TestItem.IsEmpty());
//        }
//        [TestMethod] public void EmptyTest() { Assert.IsTrue(Currency.Empty.Equals(new Currency())); }
//        [TestMethod] public void FormulaTest() {
//            Assert.AreEqual(TestItem.Name, TestItem.Formula());
//            Assert.AreEqual(TestItem.Code, TestItem.Formula(false));
//        }
//    }
//}
