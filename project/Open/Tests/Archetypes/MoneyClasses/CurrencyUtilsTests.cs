﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Open.Archetypes.CommonClasses;
//using Open.Archetypes.MoneyClasses;
//namespace Open.Tests.Archetypes.MoneyClasses {
//    [TestClass] public class CurrencyUtilsTests : Utils.CommonTests {
//        [TestInitialize] public override void TestInitialize() {
//            TestItemType = typeof (CurrencyUtils);
//            CurrenciesBook.Instance.Clear();
//        }
//        [TestMethod]
//        public void GetResponseTest()
//        {
//            GetRespondEuroDaylyTest();
//            GetRespondEuroHistoryTest();
//        }
//        [TestMethod]
//        public void GetRespondEuroHistoryTest() {
//            var s = WebServiceAccess.GetResponse(CurrencyUtils.EuroHistoryUrl);
//            Assert.AreNotEqual(string.Empty, s);
//        }
//        [TestMethod]
//        public void GetRespondEuroDaylyTest()
//        {
//            var s = WebServiceAccess.GetResponse(CurrencyUtils.EuroDailyUrl);
//            Assert.AreNotEqual(string.Empty, s);
//        }
//        [TestMethod]
//        public void DownloadEuroRatesHistoryTest()
//        {
//            CurrenciesBook.Instance.Clear();
//            CurrencyUtils.RegisterCurrencies();
//            var usd = CurrenciesBook.Instance.Find("USD");
//            var eur = CurrenciesBook.Instance.Find("EUR");
//            var dt = RandomValue.DateTime();
//            Assert.AreEqual(0, CurrenciesBook.Instance.GetNominalRate(usd, dt));
//            Assert.AreEqual(1, CurrenciesBook.Instance.GetNominalRate(eur, dt));
//            CurrencyUtils.DownloadEuroRatesHistory();
//            Assert.AreNotEqual(0, CurrenciesBook.Instance.GetNominalRate(usd, dt));
//            Assert.AreEqual(1, CurrenciesBook.Instance.GetNominalRate(eur, dt));
//        }
//        [TestMethod]
//        public void DownloadEuroRatesDailyTest() {
//            CurrenciesBook.Instance.Clear();
//            CurrencyUtils.RegisterCurrencies();
//            var usd = CurrenciesBook.Instance.Find("USD");
//            var eur = CurrenciesBook.Instance.Find("EUR");
//            var dt = RandomValue.DateTime();
//            Assert.AreEqual(0, CurrenciesBook.Instance.GetNominalRate(usd, dt));
//            Assert.AreEqual(1, CurrenciesBook.Instance.GetNominalRate(eur, dt));
//            CurrencyUtils.DownloadEuroRatesDaily();
//            Assert.AreNotEqual(0, CurrenciesBook.Instance.GetNominalRate(usd, dt));
//            Assert.AreEqual(1, CurrenciesBook.Instance.GetNominalRate(eur, dt));
//        }
//        [TestMethod] public void RegisterCurrenciesTest() {
//            CurrenciesBook.Instance.Clear();
//            var c = CurrenciesBook.Instance.Find("USD");
//            Assert.AreEqual(0, CurrenciesBook.Instance.Items.Count);
//            Assert.IsTrue(c.Equals(Currency.Empty));
//            CurrencyUtils.RegisterCurrencies();
//            c = CurrenciesBook.Instance.Find("USD");
//            Assert.IsFalse(c.Equals(Currency.Empty));
//            Assert.AreEqual("USD", c.Id);
//            Assert.AreEqual("USD", c.Code);
//            Assert.IsTrue(IsoCountries.Get().Count > CurrenciesBook.Instance.Items.Count);
//        }
//    }
//}
