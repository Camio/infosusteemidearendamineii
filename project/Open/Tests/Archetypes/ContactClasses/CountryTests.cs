﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses {
    [TestClass] public class CountryTests : CommonTests<Country> {
        [TestMethod] public void ConstructorTest() {
            Assert.AreEqual(typeof(NamedItem), Obj.GetType().BaseType);
        }
        protected override Country getRandomObj() {
            return Country.Random();
        }
    }
}
