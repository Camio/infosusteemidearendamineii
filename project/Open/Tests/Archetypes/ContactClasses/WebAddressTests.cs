﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses {
    [TestClass] public class WebAddressTests : CommonTests<WebAddress> {
        [TestMethod] public void IsAddressTest() { Assert.IsInstanceOfType(Obj, typeof(Contact)); }
        [TestMethod] public void UrlTest() {
            Obj = new WebAddress();
            testProperty(() => Obj.Url, x => Obj.Url = x);
        }
        protected override WebAddress getRandomObj() {
            return WebAddress.Random();
        }
        [TestMethod]
        public void ContentTest()
        {
            var s = Obj.Url;
            Assert.AreEqual(s, Obj.Content);
        }
    }
}
