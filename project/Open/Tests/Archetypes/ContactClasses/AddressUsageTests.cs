﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses {
    [TestClass]
    public class AddressUsageTests : CommonTests<AddressUsage>
    {
        [TestMethod]
        public void ConstructorTest()
        {
            Assert.AreEqual(Obj.GetType().BaseType,
                typeof(UniqueEntity));
        }
        [TestMethod]
        public void UsesTest()
        {
            testProperty(() => Obj.Uses, x => Obj.Uses = x);
        }
        [TestMethod]
        public void AddressTest()
        {
            testProperty(() => Obj.Address, x => Obj.Address = x);
        }
        protected override AddressUsage getRandomObj()
        {
            return AddressUsage.Random();
        }
    }
}
