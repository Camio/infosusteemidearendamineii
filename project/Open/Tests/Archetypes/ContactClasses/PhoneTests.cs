﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses {
    [TestClass] public class PhoneTests : CommonTests<Phone> {
        [TestMethod] public void CountryCodeTest() {
            testProperty(() => Obj.CountryCode, x => Obj.CountryCode = x);
        }
        [TestMethod] public void DirectDialingPrefixTest() {
            Obj = new Phone();
            testProperty(() => Obj.DirectDialingPrefix, x => Obj.DirectDialingPrefix = x);
        }

        [TestMethod] public void AreaCodeTest() {
            Obj = new Phone();
            testProperty(() => Obj.AreaCode, x => Obj.AreaCode = x);
        }
        [TestMethod] public void NumberTest() {
            Obj = new Phone();
            testProperty(() => Obj.Number, x => Obj.Number = x);
        }
        [TestMethod] public void ExtensionTest() {
            Obj = new Phone();
            testProperty(() => Obj.Extension, x => Obj.Extension = x);
        }
        [TestMethod] public void DeviceTest() {
            Obj = new Phone();
            testEnumProperty(() => Obj.Device, x => Obj.Device = x);
        }
        [TestMethod] public void RegisteredInTest() {
            Assert.AreEqual(0, Obj.RegisteredIn.Count);
            var a = Address.Random();
            Addresses.Instance.Add(a);
            var r = new Registration {AddressId = a.UniqueId, PhoneId = Obj.UniqueId};
            Registrations.Instance.Add(r);
            var l = Obj.RegisteredIn;
            Assert.AreEqual(1, l.Count);
            Assert.AreEqual(a, l[0]);
        }
        protected override Phone getRandomObj() { return Phone.Random(); }
        [TestMethod] public void ContentTest() {
            var s =
                $"+{Obj.CountryCode} ({Obj.DirectDialingPrefix}){Obj.AreaCode} {Obj.Number} ext.{Obj.Extension}";
            Assert.AreEqual(s, Obj.Content);
        }
    }
}
