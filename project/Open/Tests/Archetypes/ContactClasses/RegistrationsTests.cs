﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses
{
    [TestClass]
    public class RegistrationsTests: CommonTests<Registrations>
    {
        protected override Registrations getRandomObj() {
            return Registrations.Random();
        }
        [TestMethod] public void GetPhonesTest() {
            var a = Address.Random();
            Assert.AreEqual(0, Registrations.GetPhones(a.UniqueId).Count);
            var c = GetRandom.Count();
            for (var i = 0; i < c; i++) {
                var t = Phone.Random();
                Phones.Instance.Add(t);
                var r = Registration.Random();
                r.PhoneId = t.UniqueId;
                r.AddressId = a.UniqueId;
                Registrations.Instance.Add(r);
            }
            Assert.AreEqual(c, Registrations.GetPhones(a.UniqueId).Count);
        }
        [TestMethod]
        public void GetAddressesTest()
        {
            var t = Phone.Random();
            Assert.AreEqual(0, Registrations.GetAddresses(t.UniqueId).Count);
            var c = GetRandom.Count();
            for (var i = 0; i < c; i++)
            {
                var a = Address.Random();
                Addresses.Instance.Add(a);
                var r = Registration.Random();
                r.PhoneId = t.UniqueId;
                r.AddressId = a.UniqueId;
                Registrations.Instance.Add(r);
            }
            Assert.AreEqual(c, Registrations.GetAddresses(t.UniqueId).Count);
        }
        [TestMethod] public void InstanceTest() {
            testSingleton(()=> Registrations.Instance);
        }
    }
}
