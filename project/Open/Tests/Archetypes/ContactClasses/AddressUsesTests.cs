﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses {
    [TestClass] public class AddressUsesTests : CommonTests<AddressUses> {
        [TestMethod] public void ConstructorTest() {
            Assert.AreEqual(Obj.GetType().BaseType, typeof(Strings));
        }
        protected override AddressUses getRandomObj() {
            return AddressUses.Random();
        }
    }
}