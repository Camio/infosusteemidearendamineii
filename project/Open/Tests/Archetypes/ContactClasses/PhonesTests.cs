﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses {
    [TestClass] public class PhonesTests : CommonTests<Phones> {
        [TestMethod] public void ConstructorTest() {
            Assert.AreEqual(Obj.GetType().BaseType, typeof(Archetypes<Phone>));
        }
        protected override Phones getRandomObj() { return Phones.Random(); }
        [TestMethod] public void GetEquipmentTest() {
            var a = Address.Random();
            Assert.AreEqual(0, Phones.GetEquipment(a.UniqueId).Count);
            var c = GetRandom.Count();
            for (var i = 0; i < c; i++) {
                var t = Phone.Random();
                Phones.Instance.Add(t);
                var r = new Registration {AddressId = a.UniqueId, PhoneId = t.UniqueId};
                Registrations.Instance.Add(r);
            }
            Assert.AreEqual(c, Phones.GetEquipment(a.UniqueId).Count);
        }
        [TestMethod]
        public void FindTest()
        {
            var a = Phone.Random();
            Assert.IsNull(Phones.Find(a.UniqueId));
            Phones.Instance.Add(a);
            Assert.AreEqual(a, Phones.Find(a.UniqueId));
        }
        [TestMethod]
        public void InstanceTest()
        {
            testSingleton(() => Phones.Instance);
        }

    }
}