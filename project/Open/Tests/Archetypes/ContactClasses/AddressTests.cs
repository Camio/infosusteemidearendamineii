﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses {
    [TestClass] public class AddressTests : CommonTests<Address> {
        [TestMethod] public void IsAddressTest() {
            Assert.AreEqual(Obj.GetType().BaseType, typeof(Contact));
        }
        [TestMethod] public void AddressLinesTest() {
            Obj = new Address();
            testProperty(() => Obj.AddressLines, x => Obj.AddressLines = x);
        }
        [TestMethod] public void LocationTest() {
            Obj = new Address();
            testProperty(() => Obj.Location, x => Obj.Location = x);
        }
        [TestMethod] public void RegionTest() {
            Obj = new Address();
            testProperty(() => Obj.Region, x => Obj.Region = x);
        }
        [TestMethod] public void ZipCodeTest() {
            Obj = new Address();
            testProperty(() => Obj.ZipCode, x => Obj.ZipCode = x);
        }
        [TestMethod] public void CountryTest() {
            Obj = new Address();
            testProperty(() => Obj.Country, x => Obj.Country = x);
        }
        [TestMethod] public void RegisteredEquipmentsTest() {
            Assert.AreEqual(0, Obj.RegisteredEquipments.Count);
            var t = Phone.Random();
            Phones.Instance.Add(t);
            var r = new Registration {
                AddressId = Obj.UniqueId,
                PhoneId = t.UniqueId
            };
            Registrations.Instance.Add(r);
            var l = Obj.RegisteredEquipments;
            Assert.AreEqual(1, l.Count);
            Assert.AreEqual(t, l[0]);
        }
        protected override Address getRandomObj() { return Address.Random(); }
        [TestMethod]
        public void ContentTest()
        {
            var s = $"{Obj.AddressLines.Content} {Obj.ZipCode} {Obj.Location} {Obj.Region} {Obj.Country.Name}".Trim();
            Assert.AreEqual(s, Obj.Content);
        }
    }
}
