﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses {
    [TestClass]
    public class AddressUsagesTests : CommonTests<AddressUsages>
    {
        [TestMethod]
        public void ConstructorTest()
        {
            Assert.AreEqual(Obj.GetType().BaseType,
                typeof(Archetypes<AddressUsage>));
        }
        protected override AddressUsages getRandomObj()
        {
            return AddressUsages.Random();
        }
        [TestMethod] public void InstanceTest() {
            testSingleton(()=> AddressUsages.Instance);
        }
    }
}