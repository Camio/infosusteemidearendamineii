﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses {
    [TestClass] public class RegistrationTests : CommonTests<Registration> {
        protected override Registration getRandomObj() { return Registration.Random(); }
        [TestMethod] public void PhoneIdTest() {
            Obj = new Registration();
            testProperty(() => Obj.PhoneId, x => Obj.PhoneId = x);
        }
        [TestMethod] public void AddressIdTest() {
            Obj = new Registration();
            testProperty(() => Obj.AddressId, x => Obj.AddressId = x);
        }
        [TestMethod] public void PhoneTest() {
            Assert.AreEqual(null, Obj.Phone);
            var p = Phone.Random();
            Obj.PhoneId = p.UniqueId;
            Phones.Instance.Add(p);
            Assert.AreEqual(p, Obj.Phone);
        }
        [TestMethod] public void AddressTest() {
            Assert.AreEqual(null, Obj.Phone);
            var a = Address.Random();
            Obj.AddressId = a.UniqueId;
            Addresses.Instance.Add(a);
            Assert.AreEqual(a, Obj.Address);
        }
    }
}
