﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses {
    [TestClass] public class IsoCountryTests : CommonTests<IsoCountry> {
        [TestMethod] public void CharacterCodeTest() {
            testProperty(() => Obj.CharacterCode, x => Obj.CharacterCode = x);
        }
        [TestMethod] public void OfficialNameTest() {
            testProperty(() => Obj.OfficialName, x => Obj.OfficialName = x);
        }
        [TestMethod] public void NumericCodeTest() {
            testProperty(() => Obj.NumericCode, x => Obj.NumericCode = x);
        }
        protected override IsoCountry getRandomObj() {
            return IsoCountry.Random();
        }
    }
}
