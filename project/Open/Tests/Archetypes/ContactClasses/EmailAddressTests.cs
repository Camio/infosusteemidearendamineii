﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses
{
    [TestClass] public class EmailAddressTests : CommonTests<EmailAddress> {
        [TestMethod] public void IsAddressTest() {
            Assert.AreEqual(Obj.GetType().BaseType, typeof(Contact));
        }
        [TestMethod] public void AddressTest() {
            Obj = new EmailAddress();
            testProperty(()=> Obj.Address, x => Obj.Address = x);
        }
        protected override EmailAddress getRandomObj() {
            return EmailAddress.Random();
        }
        [TestMethod]
        public void ContentTest()
        {
            var s = Obj.Address.Trim();
            Assert.AreEqual(s, Obj.Content);
        }
    }
}
