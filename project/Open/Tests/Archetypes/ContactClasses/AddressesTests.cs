﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses {
    [TestClass] public class AddressesTests : CommonTests<Addresses> {
        [TestMethod] public void ConstructorTest() {
            Assert.AreEqual(Obj.GetType().BaseType, typeof(Archetypes<Address>));
        }
        protected override Addresses getRandomObj() {
            return Addresses.Random();
        }
        [TestMethod] public void FindTest() {
            var a = Address.Random();
            Assert.IsNull(Addresses.Find(a.UniqueId));
            Addresses.Instance.Add(a);
            Assert.AreEqual(a, Addresses.Find(a.UniqueId));
        }
        [TestMethod] public void InstanceTest() {
            testSingleton(() => Addresses.Instance);
        }
        [TestMethod] public void GetRegistrationsTest() {
            var t = Phone.Random();
            Assert.AreEqual(0, Addresses.GetRegistrations(t.UniqueId).Count);
            var c = GetRandom.Count();
            for (var i = 0; i < c; i++) {
                var a = Address.Random(); 
                Addresses.Instance.Add(a);
                var r = new Registration {AddressId = a.UniqueId, PhoneId = t.UniqueId};
                Registrations.Instance.Add(r);
            }
            Assert.AreEqual(c, Addresses.GetRegistrations(t.UniqueId).Count);
        }
    }
}