﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses {
    [TestClass] public class ContactTests : CommonTests<Contact> {
        [TestMethod] public void ConstructorTest() {
            Assert.AreEqual(Obj.GetType().BaseType, typeof(UniqueEntity));
        }
        [TestMethod]
        public void ContentTest()
        {
            Assert.AreEqual(string.Empty, Obj.Content);
        }
        protected override Contact getRandomObj() { return Contact.Random(); }
    }
}
