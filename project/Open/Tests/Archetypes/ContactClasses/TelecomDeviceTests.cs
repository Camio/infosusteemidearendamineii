﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.ContactClasses;
namespace Open.Tests.Archetypes.ContactClasses {
    [TestClass] public class TelecomDeviceTests : ClassTests<TelecomDevice> {
        [TestMethod] public void IsEnum() { Assert.IsTrue(typeof(TelecomDevice).IsEnum); }
        [TestMethod] public void CountTest() { Assert.AreEqual(5, GetEnum.Count<TelecomDevice>()); }
        [TestMethod] public void OtherTest() { Assert.AreEqual(0, (int) TelecomDevice.Other); }
        [TestMethod] public void PhoneTest() { Assert.AreEqual(1, (int) TelecomDevice.Phone); }
        [TestMethod] public void MobileTest() { Assert.AreEqual(2, (int) TelecomDevice.Mobile); }
        [TestMethod] public void FaxTest() { Assert.AreEqual(3, (int) TelecomDevice.Fax); }
        [TestMethod] public void PagerTest() { Assert.AreEqual(4, (int) TelecomDevice.Pager); }
    }
}
