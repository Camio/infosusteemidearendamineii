﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.PartyClasses;
using Open.Archetypes.RoleClasses;
namespace Open.Tests.Archetypes.RoleClasses {
    [TestClass]
    public class RoleTests : CommonTests<Role> {
        protected override Role getRandomObj() { return Role.Random(); }

        [TestInitialize]
        public override void TestInitialize() {
            base.TestInitialize();
        }

        [TestCleanup]
        public override void TestCleanup() {
            base.TestCleanup();
            Entities.Instance.Clear();
        }

        [TestMethod]
        public void Constructor() {
            var role = new Role().GetType().BaseType;
            Assert.AreEqual(role, typeof(BaseEntity<RoleType>));
        }

        [TestMethod]
        public void ConstructorWithParameters() {
            var name = GetRandom.String();
            var description = GetRandom.String();
            var typeId = GetRandom.String();
            var role = new Role(name, description, typeId);
            Assert.AreEqual(name, role.Name);
            Assert.AreEqual(description, role.Description);
            Assert.AreEqual(typeId, role.TypeId);
        }

        [TestMethod]
        public void PerformerTest() {
            Assert.IsNull(Obj.Performer);
            var parties = Parties.Random();
            var party = parties.list.Last();
            Obj.PerformerId = party.UniqueId;
            Parties.Instance.AddRange(parties);
            Assert.AreEqual(party, Obj.Performer);
        }

        [TestMethod]
        public void IsPerformerIdTest() {
            Obj.PerformerId = GetRandom.String();
            Assert.AreEqual(false, Obj.IsPerformerId(GetRandom.String()));
            Assert.AreEqual(true, Obj.IsPerformerId(Obj.PerformerId));
        }

        [TestMethod]
        public void IsPerformerIdEmptyString() {
            Assert.IsFalse(Obj.IsPerformerId(string.Empty));
        }

        [TestMethod]
        public void IsPerformerIdOnlySpaces() {
            Assert.IsFalse(Obj.IsPerformerId("         "));
        }

        [TestMethod]
        public void PerformerIdTest() {
            Obj = new Role();
            testProperty(() => Obj.PerformerId, x => Obj.PerformerId = x);
        }

        [TestMethod]
        public void SetName() {
            const string testName = "Name";
            var role = new Role {Name = testName};
            Assert.AreEqual(role.Name, testName);
        }

        [TestMethod]
        public void SetEmptyName() {
            var testName = string.Empty;
            var role = new Role {Name = testName};
            Assert.AreEqual(role.Name, testName);
        }

        [TestMethod]
        public void SetNullName() {
            var role = new Role {Name = null};
            Assert.AreEqual(role.Name, string.Empty);
        }

        [TestMethod]
        public void GetName() {
            var role = new Role();
            Assert.AreEqual(role.Name, string.Empty);
        }

        [TestMethod]
        public void SetDescription() {
            const string testDescription = "Description";
            var role = new Role {Description = testDescription};
            Assert.AreEqual(role.Description, testDescription);
        }

        [TestMethod]
        public void SetEmptyDescription() {
            var testDescription = string.Empty;
            var role = new Role {Name = testDescription};
            Assert.AreEqual(role.Name, testDescription);
        }

        [TestMethod]
        public void SetNullDescription() {
            var role = new Role {Description = null};
            Assert.AreEqual(role.Description, string.Empty);
        }

        [TestMethod]
        public void GetDescription() {
            var role = new Role();
            Assert.AreEqual(role.Description, string.Empty);
        }

        [TestMethod]
        public void IsActiveDefault() {
            var role = new Role();
            Assert.IsTrue(role.IsActive);
        }

        [TestMethod]
        public void IsActiveValidToHigher() {
            var role = new Role {
                                    Valid =
                                        new Period {
                                                       From = DateTime.Now - TimeSpan.FromDays(2),
                                                       To = DateTime.Now + TimeSpan.FromDays(12)
                                                   }
                                };
            Assert.IsTrue(role.IsActive);
        }

        [TestMethod]
        public void IsActiveValidToLower() {
            var role = new Role {
                                    Valid = {
                                                From = DateTime.Now - TimeSpan.FromDays(2),
                                                To = DateTime.Now - TimeSpan.FromDays(1)
                                            }
                                };
            Assert.IsFalse(role.IsActive);
        }

        [TestMethod]
        public void IsActiveValidFromHigher() {
            var role = new Role {
                                    Valid = {
                                                From = DateTime.Now + TimeSpan.FromDays(2),
                                                To = DateTime.Now + TimeSpan.FromDays(12)
                                            }
                                };
            Assert.IsFalse(role.IsActive);
        }

        [TestMethod]
        public void TypeFound() {
            RoleTypes.Instance.AddRange(RoleTypes.Random());
            var firstRoleType = RoleTypes.Instance[0];
            var role = new Role {TypeId = firstRoleType.UniqueId};
            Assert.AreEqual(role.Type, firstRoleType);
        }

        [TestMethod]
        public void TypeNotFound() {
            var role = new Role {TypeId = "test"};
            Assert.IsNull(role.Type);
        }

        [TestMethod]
        public void SetRandomValues() {
            var role = Role.Random();
            Assert.IsNotNull(role.UniqueId);
            Assert.IsNotNull(role.Name);
            Assert.IsNotNull(role.Description);
            Assert.IsNotNull(role.Type);
            Assert.IsTrue(string.IsNullOrEmpty(role.PerformerId));
        }

        [TestMethod]
        public void SetRandomValuesAddsRoleType() {
            RoleTypes.Instance.Clear();
            Assert.AreEqual(0, RoleTypes.Instance.Count);
            // ReSharper disable once UnusedVariable
            var role = Role.Random();
            Assert.IsTrue(RoleTypes.Instance.Count > 0);
        }
    }
}