﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.RoleClasses;
namespace Open.Tests.Archetypes.RoleClasses {
    [TestClass]
    public class RoleTypeTests : CommonTests<RoleType> {
        protected override RoleType getRandomObj() { return RoleType.Random(); }

        [TestMethod]
        public void ConstraintsTest() {
            Assert.AreEqual(0, Obj.Constraints.Count);
            var l = RoleConstraints.Random();
            foreach (var e in l) e.RoleTypeId = Obj.UniqueId;
            RoleConstraints.Instance.AddRange(l);
            RoleConstraints.Instance.AddRange(RoleConstraints.Random());
            Assert.AreEqual(l.Count, Obj.Constraints.Count);
        }

        [TestMethod]
        public void TypeTest() {
            Assert.IsNull(Obj.Type);
            var t = RoleType.Random();
            t.UniqueId = Obj.TypeId;
            RoleTypes.Instance.AddRange(RoleTypes.Random());
            RoleTypes.Instance.Add(t);
            Assert.AreEqual(t, Obj.Type);
        }

        [TestMethod]
        public void SetName() {
            const string testName = "Name";
            var roleType = new RoleType {Name = testName};
            Assert.AreEqual(roleType.Name, testName);
        }

        [TestMethod]
        public void SetEmptyName() {
            var testName = string.Empty;
            var roleType = new RoleType {Name = testName};
            Assert.AreEqual(roleType.Name, testName);
        }

        [TestMethod]
        public void SetNullName() {
            var roleType = new RoleType {Name = null};
            Assert.AreEqual(roleType.Name, string.Empty);
        }

        [TestMethod]
        public void GetName() {
            var roleType = new RoleType();
            Assert.AreEqual(roleType.Name, string.Empty);
        }

        [TestMethod]
        public void SetDescription() {
            const string testDescription = "Description";
            var roleType = new RoleType {Description = testDescription};
            Assert.AreEqual(roleType.Description, testDescription);
        }

        [TestMethod]
        public void SetEmptyDescription() {
            var testDescription = string.Empty;
            var roleType = new RoleType {Name = testDescription};
            Assert.AreEqual(roleType.Name, testDescription);
        }

        [TestMethod]
        public void SetNullDescription() {
            var roleType = new RoleType {Description = null};
            Assert.AreEqual(roleType.Description, string.Empty);
        }

        [TestMethod]
        public void GetDescription() {
            var roleType = new RoleType();
            Assert.AreEqual(roleType.Description, string.Empty);
        }

        [TestMethod]
        public void IsActiveDefault() {
            var roleType = new RoleType();
            Assert.IsTrue(roleType.IsActive);
        }

        [TestMethod]
        public void IsActiveValidToHigher() {
            var roleType = new RoleType();
            roleType.Valid.From = DateTime.Now - TimeSpan.FromDays(2);
            roleType.Valid.To = DateTime.Now + TimeSpan.FromDays(12);
            Assert.IsTrue(roleType.IsActive);
        }

        [TestMethod]
        public void IsActiveValidToLower() {
            var roleType = new RoleType();
            roleType.Valid.From = DateTime.Now - TimeSpan.FromDays(2);
            roleType.Valid.To = DateTime.Now - TimeSpan.FromDays(1);
            Assert.IsFalse(roleType.IsActive);
        }

        [TestMethod]
        public void IsActiveValidFromHigher() {
            var roleType = new RoleType();
            roleType.Valid.From = DateTime.Now + TimeSpan.FromDays(2);
            roleType.Valid.To = DateTime.Now + TimeSpan.FromDays(12);
            Assert.IsFalse(roleType.IsActive);
        }
    }
}