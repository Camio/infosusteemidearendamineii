﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.RuleClasses;
namespace Open.Tests.Archetypes.RuleClasses {
    [TestClass] public class VariablesTests : CommonTests<Variables> {
        protected override Variables getRandomObj() {
            var v = new Variables();
            for (var i = 0; i < GetRandom.Count(); i++) v.Add(randomVariable());
            return v;
        }
        private static Operand randomVariable() {
            var i = GetRandom.UInt32();
            var c = i % 6;
            if (c == 0) return StringVariable.Random();
            if (c == 1) return BooleanVariable.Random();
            if (c == 2) return DateTimeVariable.Random();
            if (c == 3) return DoubleVariable.Random();
            if (c == 4) return IntegerVariable.Random();
            return DecimalVariable.Random();
        }
        [TestMethod] public void IsEmptyTest() {
            Assert.IsFalse(new Variables().IsEmpty());
            Assert.IsFalse(Obj.IsEmpty());
            Assert.IsTrue(Variables.Empty.IsEmpty());
        }
        [TestMethod] public void EmptyTest() { testSingleton(() => Variables.Empty); }
    }
}
