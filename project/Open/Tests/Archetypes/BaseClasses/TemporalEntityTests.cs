﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.BaseClasses;
namespace Open.Tests.Archetypes.BaseClasses
{
    [TestClass]
    public class TemporalEntityTests: ClassTests<TemporalEntity>
    {
        private class testClass: TemporalEntity { }
        [TestMethod]
        public void ConstructorTest()
        {
            var a = new testClass().GetType().BaseType.BaseType;
            Assert.AreEqual(a, typeof(Archetype));
        }
        [TestMethod] public void ValidTest() {
            TemporalEntity o = new testClass();
            testProperty(()=>o.Valid, x => o.Valid = x);
        }
    }
}
