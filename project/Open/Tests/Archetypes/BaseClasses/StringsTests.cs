﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.BaseClasses;
namespace Open.Tests.Archetypes.BaseClasses {
    [TestClass] public class StringsTests : CommonTests<Strings> {
        [TestMethod] public void ConstructorTest() {
            Assert.AreEqual(Obj.GetType().BaseType, typeof(Archetypes<string>));
        }
        protected override Strings getRandomObj() {
            return Strings.Random();
        }
        [TestMethod] public void ToListTest() {
            var l = Strings.ToList("A B C");
            Assert.AreEqual(3, l.Count);
            Assert.AreEqual("A", l[0]);
            Assert.AreEqual("B", l[1]);
            Assert.AreEqual("C", l[2]);
        }
        [TestMethod]
        public void ContentTest()
        {
            var l = new Strings {"A", "B", "C"};
            var s = l.Content;
            Assert.AreEqual("A B C", s);
        }
    }
}
