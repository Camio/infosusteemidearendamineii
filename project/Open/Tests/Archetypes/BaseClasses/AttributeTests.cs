﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.BaseClasses;
namespace Open.Tests.Archetypes.BaseClasses {
    [TestClass] public class AttributeTests : CommonTests<Attribute> {
        protected override Attribute getRandomObj() { return Attribute.Random(); }
        [TestMethod] public void EntityIdTest() {
            Obj = new Attribute();
            testProperty(() => Obj.EntityId, x => Obj.EntityId = x);
        }
        [TestMethod] public void TagsTest() {
            Obj = new Attribute();
            testProperty(() => Obj.Tags, x => Obj.Tags = x);
        }
    }
}
