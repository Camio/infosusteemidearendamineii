﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.BaseClasses;
namespace Open.Tests.Archetypes.BaseClasses {
    [TestClass] public class NamedItemTests : CommonTests<NamedItem> {
        [TestMethod] public void ConstructorTest() {
            Assert.AreEqual(Obj.GetType().BaseType,
                typeof(UniqueEntity));
        }
        [TestMethod] public void CodeTest() {
            Obj = new NamedItem();
            testProperty(() => Obj.Symbol, x => Obj.Symbol = x);
        }
        [TestMethod] public void NameTest() {
            Obj = new NamedItem();
            testProperty(() => Obj.Name, x => Obj.Name = x);
        }
        [TestMethod] public void NoteTest() {
            Obj = new NamedItem();
            testProperty(() => Obj.Description, x => Obj.Description = x);
        }
        [TestMethod] public void EmptyTest() {
            testSingleton(()=> NamedItem.Empty);
        }
        protected override NamedItem getRandomObj() {
            return NamedItem.Random();
        }
    }
}
