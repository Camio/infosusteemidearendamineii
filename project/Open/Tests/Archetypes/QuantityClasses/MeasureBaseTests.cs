﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class MeasureBaseTests : CommonTests<MeasureBase> {
        protected string elementPattern => "({0})";
        protected string multiplyPattern => "{0}*{1}";
        protected string powerPattern => "{0}^{1}";
        protected override MeasureBase getRandomObj() {
            return MeasureBase.Random();
        }
        [ClassInitialize] public static void ClassInitialize(TestContext c) {
            Measures.Instance.Clear();
            for (var i = 0; i < 100; i++)
                Measures.Add(GetRandom.String());
        }
        private static Measure createMeasureDerived() {
            var m1 = Measures.Add("measure1", "m1");
            var m2 = Measures.Add("measure2", "m2");
            var t1 = new MeasureTerm(m1, 2);
            var t2 = new MeasureTerm(m2, -1);
            var l = new List<MeasureTerm> {t1, t2};
            var terms = new MeasureTerms(l);
            return new MeasureDerived(terms, "measure3", "m3");
        }
        [TestMethod] public void CreateTest() {
            var name = GetRandom.String(3, 5);
            var m = Measures.Add(name);
            Assert.IsInstanceOfType(m, typeof(MeasureBase));
            Assert.IsNotInstanceOfType(m, typeof(MeasureDerived));
            Assert.AreEqual(m, Measures.Find(m.UniqueId));
            Assert.AreEqual(m, Measures.Instance.Find(o => o.Name == name));
        }
        [TestMethod] public void FormulaTest() {
            var name = Obj.Name;
            var symbol = Obj.Symbol;
            var s = string.Format(Metric.FormulaPattern, name);
            Assert.AreEqual(s, Obj.Formula(true));
            s = string.Format(Metric.FormulaPattern, symbol);
            Assert.AreEqual(s, Obj.Formula());
            Obj.Symbol = string.Empty;
            Obj.Name = string.Empty;
            Assert.AreEqual(string.Empty, Obj.Formula(true));
            Assert.AreEqual(string.Empty, Obj.Formula());
        }
        [TestMethod] public void IsSameFormulaTest() {
            var a = createMeasureDerived();
            var b = createMeasureDerived();
            Assert.IsTrue(a.IsSameFormula(b));
            Assert.IsFalse(Obj.IsSameFormula(a));
            Assert.IsTrue(new MeasureBase().IsSameFormula(new MeasureBase()));
            Assert.IsFalse(new MeasureBase().IsSameFormula(null));
            var t = MeasureTerms.Random();
            Assert.IsTrue(
                new MeasureDerived(t).IsSameFormula(new MeasureDerived(t)));
        }
        [TestMethod] public void IsThisTest() {
            var a = createMeasureDerived();
            Assert.IsTrue(a.IsThis(a.Name));
            Assert.IsTrue(a.IsThis(a.Symbol));
            Assert.IsTrue(a.IsThis(a.Formula()));
            Assert.IsTrue(a.IsThis(a.Formula(true)));
            Assert.IsFalse(a.IsThis(null));
            Assert.IsFalse(a.IsThis("  "));
            Assert.IsFalse(a.IsThis(GetRandom.String()));
        }
        [TestMethod]
        public void GetUnitsTest()
        {
            Measures.Instance.Add(Obj);
            var u = Units.Random();
            var v = Units.Random();
            foreach (var e in u) e.Measure = Obj.Name;
            Units.Instance.AddRange(u);
            Units.Instance.AddRange(v);
            var units = Obj.GetUnits();
            Assert.IsNotNull(units);
            Assert.AreEqual(u.Count, units.Count);
        }
        [TestMethod]
        public void IsValidQuantityTest()
        {
            var u = UnitBase.Random();
            var q = new Quantity(u, GetRandom.Double());
            Measures.Instance.Add(Obj);
            Assert.IsFalse(Obj.IsValidQuantity(q));
            GetUnitsTest();
            u.Measure = Obj.Name;
            Units.Instance.Add(u);
            Assert.IsTrue(Obj.IsValidQuantity(q));
        }
    }
}
