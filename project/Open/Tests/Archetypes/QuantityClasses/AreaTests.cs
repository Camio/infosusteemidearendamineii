﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class AreaTests : DefinedUnitsTests {
        [TestInitialize] public override void TestInitialize() {
            base.TestInitialize();
            type = typeof(Area);
            measure = Area.Measure;
        }
        [TestMethod] public void MeasureTest() {
            var m = Measures.Instance.Find(x => x.Name == Area.MeasureName) as MeasureDerived;
            Assert.AreEqual(Area.Measure, m);
            var terms = m.Terms;
            Assert.AreEqual(1, terms.Count);
            var term = terms[0];
            Assert.AreEqual(2, term.Power);
            Assert.AreEqual(Distance.Measure, term.GetMeasure);
        }
        private static UnitTerms getTerms(Unit u) {
            if (ReferenceEquals(null, u)) return UnitTerms.Empty;
            var d = u.Multiply(u) as UnitDerived;
            return ReferenceEquals(null, d) ? UnitTerms.Empty : d.Terms;
        }
        [TestMethod] public void SquareMetersTest() {
            testUnitDerived(Area.SquareMeters, Area.SquareMetersName, getTerms(Distance.Meters), 1.0);
        }
        [TestMethod] public void SquareMillimetersTest() {
            testUnitDerived(Area.SquareMillimeters, Area.SquareMillimetersName,
                getTerms(Distance.Millimeters), 1000000.0);
        }
        [TestMethod] public void SquareCentimetersTest() {
            testUnitDerived(Area.SquareCentimeters, Area.SquareCentimetersName,
                getTerms(Distance.Centimeters), 10000.0);
        }
        [TestMethod] public void SquareDecimetersTest() {
            testUnitDerived(Area.SquareDecimeters, Area.SquareDecimetersName,
                getTerms(Distance.Decimeters), 100.0);
        }
        [TestMethod] public void SquareDecametersTest() {
            testUnitDerived(Area.SquareDecameters, Area.SquareDecametersName,
                getTerms(Distance.Decameters), 0.01);
        }
        [TestMethod] public void SquareHectometersTest() {
            testUnitDerived(Area.SquareHectometers, Area.SquareHectometersName,
                getTerms(Distance.Hectometers), 0.0001);
        }
        [TestMethod] public void SquareKilometersTest() {
            testUnitDerived(Area.SquareKilometers, Area.SquareKilometersName,
                getTerms(Distance.Kilometers), 0.000001);
        }
        [TestMethod] public void SquareInchesTest() {
            const double f = Distance.InchesFactor*Distance.InchesFactor;
            testUnitDerived(Area.SquareInches, Area.SquareInchesName, getTerms(Distance.Inches),
                1.0/f);
        }
        [TestMethod] public void SquareFeetTest() {
            const double f = Distance.FeetFactor*Distance.FeetFactor;
            testUnitDerived(Area.SquareFeet, Area.SquareFeetName, getTerms(Distance.Feet), 1.0/f);
        }
        [TestMethod] public void SquareYardsTest() {
            const double f = Distance.YardsFactor*Distance.YardsFactor;
            testUnitDerived(Area.SquareYards, Area.SquareYardsName, getTerms(Distance.Yards), 1.0/f);
        }
        [TestMethod] public void SquareMilesTest() {
            const double f = Distance.MilesFactor*Distance.MilesFactor;
            testUnitDerived(Area.SquareMiles, Area.SquareMilesName, getTerms(Distance.Miles), 1.0/f);
        }
        [TestMethod] public void AcresTest() {
            const double f = Distance.FurlongsFactor*Distance.ChainsFactor;
            var terms = Distance.Furlongs.Multiply(Distance.Chains) as UnitDerived;
            Assert.IsNotNull(terms);
            testUnitDerived(Area.Acres, Area.AcresName, terms.Terms, 1.0/f);
        }
        [TestMethod] public void CentiaresTest() {
            testUnitDerived(Area.Centiares, Area.CentiaresName, getTerms(Distance.Meters), 1.0);
        }
        [TestMethod] public void AresTest() {
            testUnitDerived(Area.Ares, Area.AresName, getTerms(Distance.Decameters), 0.01);
        }
        [TestMethod] public void HectaresTest() {
            testUnitDerived(Area.Hectares, Area.HectaresName, getTerms(Distance.Hectometers), 0.0001);
        }
        [TestMethod] public void SquareRodsTest() {
            const double f = Distance.RodsFactor*Distance.RodsFactor;
            testUnitDerived(Area.SquareRods, Area.SquareRodsName, getTerms(Distance.Rods), 1.0/f);
        }
        [TestMethod] public void MeasureNameTest() { Assert.AreEqual("Area", Area.MeasureName); }
        [TestMethod] public void SquareMillimetersNameTest() {
            Assert.AreEqual("SquareMillimeters", Area.SquareMillimetersName);
        }
        [TestMethod] public void SquareCentimetersNameTest() {
            Assert.AreEqual("SquareCentimeters", Area.SquareCentimetersName);
        }
        [TestMethod] public void SquareDecimetersNameTest() {
            Assert.AreEqual("SquareDecimeters", Area.SquareDecimetersName);
        }
        [TestMethod] public void SquareMetersNameTest() {
            Assert.AreEqual("SquareMeters", Area.SquareMetersName);
        }
        [TestMethod] public void SquareDecametersNameTest() {
            Assert.AreEqual("SquareDecameters", Area.SquareDecametersName);
        }
        [TestMethod] public void SquareHectometersNameTest() {
            Assert.AreEqual("SquareHectometers", Area.SquareHectometersName);
        }
        [TestMethod] public void SquareKilometersNameTest() {
            Assert.AreEqual("SquareKilometers", Area.SquareKilometersName);
        }
        [TestMethod] public void SquareInchesNameTest() {
            Assert.AreEqual("SquareInches", Area.SquareInchesName);
        }
        [TestMethod] public void SquareFeetNameTest() {
            Assert.AreEqual("SquareFeet", Area.SquareFeetName);
        }
        [TestMethod] public void SquareYardsNameTest() {
            Assert.AreEqual("SquareYards", Area.SquareYardsName);
        }
        [TestMethod] public void SquareMilesNameTest() {
            Assert.AreEqual("SquareMiles", Area.SquareMilesName);
        }
        [TestMethod] public void AcresNameTest() { Assert.AreEqual("Acres", Area.AcresName); }
        [TestMethod] public void CentiaresNameTest() {
            Assert.AreEqual("Centares", Area.CentiaresName);
        }
        [TestMethod] public void AresNameTest() { Assert.AreEqual("Ares", Area.AresName); }
        [TestMethod] public void HectaresNameTest() {
            Assert.AreEqual("Hectares", Area.HectaresName);
        }
        [TestMethod] public void SquareRodsNameTest() {
            Assert.AreEqual("SquareRods", Area.SquareRodsName);
        }
        [TestMethod] public void InitializeTest() {
            Measures.Instance.Clear();
            Units.Instance.Clear();
            Assert.IsTrue(Area.Initialize());
            Assert.AreEqual(2, Measures.Instance.Count);
            Assert.IsNotNull(Measures.Find(Area.MeasureName));
            Assert.IsNotNull(Measures.Find(Distance.MeasureName));
            Assert.AreEqual(30, Units.Instance.Count);
            testUnit(Area.SquareMillimetersName);
            testUnit(Area.SquareCentimetersName);
            testUnit(Area.SquareDecimetersName);
            testUnit(Area.SquareMetersName);
            testUnit(Area.SquareDecametersName);
            testUnit(Area.SquareHectometersName);
            testUnit(Area.SquareKilometersName);
            testUnit(Area.SquareInchesName);
            testUnit(Area.SquareFeetName);
            testUnit(Area.SquareYardsName);
            testUnit(Area.SquareMilesName);
            testUnit(Area.AcresName);
            testUnit(Area.CentiaresName);
            testUnit(Area.AresName);
            testUnit(Area.HectaresName);
            testUnit(Area.SquareRodsName);
            testUnit(Distance.MetersName);
            testUnit(Distance.MillimetersName);
            testUnit(Distance.CentimetersName);
            testUnit(Distance.DecimetersName);
            testUnit(Distance.DecametersName);
            testUnit(Distance.HectometersName);
            testUnit(Distance.KilometersName);
            testUnit(Distance.InchesName);
            testUnit(Distance.FeetName);
            testUnit(Distance.YardsName);
            testUnit(Distance.MilesName);
            testUnit(Distance.FurlongsName);
            testUnit(Distance.ChainsName);
            testUnit(Distance.RodsName);
        }
    }
}
