﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class DistanceTests : DefinedUnitsTests {
        [TestInitialize] public override void TestInitialize() {
            type = typeof(Distance);
            measure = Distance.Measure;
        }
        [TestMethod] public void MeasureTest() { testBaseMeasure(Distance.MeasureName); }
        [TestMethod] public void MetersTest() {
            testBaseUnit(Distance.Meters, Distance.MetersName);
        }
        [TestMethod] public void MicromicronsTest() {
            testFactoredUnit(Distance.Micromicrons, Distance.MicromicronsName, Factors.Micromicro);
        }
        [TestMethod] public void AngstromsTest() {
            testFactoredUnit(Distance.Angstroms, Distance.AngstromsName, Factors.Angstrom);
        }
        [TestMethod] public void NanometersTest() {
            testFactoredUnit(Distance.Nanometers, Distance.NanometersName, Factors.Nano);
        }
        [TestMethod] public void MillimicronsTest() {
            testFactoredUnit(Distance.Millimicrons, Distance.MillimicronsName, Factors.Millimicro);
        }
        [TestMethod] public void MicronsTest() {
            testFactoredUnit(Distance.Microns, Distance.MicronsName, Factors.Micro);
        }
        [TestMethod] public void MillimetersTest() {
            testFactoredUnit(Distance.Millimeters, Distance.MillimetersName, Factors.Milli);
        }
        [TestMethod] public void CentimetersTest() {
            testFactoredUnit(Distance.Centimeters, Distance.CentimetersName, Factors.Centi);
        }
        [TestMethod] public void DecimetersTest() {
            testFactoredUnit(Distance.Decimeters, Distance.DecimetersName, Factors.Deci);
        }
        [TestMethod] public void DecametersTest() {
            testFactoredUnit(Distance.Decameters, Distance.DecametersName, Factors.Deca);
        }
        [TestMethod] public void HectometersTest() {
            testFactoredUnit(Distance.Hectometers, Distance.HectometersName, Factors.Hecto);
        }
        [TestMethod] public void KilometersTest() {
            testFactoredUnit(Distance.Kilometers, Distance.KilometersName, Factors.Kilo);
        }
        [TestMethod] public void MegametersTest() {
            testFactoredUnit(Distance.Megameters, Distance.MegametersName, Factors.Mega);
        }
        [TestMethod] public void GigametersTest() {
            testFactoredUnit(Distance.Gigameters, Distance.GigametersName, Factors.Giga);
        }
        [TestMethod] public void InchesTest() {
            testFactoredUnit(Distance.Inches, Distance.InchesName, Distance.InchesFactor);
        }
        [TestMethod] public void FeetTest() {
            testFactoredUnit(Distance.Feet, Distance.FeetName, Distance.FeetFactor);
        }
        [TestMethod] public void YardsTest() {
            testFactoredUnit(Distance.Yards, Distance.YardsName, Distance.YardsFactor);
        }
        [TestMethod] public void MilesTest() {
            testFactoredUnit(Distance.Miles, Distance.MilesName, Distance.MilesFactor);
        }
        [TestMethod] public void NauticalMilesTest() {
            testFactoredUnit(Distance.NauticalMiles, Distance.NauticalMilesName,
                Distance.NauticalMilesFactor);
        }
        [TestMethod] public void AstronomicalUnitTest() {
            testFactoredUnit(Distance.AstronomicalUnit, Distance.AstronomicalUnitsName,
                Distance.AstronomicalUnitsFactor);
        }
        [TestMethod] public void LightYearsTest() {
            testFactoredUnit(Distance.LightYears, Distance.LightYearsName, Distance.LightYearsFactor);
        }
        [TestMethod] public void LightSecondsTest() {
            testFactoredUnit(Distance.LightSeconds, Distance.LightSecondsName,
                Distance.LightSecondsFactor);
        }
        [TestMethod] public void ParsecsTest() {
            testFactoredUnit(Distance.Parsecs, Distance.ParsecsName, Distance.ParsecsFactor);
        }
        [TestMethod] public void CubitsTest() {
            testFactoredUnit(Distance.Cubits, Distance.CubitsName, Distance.CubitsFactor);
        }
        [TestMethod] public void FathomsTest() {
            testFactoredUnit(Distance.Fathoms, Distance.FathomsName, Distance.FathomsFactor);
        }
        [TestMethod] public void FurlongsTest() {
            testFactoredUnit(Distance.Furlongs, Distance.FurlongsName, Distance.FurlongsFactor);
        }
        [TestMethod] public void HandsTest() {
            testFactoredUnit(Distance.Hands, Distance.HandsName, Distance.HandsFactor);
        }
        [TestMethod] public void PacesTest() {
            testFactoredUnit(Distance.Paces, Distance.PacesName, Distance.PacesFactor);
        }
        [TestMethod] public void RodsTest() {
            testFactoredUnit(Distance.Rods, Distance.RodsName, Distance.RodsFactor);
        }
        [TestMethod] public void ChainsTest() {
            testFactoredUnit(Distance.Chains, Distance.ChainsName, Distance.ChainsFactor);
        }
        [TestMethod] public void LinksTest() {
            testFactoredUnit(Distance.Links, Distance.LinksName, Distance.LinksFactor);
        }
        [TestMethod] public void PicasTest() {
            testFactoredUnit(Distance.Picas, Distance.PicasName, Distance.PicasFactor);
        }
        [TestMethod] public void PointsTest() {
            testFactoredUnit(Distance.Points, Distance.PointsName, Distance.PointsFactor);
        }
        [TestMethod] public void MeasureNameTest() {
            Assert.AreEqual("Distance", Distance.MeasureName);
        }
        [TestMethod] public void MetersNameTest() {
            Assert.AreEqual("Meters", Distance.MetersName);
        }
        [TestMethod] public void MicromicronsNameTest() {
            Assert.AreEqual("Micromicrons", Distance.MicromicronsName);
        }
        [TestMethod] public void AngstromsNameTest() {
            Assert.AreEqual("Angstroms", Distance.AngstromsName);
        }
        [TestMethod] public void NanometersNameTest() {
            Assert.AreEqual("Nanometers", Distance.NanometersName);
        }
        [TestMethod] public void MillimicronsNameTest() {
            Assert.AreEqual("Millimicrons", Distance.MillimicronsName);
        }
        [TestMethod] public void MicronsNameTest() {
            Assert.AreEqual("Microns", Distance.MicronsName);
        }
        [TestMethod] public void MillimetersNameTest() {
            Assert.AreEqual("Millimeters", Distance.MillimetersName);
        }
        [TestMethod] public void CentimetersNameTest() {
            Assert.AreEqual("Centimeters", Distance.CentimetersName);
        }
        [TestMethod] public void DecimetersNameTest() {
            Assert.AreEqual("Decimeters", Distance.DecimetersName);
        }
        [TestMethod] public void DecametersNameTest() {
            Assert.AreEqual("Decameters", Distance.DecametersName);
        }
        [TestMethod] public void HectometersNameTest() {
            Assert.AreEqual("Hectometers", Distance.HectometersName);
        }
        [TestMethod] public void KilometersNameTest() {
            Assert.AreEqual("Kilometers", Distance.KilometersName);
        }
        [TestMethod] public void MegametersNameTest() {
            Assert.AreEqual("Megameters", Distance.MegametersName);
        }
        [TestMethod] public void GigametersNameTest() {
            Assert.AreEqual("Gigameters", Distance.GigametersName);
        }
        [TestMethod] public void InchesNameTest() {
            Assert.AreEqual("Inches", Distance.InchesName);
        }
        [TestMethod] public void FeetNameTest() { Assert.AreEqual("Feet", Distance.FeetName); }
        [TestMethod] public void YardsNameTest() { Assert.AreEqual("Yards", Distance.YardsName); }
        [TestMethod] public void MilesNameTest() { Assert.AreEqual("Miles", Distance.MilesName); }
        [TestMethod] public void NauticalMilesNameTest() {
            Assert.AreEqual("NauticalMiles", Distance.NauticalMilesName);
        }
        [TestMethod] public void AstronomicalUnitsNameTest() {
            Assert.AreEqual("AstronomicalUnits", Distance.AstronomicalUnitsName);
        }
        [TestMethod] public void LightYearsNameTest() {
            Assert.AreEqual("LightYears", Distance.LightYearsName);
        }
        [TestMethod] public void LightSecondsNameTest() {
            Assert.AreEqual("LightSeconds", Distance.LightSecondsName);
        }
        [TestMethod] public void ParsecsNameTest() {
            Assert.AreEqual("Parsecs", Distance.ParsecsName);
        }
        [TestMethod] public void CubitsNameTest() {
            Assert.AreEqual("Cubits", Distance.CubitsName);
        }
        [TestMethod] public void FathomsNameTest() {
            Assert.AreEqual("Fathoms", Distance.FathomsName);
        }
        [TestMethod] public void FurlongsNameTest() {
            Assert.AreEqual("Furlongs", Distance.FurlongsName);
        }
        [TestMethod] public void HandsNameTest() { Assert.AreEqual("Hands", Distance.HandsName); }
        [TestMethod] public void PacesNameTest() { Assert.AreEqual("Paces", Distance.PacesName); }
        [TestMethod] public void RodsNameTest() { Assert.AreEqual("Rods", Distance.RodsName); }
        [TestMethod] public void ChainsNameTest() {
            Assert.AreEqual("Chains", Distance.ChainsName);
        }
        [TestMethod] public void LinksNameTest() { Assert.AreEqual("Links", Distance.LinksName); }
        [TestMethod] public void PicasNameTest() { Assert.AreEqual("Picas", Distance.PicasName); }
        [TestMethod] public void PointsNameTest() {
            Assert.AreEqual("Points", Distance.PointsName);
        }
        [TestMethod] public void InchesFactorTest() {
            Assert.AreEqual(0.0254, Distance.InchesFactor);
        }
        [TestMethod] public void FeetFactorTest() {
            Assert.AreEqual(Distance.InchesFactor * 12.0, Distance.FeetFactor);
        }
        [TestMethod] public void YardsFactorTest() {
            Assert.AreEqual(Distance.FeetFactor * 3.0, Distance.YardsFactor);
        }
        [TestMethod] public void MilesFactorTest() {
            Assert.AreEqual(Distance.FeetFactor * 5280, Distance.MilesFactor);
        }
        [TestMethod] public void NauticalMilesFactorTest() {
            Assert.AreEqual(1852.0, Distance.NauticalMilesFactor);
        }
        [TestMethod] public void AstronomicalUnitsFactorTest() {
            Assert.AreEqual(1.49598E11, Distance.AstronomicalUnitsFactor);
        }
        [TestMethod] public void LightSecondsFactorTest() {
            Assert.AreEqual(2.99792458E8, Distance.LightSecondsFactor);
        }
        [TestMethod] public void LightYearsFactorTest() {
            Assert.AreEqual(Distance.LightSecondsFactor * 31556925.9747, Distance.LightYearsFactor);
        }
        [TestMethod] public void ParsecsFactorTest() {
            Assert.AreEqual(Distance.AstronomicalUnitsFactor * 206264.806247096,
                Distance.ParsecsFactor);
        }
        [TestMethod] public void CubitsFactorTest() {
            Assert.AreEqual(0.4572, Distance.CubitsFactor);
        }
        [TestMethod] public void FathomsFactorTest() {
            Assert.AreEqual(Distance.FeetFactor * 6.0, Distance.FathomsFactor);
        }
        [TestMethod] public void FurlongsFactorTest() {
            Assert.AreEqual(Distance.YardsFactor * 220.0, Distance.FurlongsFactor);
        }
        [TestMethod] public void HandsFactorTest() {
            Assert.AreEqual(Distance.InchesFactor * 4.0, Distance.HandsFactor);
        }
        [TestMethod] public void PacesFactorTest() {
            Assert.AreEqual(Distance.InchesFactor * 30.0, Distance.PacesFactor);
        }
        [TestMethod] public void RodsFactorTest() {
            Assert.AreEqual(Distance.FeetFactor * 16.5, Distance.RodsFactor);
        }
        [TestMethod] public void ChainsFactorTest() {
            Assert.AreEqual(Distance.RodsFactor * 4.0, Distance.ChainsFactor);
        }
        [TestMethod] public void LinksFactorTest() {
            Assert.AreEqual(Distance.ChainsFactor / 100.0, Distance.LinksFactor);
        }
        [TestMethod] public void PointsFactorTest() {
            Assert.AreEqual(Distance.InchesFactor * 0.013837, Distance.PointsFactor);
        }
        [TestMethod] public void PicasFactorTest() {
            Assert.AreEqual(Distance.PointsFactor * 12.0, Distance.PicasFactor);
        }
        [TestMethod] public void InitializeTest() {
            Measures.Instance.Clear();
            Units.Instance.Clear();
            Assert.IsTrue(Distance.Initialize());
            Assert.AreEqual(1, Measures.Instance.Count);
            Assert.IsNotNull(Measures.Find(Distance.MeasureName));
            Assert.AreEqual(33, Units.Instance.Count);
            testUnit(Distance.MicromicronsName);
            testUnit(Distance.AngstromsName);
            testUnit(Distance.NanometersName);
            testUnit(Distance.MillimicronsName);
            testUnit(Distance.MicronsName);
            testUnit(Distance.MillimetersName);
            testUnit(Distance.CentimetersName);
            testUnit(Distance.DecimetersName);
            testUnit(Distance.DecametersName);
            testUnit(Distance.HectometersName);
            testUnit(Distance.KilometersName);
            testUnit(Distance.MegametersName);
            testUnit(Distance.GigametersName);
            testUnit(Distance.InchesName);
            testUnit(Distance.FeetName);
            testUnit(Distance.YardsName);
            testUnit(Distance.MilesName);
            testUnit(Distance.NauticalMilesName);
            testUnit(Distance.AstronomicalUnitsName);
            testUnit(Distance.LightYearsName);
            testUnit(Distance.LightSecondsName);
            testUnit(Distance.ParsecsName);
            testUnit(Distance.CubitsName);
            testUnit(Distance.FathomsName);
            testUnit(Distance.FurlongsName);
            testUnit(Distance.HandsName);
            testUnit(Distance.PacesName);
            testUnit(Distance.RodsName);
            testUnit(Distance.ChainsName);
            testUnit(Distance.LinksName);
            testUnit(Distance.PicasName);
            testUnit(Distance.PointsName);
        }
    }
}
