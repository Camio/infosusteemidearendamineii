﻿//using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Open.Archetypes.CommonClasses;
//namespace Open.Tests.Archetypes.CommonClassses {
//    [TestClass] public class RoundOffTests : Utils.CommonTests {
//        [TestInitialize] public override void TestInitialize() { TestItemType = typeof (RoundOff); }
//        [TestMethod] public void UpTest() {
//            Assert.AreEqual(4.5, RoundOff.Up(4.45, 1));
//            Assert.AreEqual(4.46, RoundOff.Up(4.456, 2));
//            Assert.AreEqual(-4.5, RoundOff.Up(-4.45, 1));
//            Assert.AreEqual(-4.46, RoundOff.Up(-4.456, 2));
//            Assert.AreEqual(1400, RoundOff.Up(1400.00, 2));
//        }
//        [TestMethod] public void DownTest() {
//            Assert.AreEqual(4.4, RoundOff.Down(4.45, 1));
//            Assert.AreEqual(4.45, RoundOff.Down(4.456, 2));
//            Assert.AreEqual(-4.4, RoundOff.Down(-4.45, 1));
//            Assert.AreEqual(-4.45, RoundOff.Down(-4.456, 2));
//            Assert.AreEqual(1400, RoundOff.Down(1400.00, 2));
//        }
//        [TestMethod] public void OffTest() {
//            Assert.AreEqual(4.5, RoundOff.Off(4.45, 1, 5));
//            Assert.AreEqual(4.45, RoundOff.Off(4.456, 2, 7));
//            Assert.AreEqual(-4.5, RoundOff.Off(-4.45, 1, 5));
//            Assert.AreEqual(-4.45, RoundOff.Off(-4.456, 2, 7));
//            Assert.AreEqual(0.01, RoundOff.Off(0.01, 5, 5));
//            Assert.AreEqual(0.01, RoundOff.Off(0.0100000000002, 5, 5));
//            Assert.AreEqual(1, RoundOff.Off(0.99999999999999989, 0, 5));
//        }
//        [TestMethod] public void UpByStepTest() {
//            Assert.AreEqual(4.5, RoundOff.UpByStep(4.45, 0.25));
//            Assert.AreEqual(-4.5, RoundOff.UpByStep(-4.45, 0.25));
//        }
//        [TestMethod] public void DownByStepTest() {
//            Assert.AreEqual(4.25, RoundOff.DownByStep(4.45, 0.25));
//            Assert.AreEqual(-4.25, RoundOff.DownByStep(-4.45, 0.25));
//        }
//        [TestMethod] public void TowardsPositiveTest() {
//            Assert.AreEqual(4.5, RoundOff.TowardsPositive(4.45, 1));
//            Assert.AreEqual(4.46, RoundOff.TowardsPositive(4.456, 2));
//            Assert.AreEqual(-4.4, RoundOff.TowardsPositive(-4.45, 1));
//            Assert.AreEqual(-4.45, RoundOff.TowardsPositive(-4.456, 2));
//        }
//        [TestMethod] public void TowardsNegativeTest() {
//            Assert.AreEqual(4.4, RoundOff.TowardsNegative(4.45, 1));
//            Assert.AreEqual(4.45, RoundOff.TowardsNegative(4.456, 2));
//            Assert.AreEqual(-4.5, RoundOff.TowardsNegative(-4.45, 1));
//            Assert.AreEqual(-4.46, RoundOff.TowardsNegative(-4.456, 2));
//        }
//        [TestMethod]
//        public void DecimalUpTest()
//        {
//            Assert.AreEqual(4.5M, RoundOff.Up(4.45M, 1));
//            Assert.AreEqual(4.46M, RoundOff.Up(4.456M, 2));
//            Assert.AreEqual(-4.5M, RoundOff.Up(-4.45M, 1));
//            Assert.AreEqual(-4.46M, RoundOff.Up(-4.456M, 2));
//        }
//        [TestMethod]
//        public void DecimalDownTest()
//        {
//            Assert.AreEqual(4.4M, RoundOff.Down(4.45M, 1));
//            Assert.AreEqual(4.45M, RoundOff.Down(4.456M, 2));
//            Assert.AreEqual(-4.4M, RoundOff.Down(-4.45M, 1));
//            Assert.AreEqual(-4.45M, RoundOff.Down(-4.456M, 2));
//        }
//        [TestMethod]
//        public void DecimalOffTest()
//        {
//            Assert.AreEqual(4.5M, RoundOff.Off(4.45M, 1, 5));
//            Assert.AreEqual(4.45M, RoundOff.Off(4.456M, 2, 7));
//            Assert.AreEqual(-4.5M, RoundOff.Off(-4.45M, 1, 5));
//            Assert.AreEqual(-4.45M, RoundOff.Off(-4.456M, 2, 7));
//            Assert.AreEqual(0.01M, RoundOff.Off(0.01M, 5, 5));
//            Assert.AreEqual(0.01M, RoundOff.Off(0.0100000000002M, 5, 5));
//            Assert.AreEqual(1M, RoundOff.Off(0.99999999999999989M, 0, 5));
//        }
//        [TestMethod]
//        public void DecimalUpByStepTest()
//        {
//            Assert.AreEqual(4.5M, RoundOff.UpByStep(4.45M, 0.25));
//            Assert.AreEqual(-4.5M, RoundOff.UpByStep(-4.45M, 0.25));
//        }
//        [TestMethod]
//        public void DecimalDownByStepTest()
//        {
//            Assert.AreEqual(4.25M, RoundOff.DownByStep(4.45M, 0.25));
//            Assert.AreEqual(-4.25M, RoundOff.DownByStep(-4.45M, 0.25));
//        }
//        [TestMethod]
//        public void DecimalTowardsPositiveTest()
//        {
//            Assert.AreEqual(4.5M, RoundOff.TowardsPositive(4.45M, 1));
//            Assert.AreEqual(4.46M, RoundOff.TowardsPositive(4.456M, 2));
//            Assert.AreEqual(-4.4M, RoundOff.TowardsPositive(-4.45M, 1));
//            Assert.AreEqual(-4.45M, RoundOff.TowardsPositive(-4.456M, 2));
//        }
//        [TestMethod]
//        public void DecimalTowardsNegativeTest()
//        {
//            Assert.AreEqual(4.4M, RoundOff.TowardsNegative(4.45M, 1));
//            Assert.AreEqual(4.45M, RoundOff.TowardsNegative(4.456M, 2));
//            Assert.AreEqual(-4.5M, RoundOff.TowardsNegative(-4.45M, 1));
//            Assert.AreEqual(-4.46M, RoundOff.TowardsNegative(-4.456M, 2));
//        }
//        [TestMethod]
//        public void RoundTest()
//        {
//            Func<double, RoundingStrategyCode, sbyte, byte, double, double>
//                round = (x, strategy, percition, digits, step) =>
//                {
//                    var p = new RoundingPolicy(strategy, percition, digits, step);
//                    switch (p.Strategy)
//                    {
//                        case RoundingStrategyCode.Up: return RoundOff.Up(x, p.Decimals);
//                        case RoundingStrategyCode.Down: return RoundOff.Down(x, p.Decimals);
//                        case RoundingStrategyCode.UpByStep: return RoundOff.UpByStep(x, p.Step);
//                        case RoundingStrategyCode.DownByStep: return RoundOff.DownByStep(x, p.Step);
//                        case RoundingStrategyCode.TowardsPositive: return RoundOff.TowardsPositive(x, p.Decimals);
//                        case RoundingStrategyCode.TowardsNegative: return RoundOff.TowardsNegative(x, p.Decimals);
//                        default: return RoundOff.Off(x, p.Decimals, p.RoundingDigit);
//                    }
//                };
//            Assert.AreEqual(4.5, round(4.45, RoundingStrategyCode.Up, 1, RandomValue.UInt8(), RandomValue.Double()));
//            Assert.AreEqual(-4.45, round(-4.456, RoundingStrategyCode.Down, 2, RandomValue.UInt8(), RandomValue.Double()));
//            Assert.AreEqual(-4.5, round(-4.45, RoundingStrategyCode.UpByStep, RandomValue.Int8(), RandomValue.UInt8(), 0.25));
//            Assert.AreEqual(4.25, round(4.45, RoundingStrategyCode.DownByStep, RandomValue.Int8(), RandomValue.UInt8(), 0.25));
//            Assert.AreEqual(-4.5, round(-4.45, RoundingStrategyCode.TowardsNegative, 1, RandomValue.UInt8(), RandomValue.Double()));
//            Assert.AreEqual(4.46, round(4.456, RoundingStrategyCode.TowardsPositive, 2, RandomValue.UInt8(), RandomValue.Double()));
//            Assert.AreEqual(4.45, round(4.456, RoundingStrategyCode.Round, 2, 7, RandomValue.Double()));
//        }
//    }
//}
