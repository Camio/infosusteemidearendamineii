﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class UnitFactoredTests : Tests<UnitFactored> {
        [TestMethod] public void FactorTest() {
            Obj.Factor = 0.0;
            DoublePropertyTest(() => Obj.Factor, x => Obj.Factor = x);
        }
        [TestMethod] public void FromBaseTest() {
            var d = RandomValue.Double();
            Assert.AreEqual(d/Obj.Factor, Obj.FromBase(d));
        }
        protected override UnitFactored GetRandomObj() {
            return UnitFactored.Random();
        }
        [TestMethod] public void ToBaseTest() {
            var d = RandomValue.Double();
            Assert.AreEqual(d*Obj.Factor, Obj.ToBase(d));
        }
    }
}
