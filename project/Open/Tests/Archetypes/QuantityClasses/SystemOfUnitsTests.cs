﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class SystemOfUnitsTests : CommonTests<SystemOfUnits> {
        protected override SystemOfUnits getRandomObj() { return SystemOfUnits.Random(); }
        [TestMethod] public void ConstructorTest() {
            Func<string> x = () => GetRandom.String();
            Action<SystemOfUnits> test = o => { Assert.AreEqual(o.GetType().BaseType, typeof(Metric)); };
            test(new SystemOfUnits());
            test(new SystemOfUnits(x()));
            test(new SystemOfUnits(x(), x()));
            test(new SystemOfUnits(x(), x(), x()));
        }
        [TestMethod] public void EmptyTest() { testSingleton(() => SystemOfUnits.Empty); }
        [TestMethod] public void IsEmptyTest() {
            Assert.IsTrue(SystemOfUnits.Empty.IsEmpty());
            Assert.IsFalse(SystemOfUnits.Random().IsEmpty());
            Assert.IsFalse(Obj.IsEmpty());
            Assert.IsFalse(new SystemOfUnits().IsEmpty());
        }
        [TestMethod] public void IsThisTest() {
            Assert.IsTrue(Obj.IsThis(Obj.Name));
            Assert.IsTrue(Obj.IsThis(Obj.Symbol));
            Assert.IsFalse(Obj.IsThis(GetRandom.String()));
            Assert.IsFalse(Obj.IsThis(null));
            Assert.IsFalse(Obj.IsThis(string.Empty));
        }
        [TestMethod] public void GetCommonTest() {
            var s = SystemOfUnits.Random();
            var u1 = UnitBase.Random();
            var u2 = UnitDerived.Random();
            Assert.AreEqual(SystemOfUnits.Empty, SystemOfUnits.GetCommon(u1, u2));
            SystemsOfUnits.Instance.Add(s);
            u1.SystemOfUnits = s.Name;
            u2.SystemOfUnits = s.Name;
            Assert.AreEqual(s, SystemOfUnits.GetCommon(u1, u2));
        }
        [TestMethod] public void FormulaTest() {
            var s = SystemOfUnits.Random();
            Assert.AreEqual(s.Name, s.Formula(true));
            Assert.AreEqual(s.Symbol, s.Formula());
        }
    }
}
