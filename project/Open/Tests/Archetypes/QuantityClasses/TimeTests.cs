﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class TimeTests : DefinedUnitsTests {
        [TestInitialize] public override void TestInitialize() {
            type = typeof(Time);
            measure = Time.Measure;
        }
        [TestMethod] public void MeasureTest() { testBaseMeasure(Time.MeasureName); }
        [TestMethod] public void SecondsTest() { testBaseUnit(Time.Seconds, Time.SecondsName); }
        [TestMethod] public void NanosecondsTest() {
            testFactoredUnit(Time.Nanoseconds, Time.NanosecondsName, Factors.Nano);
        }
        [TestMethod] public void MicosecondsTest() {
            testFactoredUnit(Time.Micoseconds, Time.MicrosecondsName, Factors.Micro);
        }
        [TestMethod] public void MillisecondsTest() {
            testFactoredUnit(Time.Milliseconds, Time.MillisecondsName, Factors.Milli);
        }
        [TestMethod] public void MinutesTest() {
            testFactoredUnit(Time.Minutes, Time.MinutesName, Time.MinutesFactor);
        }
        [TestMethod] public void HoursTest() {
            testFactoredUnit(Time.Hours, Time.HoursName, Time.HoursFactor);
        }
        [TestMethod] public void DaysTest() {
            testFactoredUnit(Time.Days, Time.DaysName, Time.DaysFactor);
        }
        [TestMethod] public void WeeksTest() {
            testFactoredUnit(Time.Weeks, Time.WeeksName, Time.WeeksFactor);
        }
        [TestMethod] public void FortnightsTest() {
            testFactoredUnit(Time.Fortnights, Time.FortnightsName, Time.FortnightsFactor);
        }
        [TestMethod] public void MonthsTest() {
            testFactoredUnit(Time.Months, Time.MonthsName, Time.MonthsFactor);
        }
        [TestMethod] public void YearsTest() {
            testFactoredUnit(Time.Years, Time.YearsName, Time.YearsFactor);
        }
        [TestMethod] public void DecadesTest() {
            testFactoredUnit(Time.Decades, Time.DecadesName, Time.DecadesFactor);
        }
        [TestMethod] public void CenturiesTest() {
            testFactoredUnit(Time.Centuries, Time.CenturiesName, Time.CenturiesFactor);
        }
        [TestMethod] public void MillenniaTest() {
            testFactoredUnit(Time.Millennia, Time.MillenniaName, Time.MillenniasFactor);
        }
        [TestMethod] public void MeasureNameTest() { Assert.AreEqual("Time", Time.MeasureName); }
        [TestMethod] public void NanosecondsNameTest() {
            Assert.AreEqual("Nanoseconds", Time.NanosecondsName);
        }
        [TestMethod] public void MicrosecondsNameTest() {
            Assert.AreEqual("Microseconds", Time.MicrosecondsName);
        }
        [TestMethod] public void MillisecondsNameTest() {
            Assert.AreEqual("Milliseconds", Time.MillisecondsName);
        }
        [TestMethod] public void SecondsNameTest() { Assert.AreEqual("Seconds", Time.SecondsName); }
        [TestMethod] public void MinutesNameTest() { Assert.AreEqual("Minutes", Time.MinutesName); }
        [TestMethod] public void HoursNameTest() { Assert.AreEqual("Hours", Time.HoursName); }
        [TestMethod] public void DaysNameTest() { Assert.AreEqual("Days", Time.DaysName); }
        [TestMethod] public void WeeksNameTest() { Assert.AreEqual("Weeks", Time.WeeksName); }
        [TestMethod] public void FortnightsNameTest() {
            Assert.AreEqual("Fortnights", Time.FortnightsName);
        }
        [TestMethod] public void MonthsNameTest() { Assert.AreEqual("Months", Time.MonthsName); }
        [TestMethod] public void YearsNameTest() { Assert.AreEqual("Years", Time.YearsName); }
        [TestMethod] public void DecadesNameTest() { Assert.AreEqual("Decades", Time.DecadesName); }
        [TestMethod] public void CenturiesNameTest() {
            Assert.AreEqual("Centuries", Time.CenturiesName);
        }
        [TestMethod] public void MillenniaNameTest() {
            Assert.AreEqual("Millennia", Time.MillenniaName);
        }
        [TestMethod] public void HoursPerDayTest() { Assert.AreEqual(24.0, Time.HoursPerDay); }
        [TestMethod] public void MinutesFactorTest() { Assert.AreEqual(60.0, Time.MinutesFactor); }
        [TestMethod] public void MinutesPerDayTest() {
            Assert.AreEqual(Time.HoursPerDay * Time.MinutesFactor, Time.MinutesPerDay);
        }
        [TestMethod] public void HoursFactorTest() {
            Assert.AreEqual(Time.MinutesFactor * Time.MinutesFactor, Time.HoursFactor);
        }
        [TestMethod] public void DaysFactorTest() {
            Assert.AreEqual(Time.HoursPerDay * Time.HoursFactor, Time.DaysFactor);
        }
        [TestMethod] public void DaysPerWeekTest() { Assert.AreEqual(7.0, Time.DaysPerWeek); }
        [TestMethod] public void WeeksFactorTest() {
            Assert.AreEqual(Time.DaysPerWeek * Time.DaysFactor, Time.WeeksFactor);
        }
        [TestMethod] public void WeeksPerFortnightTest() {
            Assert.AreEqual(2.0, Time.WeeksPerFortnight);
        }
        [TestMethod] public void ApproxDaysPerMonthTest() {
            Assert.AreEqual(30.4375, Time.ApproxDaysPerMonth);
        }
        [TestMethod] public void MonthsFactorTest() {
            Assert.AreEqual(Time.ApproxDaysPerMonth * Time.DaysFactor, Time.MonthsFactor);
        }
        [TestMethod] public void ApproxDaysPerYearTest() {
            Assert.AreEqual(365.25, Time.ApproxDaysPerYear);
        }
        [TestMethod] public void YearsFactorTest() {
            Assert.AreEqual(Time.ApproxDaysPerYear * Time.DaysFactor, Time.YearsFactor);
        }
        [TestMethod] public void YearsPerDecadeTest() {
            Assert.AreEqual(10.0, Time.YearsPerDecade);
        }
        [TestMethod] public void YearsPerCenturyTest() {
            Assert.AreEqual(10.0 * Time.YearsPerDecade, Time.YearsPerCentury);
        }
        [TestMethod] public void YearsPerMillenniumTest() {
            Assert.AreEqual(10.0 * Time.YearsPerCentury, Time.YearsPerMillennium);
        }
        [TestMethod] public void FortnightsFactorTest() {
            Assert.AreEqual(Time.WeeksPerFortnight * Time.WeeksFactor, Time.FortnightsFactor);
        }
        [TestMethod] public void DecadesFactorTest() {
            Assert.AreEqual(Time.YearsPerDecade * Time.YearsFactor, Time.DecadesFactor);
        }
        [TestMethod] public void CenturiesFactorTest() {
            Assert.AreEqual(Time.YearsPerCentury * Time.YearsFactor, Time.CenturiesFactor);
        }
        [TestMethod] public void MillenniasFactorTest() {
            Assert.AreEqual(Time.YearsPerMillennium * Time.YearsFactor, Time.MillenniasFactor);
        }
        [TestMethod] public void InitializeTest() {
            Measures.Instance.Clear();
            Units.Instance.Clear();
            Assert.IsTrue(Time.Initialize());
            Assert.AreEqual(1, Measures.Instance.Count);
            Assert.IsNotNull(Measures.Find(Time.MeasureName));
            Assert.AreEqual(14, Units.Instance.Count);
            testUnit(Time.NanosecondsName);
            testUnit(Time.MicrosecondsName);
            testUnit(Time.MillisecondsName);
            testUnit(Time.SecondsName);
            testUnit(Time.MinutesName);
            testUnit(Time.HoursName);
            testUnit(Time.DaysName);
            testUnit(Time.WeeksName);
            testUnit(Time.FortnightsName);
            testUnit(Time.MonthsName);
            testUnit(Time.YearsName);
            testUnit(Time.DecadesName);
            testUnit(Time.CenturiesName);
            testUnit(Time.MillenniaName);
        }
    }
}
