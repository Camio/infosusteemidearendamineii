﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class SystemsOfUnitsTests :
        Tests<SystemsOfUnits> {
        protected override SystemsOfUnits GetRandomObj() {
            return SystemsOfUnits.Random();
        }
        [TestMethod] public void ConstructorTest() {
            Assert.AreEqual(new SystemsOfUnits().GetType().BaseType,
                typeof(Register<SystemOfUnits>));
        }
        [TestMethod] public void CreateTest() {
            SystemsOfUnits.Instance.Clear();
            var s = RandomValue.String();
            SystemsOfUnits.Create(s);
            Assert.AreEqual(1, SystemsOfUnits.Instance.Count);
            var m = SystemsOfUnits.Instance[0];
            Assert.AreEqual(s, m.Name);
            Assert.IsInstanceOfType(m, typeof(SystemOfUnits));
        }
        [TestMethod] public void FindTest() {
            var s = RandomValue.String();
            var o = SystemsOfUnits.Find(s);
            Assert.AreEqual(SystemOfUnits.Empty, o);
        }
        [TestMethod] public void InstanceTest() {
            IsSingletonTest(() => SystemsOfUnits.Instance);
        }
        [TestMethod] public void RegisterTest() {
            var x = SystemOfUnits.Random();
            Assert.AreEqual(x, SystemsOfUnits.Register(x));
            Assert.AreEqual(x,
                SystemsOfUnits.Instance.Find(o => o.Name == x.Name));
        }
        [TestMethod] public void RegisterWithSameNameTest() {
            var x = SystemOfUnits.Random();
            var n = x.Name;
            var c = x.Code;
            var d = x.Note;
            SystemsOfUnits.Register(x);
            var y = new SystemOfUnits(n);
            var z = SystemsOfUnits.Register(y);
            Assert.IsTrue(x.Equals(z));
            Assert.AreEqual(n, z.Name);
            Assert.AreEqual(c, z.Code);
            Assert.AreEqual(d, z.Note);
        }
        [TestMethod] public void RegisterWithNewCodeAndNoteTest() {
            var x = SystemOfUnits.Random();
            var na = x.Name;
            var co = RandomValue.String();
            var no = RandomValue.String();
            var y = new SystemOfUnits(na, co, no);
            Action<string, string> test = (b, c) => {
                var z = SystemsOfUnits.Register(y);
                Assert.IsTrue(x.Equals(z));
                Assert.AreEqual(na, z.Name);
                Assert.AreEqual(b, z.Code);
                Assert.AreEqual(c, z.Note);
            };
            SystemsOfUnits.Register(x);
            test(x.Code, x.Note);
            x.Code = string.Empty;
            x.Note = string.Empty;
            test(co, no);
        }
    }
}
