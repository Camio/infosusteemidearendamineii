﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.QuantityClasses;
using Open.Archetypes.RuleClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class QuantityTests : CommonTests<Quantity> {
        private const double factor1 = 0.000001;
        private const double factor2 = 100000;
        protected override Quantity getRandomObj() { return Quantity.Random(); }
        private Measure measure;
        private double amount;
        private readonly Unit baseUnit = UnitBase.Random();
        private readonly Unit derivedUnit = UnitDerived.Random();
        private readonly Unit factoredUnit = UnitFactored.Random();
        private readonly Unit functionedUnit = UnitFunctioned.Random();
        [ClassInitialize] public static void ClassInitialize(TestContext c) {
            Measures.Instance.Clear();
            Units.Instance.Clear();
        }
        [TestInitialize] public override void TestInitialize() {
            base.TestInitialize();
            amount = GetRandom.Double();
            measure = Measures.Add(GetRandom.String(3, 5));
            Obj.Unit = Units.Add(Obj.Unit, measure).UniqueId;
            Units.Instance.Add(baseUnit);
            Units.Instance.Add(derivedUnit);
            Units.Instance.Add(factoredUnit);
            Units.Instance.Add(functionedUnit);
        }
        [TestCleanup] public override void TestCleanup() {
            base.TestCleanup();
            Measures.Instance.Clear();
            Units.Instance.Clear();
        }
        [TestMethod] public void IsEmptyTest() {
            Assert.IsTrue(Quantity.Empty.IsEmpty());
            Assert.IsFalse(new Quantity().IsEmpty());
            Assert.IsFalse(Obj.IsEmpty());
        }
        [TestMethod] public void EmptyTest() { testSingleton(() => Quantity.Empty); }
        [TestMethod] public void AmountTest() {
            testProperty(() => Obj.Amount, x => Obj.Amount = x);
        }
        [TestMethod] public void UnitTest() { testProperty(() => Obj.Unit, x => Obj.Unit = x); }
        [TestMethod] public void GetUnitTest() {
            Obj = getRandomObj();
            Assert.AreEqual(Unit.Empty, Obj.GetUnit);
            var u = Units.Add(Obj.Unit, Measure.Empty);
            Assert.AreEqual(u, Obj.GetUnit);
        }
        [TestMethod] public void DefaultFormatTest() {
            Assert.AreEqual("{0} {1}", Quantity.DefaultFormat);
        }
        [TestMethod] public void RoundTest() {
            Func<double, Approximate, sbyte, byte, double, double> round =
                (x, strategy, percition, digits, step) => {
                    var p = new Rounding(strategy, percition, digits, step);
                    var v = new Quantity(Unit.Random(), x);
                    return v.Round(p).Amount;
                };
            Assert.AreEqual(4.5,
                round(4.45, Approximate.Up, 1, GetRandom.UInt8(), GetRandom.Double()));
            Assert.AreEqual(-4.45,
                round(-4.456, Approximate.Down, 2, GetRandom.UInt8(), GetRandom.Double()));
            Assert.AreEqual(-4.5,
                round(-4.45, Approximate.UpByStep, GetRandom.Int8(), GetRandom.UInt8(),
                    0.25));
            Assert.AreEqual(4.25,
                round(4.45, Approximate.DownByStep, GetRandom.Int8(), GetRandom.UInt8(),
                    0.25));
            Assert.AreEqual(-4.5,
                round(-4.45, Approximate.TowardsNegative, 1, GetRandom.UInt8(),
                    GetRandom.Double()));
            Assert.AreEqual(4.46,
                round(4.456, Approximate.TowardsPositive, 2, GetRandom.UInt8(),
                    GetRandom.Double()));
            Assert.AreEqual(4.45, round(4.456, Approximate.Common, 2, 7, GetRandom.Double()));
        }
        [TestMethod] public void ConvertToTest() {
            testQuantity(Obj.ConvertTo(baseUnit), double.NaN, baseUnit);
            testQuantity(Obj.ConvertTo(derivedUnit), double.NaN, derivedUnit);
            testQuantity(Obj.ConvertTo(factoredUnit), double.NaN, factoredUnit);
            testQuantity(Obj.ConvertTo(functionedUnit), double.NaN, functionedUnit);
            testQuantity(Obj.ConvertTo(null), double.NaN, Unit.Empty);
            var u1 = Units.Add(GetRandom.String(), factor1, measure);
            var u2 = Units.Add(GetRandom.String(), factor2, measure);
            var q = new Quantity(u1, amount);
            testQuantity(q.ConvertTo(u2), amount * factor1 / factor2, u2);
        }
        [TestMethod] public void CompareToTest() {
            amount = GetRandom.Double(1, 100000);
            var u1 = Units.Add(GetRandom.String(), factor1, measure);
            var u2 = Units.Add(GetRandom.String(), factor2, measure);
            var q1 = new Quantity(u1, amount);
            var q2 = new Quantity(u2, amount);
            var q3 = new Quantity(u1, factor2);
            var q4 = new Quantity(u2, factor1);
            Assert.AreEqual(int.MinValue, Obj.CompareTo(null));
            Assert.AreEqual(Operand.equal, Obj.CompareTo(Obj));
            Assert.AreEqual(Operand.equal, q3.CompareTo(q4));
            Assert.AreEqual(Operand.greater, q2.CompareTo(q1));
            Assert.AreEqual(Operand.less, q1.CompareTo(q2));
        }
        [TestMethod] public void AddTest() {
            var q1 = new Quantity(Distance.Meters, 10);
            var q2 = new Quantity(Distance.Millimeters, 10);
            var q3 = new Quantity(Volume.Liters, 10);
            testQuantity(q1.Add(q2), 10010, Distance.Millimeters);
            testQuantity(q2.Add(q1), 10.010, Distance.Meters);
            testQuantity(q1.Add(q3), double.NaN, Unit.Empty);
        }
        [TestMethod] public void SubtractTest() {
            var q1 = new Quantity(Volume.Liters, 10);
            var q2 = new Quantity(Volume.CubicMeters, 10);
            var q3 = new Quantity(Distance.Meters, 10);
            testQuantity(q1.Subtract(q2), -9.99, Volume.CubicMeters);
            testQuantity(q2.Subtract(q1), 9990, Volume.Liters);
            testQuantity(q1.Subtract(q3), double.NaN, Unit.Empty);
        }
        [TestMethod] public void MultiplyTest() {
            Assert.IsNotNull(Distance.Meters);
            Assert.IsNotNull(Area.SquareMeters);
            Assert.IsNotNull(Volume.CubicMeters);
            Assert.IsNotNull(Time.Hours);
            var q1 = new Quantity(Distance.Meters, 10);
            var q2 = new Quantity(Area.SquareMeters, 10);
            var q3 = new Quantity(Time.Hours, 10);
            var u = Distance.Meters.Multiply(Time.Hours);
            testQuantity(q1.Multiply(q2), 100, Volume.CubicMeters);
            testQuantity(q2.Multiply(q1), 100, Volume.CubicMeters);
            testQuantity(q1.Multiply(q3), 100, u);
        }
        [TestMethod] public void DivideTest() {
            var q1 = new Quantity(Distance.Meters, 10);
            var q2 = new Quantity(Area.SquareMeters, 100);
            var q3 = new Quantity(Time.Hours, 1000);
            var q4 = new Quantity(Volume.CubicMeters, 1000);
            var u = Distance.Meters.Divide(Time.Hours);
            testQuantity(q4.Divide(q1), 100, Area.SquareMeters);
            testQuantity(q4.Divide(q2), 10, Distance.Meters);
            testQuantity(q1.Divide(q1), 1, Unit.Empty);
            testQuantity(q3.Divide(q3), 1, Unit.Empty);
            testQuantity(q1.Divide(q3), 0.01, u);
        }
        [TestMethod] public void MultiplyDoubleTest() {
            var q = Obj.Multiply(amount);
            testQuantity(q, Obj.Amount * amount, q.GetUnit);
        }
        [TestMethod] public void DivideDoubleTest() {
            var q = Obj.Divide(amount);
            testQuantity(q, Obj.Amount / amount, Obj.GetUnit);
        }
        private static void testQuantity(Quantity q, double a, Unit u) {
            Assert.AreEqual(a, q.Amount);
            Assert.AreEqual(u, q.GetUnit);
        }
    }
}
