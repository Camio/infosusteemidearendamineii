﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class TermsTests : Tests<Terms<Metric>> {
        protected override Terms<Metric> GetRandomObj() {
            var t = new Terms<Metric>();
            var c = RandomValue.Count(3, 5);
            for (var i = 0; i < c; i++)
                t.Add(Metric.Random());
            return t;
        }
        [TestMethod] public void ConstructorTest() {
            Obj = new Terms<Metric>();
            Assert.AreEqual(Obj.GetType().BaseType, typeof(Register<Metric>));
        }
        [TestMethod] public void ParameterizedConstructorTest() {
            var a = GetRandomObj();
            Obj = new Terms<Metric>(a);
            Assert.AreEqual(Obj.Count, a.Count);
            for (var i = 0; i < a.Count; i++)
                Assert.AreEqual(a[i], Obj[i]);
        }
        [TestMethod] public void IsSetTest() {
            Obj = GetRandomObj();
            var c = Obj.Count;
            var a = Metric.Random();
            var b = new Metric(a.Name, a.Code, a.Note);
            Obj.Add(a);
            Obj.Add(b);
            Assert.AreEqual(a, Obj.Find(o => o.Name == b.Name));
            Assert.AreEqual(c + 1, Obj.Count);
        }
    }
}
