﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class MeasureTermTests : CommonTests<MeasureTerm> {
        protected override MeasureTerm getRandomObj() { return MeasureTerm.Random(); }
        [TestInitialize] public override void TestInitialize() {
            base.TestInitialize();
            var c = GetRandom.Count(100, 255);
            for (var i = 0; i < c; i++) Measures.Add(GetRandom.String());
        }
        [TestCleanup] public override void TestCleanup() {
            base.TestCleanup();
            Measures.Instance.Clear();
        }
        [TestMethod] public void GetMeasureTest() {
            Assert.AreNotEqual(0, Measures.Instance.Count);
            var measure = MeasureBase.Random();
            measure.Name = Obj.Measure;
            Measures.Instance.Add(measure);
            var m = Obj.GetMeasure;
            Assert.IsNotNull(m);
            Assert.AreEqual(Obj.Measure, m.Name);
        }
        [TestMethod] public void MeasureTest() {
            Obj = new MeasureTerm();
            testProperty(() => Obj.Measure, x => Obj.Measure = x);
        }
        [TestMethod] public void IsEmptyTest() {
            Assert.IsTrue(MeasureTerm.Empty.IsEmpty());
            Assert.IsFalse(new MeasureTerm().IsEmpty());
            Assert.IsFalse(Obj.IsEmpty());
            Assert.IsFalse(MeasureTerm.Random().IsEmpty());
        }
        [TestMethod] public void EmptyTest() { testSingleton(() => MeasureTerm.Empty); }
    }
}
