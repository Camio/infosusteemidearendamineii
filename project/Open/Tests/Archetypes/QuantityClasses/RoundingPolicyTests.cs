﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class RoundingTests : CommonTests<Rounding> {
        protected override Rounding getRandomObj() { return Rounding.Random(); }
        [TestMethod] public void StrategyTest() {
            testEnumProperty(() => Obj.Strategy, x => Obj.Strategy = x);
        }
        [TestMethod] public void StepTest() { testProperty(() => Obj.Step, x => Obj.Step = x, 0.0, 1.0); }
        [TestMethod] public void DecimalsTest() {
            testProperty(() => Obj.Decimals, x => Obj.Decimals = x);
        }
        [TestMethod] public void DigitTest() {
            testProperty(() => Obj.Digit, x => Obj.Digit = x, 1, 9);
        }
        [TestMethod] public void EmptyTest() { testSingleton(() => Rounding.Empty); }
        [TestMethod] public void IsEmptyTest() {
            Assert.IsTrue(Rounding.Empty.IsEmpty());
            Assert.IsFalse(new Rounding().IsEmpty());
            Assert.IsFalse(Obj.IsEmpty());
        }
    }
}
