﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class VolumeTests : DefinedUnitsTests {
        [TestInitialize] public override void TestInitialize() {
            type = typeof(Volume);
            measure = Volume.Measure;
        }
        [TestMethod] public void MeasureTest() {
            testMeasureDerived(Volume.MeasureName, getTerms(Distance.Measure));
        }
        private static MeasureTerms getTerms(Measure m) {
            if (ReferenceEquals(null, m)) return MeasureTerms.Empty;
            var d = m.Multiply(m) as MeasureDerived;
            if (ReferenceEquals(null, d)) return MeasureTerms.Empty;
            var d1 = d.Multiply(m) as MeasureDerived;
            return ReferenceEquals(null, d1) ? MeasureTerms.Empty : d1.Terms;
        }
        private UnitTerms getTerms(Unit u) {
            if (ReferenceEquals(null, u)) return UnitTerms.Empty;
            var d = u.Multiply(u) as UnitDerived;
            if (ReferenceEquals(null, d)) return UnitTerms.Empty;
            var d1 = d.Multiply(u) as UnitDerived;
            return ReferenceEquals(null, d1) ? UnitTerms.Empty : d1.Terms;
        }
        [TestMethod] public void CubicMillimetersTest() {
            testUnitDerived(Volume.CubicMillimeters, Volume.CubicMillimetersName,
                getTerms(Distance.Millimeters), 1000000000.0);
        }
        [TestMethod] public void CubicCentimetersTest() {
            testUnitDerived(Volume.CubicCentimeters, Volume.CubicCentimetersName,
                getTerms(Distance.Centimeters), 1000000.0);
        }
        [TestMethod] public void CubicDecimetersTest() {
            testUnitDerived(Volume.CubicDecimeters, Volume.CubicDecimetersName,
                getTerms(Distance.Decimeters), 1000.0);
        }
        [TestMethod] public void CubicMetersTest() {
            testUnitDerived(Volume.CubicMeters, Volume.CubicMetersName, getTerms(Distance.Meters),
                1.0);
        }
        [TestMethod] public void CubicDecametersTest() {
            testUnitDerived(Volume.CubicDecameters, Volume.CubicDecametersName,
                getTerms(Distance.Decameters), 0.001);
        }
        [TestMethod] public void CubicHectometersTest() {
            testUnitDerived(Volume.CubicHectometers, Volume.CubicHectometersName,
                getTerms(Distance.Hectometers), 0.000001);
        }
        [TestMethod] public void CubicKilometersTest() {
            testUnitDerived(Volume.CubicKilometers, Volume.CubicKilometersName,
                getTerms(Distance.Kilometers), 0.000000001);
        }
        [TestMethod] public void CubicInchesTest() {
            var f = Distance.InchesFactor;
            f = f*f*f;
            testUnitDerived(Volume.CubicInches, Volume.CubicInchesName, getTerms(Distance.Inches),
                1/f);
        }
        [TestMethod] public void CubicFeetTest() {
            var f = Distance.FeetFactor;
            f = f*f*f;
            testUnitDerived(Volume.CubicFeet, Volume.CubicFeetName, getTerms(Distance.Feet), 1/f);
        }
        [TestMethod] public void CubicYardsTest() {
            var f = Distance.YardsFactor;
            f = f*f*f;
            testUnitDerived(Volume.CubicYards, Volume.CubicYardsName, getTerms(Distance.Yards), 1/f);
        }
        [TestMethod] public void CubicMilesTest() {
            var f = Distance.MilesFactor;
            f = f*f*f;
            testUnitDerived(Volume.CubicMiles, Volume.CubicMilesName, getTerms(Distance.Miles), 1/f);
        }
        [TestMethod] public void MilliLitersTest() {
            testUnitDerived(Volume.MilliLiters, Volume.MilliLitersName,
                getTerms(Distance.Centimeters), 1000000.0);
        }
        [TestMethod] public void CentiLitersTest() {
            var d = Distance.Centimeters;
            var d1 = d.Multiply(d) as UnitDerived;
            Assert.IsNotNull(d1);
            var d2 = d1.Multiply(Distance.Decimeters) as UnitDerived;
            Assert.IsNotNull(d2);
            testUnitDerived(Volume.CentiLiters, Volume.CentiLitersName, d2.Terms, 100000.0);
        }
        [TestMethod] public void DeciLitersTest() {
            var d = Distance.Centimeters;
            var d1 = d.Multiply(d) as UnitDerived;
            Assert.IsNotNull(d1);
            var d2 = d1.Multiply(Distance.Meters) as UnitDerived;
            Assert.IsNotNull(d2);
            testUnitDerived(Volume.DeciLiters, Volume.DeciLitersName, d2.Terms, 10000.0);
        }
        [TestMethod] public void LitersTest() {
            testUnitDerived(Volume.Liters, Volume.LitersName, getTerms(Distance.Decimeters), 1000.0);
        }
        [TestMethod] public void DecaLitersTest() {
            var d = Distance.Decimeters;
            var d1 = d.Multiply(d) as UnitDerived;
            Assert.IsNotNull(d1);
            var d2 = d1.Multiply(Distance.Meters) as UnitDerived;
            Assert.IsNotNull(d2);
            testUnitDerived(Volume.DecaLiters, Volume.DecaLitersName, d2.Terms, 100.0);
        }
        [TestMethod] public void HectoLitersTest() {
            var d = Distance.Meters;
            var d1 = d.Multiply(d) as UnitDerived;
            Assert.IsNotNull(d1);
            var d2 = d1.Multiply(Distance.Decimeters) as UnitDerived;
            Assert.IsNotNull(d2);
            testUnitDerived(Volume.HectoLiters, Volume.HectoLitersName, d2.Terms, 10.0);
        }
        [TestMethod] public void KiloLitersTest() {
            testUnitDerived(Volume.KiloLiters, Volume.KiloLitersName, getTerms(Distance.Meters), 1.0);
        }
        [TestMethod] public void MeasureNameTest() {
            Assert.AreEqual("Volume", Volume.MeasureName);
        }
        [TestMethod] public void CubicMillimetersNameTest() {
            Assert.AreEqual("CubicMillimeters", Volume.CubicMillimetersName);
        }
        [TestMethod] public void CubicCentimetersNameTest() {
            Assert.AreEqual("CubicCentimeters", Volume.CubicCentimetersName);
        }
        [TestMethod] public void CubicDecimetersNameTest() {
            Assert.AreEqual("CubicDecimeters", Volume.CubicDecimetersName);
        }
        [TestMethod] public void CubicMetersNameTest() {
            Assert.AreEqual("CubicMeters", Volume.CubicMetersName);
        }
        [TestMethod] public void CubicDecametersNameTest() {
            Assert.AreEqual("CubicDecameters", Volume.CubicDecametersName);
        }
        [TestMethod] public void CubicHectometersNameTest() {
            Assert.AreEqual("CubicHectometers", Volume.CubicHectometersName);
        }
        [TestMethod] public void CubicKilometersNameTest() {
            Assert.AreEqual("CubicKilometers", Volume.CubicKilometersName);
        }
        [TestMethod] public void CubicInchesNameTest() {
            Assert.AreEqual("CubicInches", Volume.CubicInchesName);
        }
        [TestMethod] public void CubicFeetNameTest() {
            Assert.AreEqual("CubicFeet", Volume.CubicFeetName);
        }
        [TestMethod] public void CubicYardsNameTest() {
            Assert.AreEqual("CubicYards", Volume.CubicYardsName);
        }
        [TestMethod] public void CubicMilesNameTest() {
            Assert.AreEqual("CubicMiles", Volume.CubicMilesName);
        }
        [TestMethod] public void MilliLitersNameTest() {
            Assert.AreEqual("MilliLiters", Volume.MilliLitersName);
        }
        [TestMethod] public void CentiLitersNameTest() {
            Assert.AreEqual("CentiLiters", Volume.CentiLitersName);
        }
        [TestMethod] public void DeciLitersNameTest() {
            Assert.AreEqual("DeciLiters", Volume.DeciLitersName);
        }
        [TestMethod] public void LitersNameTest() { Assert.AreEqual("Liters", Volume.LitersName); }
        [TestMethod] public void DecaLitersNameTest() {
            Assert.AreEqual("DecaLiters", Volume.DecaLitersName);
        }
        [TestMethod] public void HectoLitersNameTest() {
            Assert.AreEqual("HectoLiters", Volume.HectoLitersName);
        }
        [TestMethod] public void KiloLitersNameTest() {
            Assert.AreEqual("KiloLiters", Volume.KiloLitersName);
        }
        [TestMethod] public void InitializeTest() {
            Measures.Instance.Clear();
            Units.Instance.Clear();
            Assert.IsTrue(Volume.Initialize());
            Assert.AreEqual(2, Measures.Instance.Count);
            Assert.IsNotNull(Measures.Find(Volume.MeasureName));
            Assert.IsNotNull(Measures.Find(Distance.MeasureName));
            Assert.AreEqual(29, Units.Instance.Count);
            testUnit(Volume.CubicMillimetersName);
            testUnit(Volume.CubicCentimetersName);
            testUnit(Volume.CubicDecimetersName);
            testUnit(Volume.CubicMetersName);
            testUnit(Volume.CubicDecametersName);
            testUnit(Volume.CubicHectometersName);
            testUnit(Volume.CubicKilometersName);
            testUnit(Volume.CubicInchesName);
            testUnit(Volume.CubicFeetName);
            testUnit(Volume.CubicYardsName);
            testUnit(Volume.CubicMilesName);
            testUnit(Volume.MilliLitersName);
            testUnit(Volume.CentiLitersName);
            testUnit(Volume.DeciLitersName);
            testUnit(Volume.LitersName);
            testUnit(Volume.DecaLitersName);
            testUnit(Volume.HectoLitersName);
            testUnit(Volume.KiloLitersName);
            testUnit(Distance.MillimetersName);
            testUnit(Distance.CentimetersName);
            testUnit(Distance.DecimetersName);
            testUnit(Distance.MetersName);
            testUnit(Distance.DecametersName);
            testUnit(Distance.HectometersName);
            testUnit(Distance.KilometersName);
            testUnit(Distance.InchesName);
            testUnit(Distance.FeetName);
            testUnit(Distance.YardsName);
            testUnit(Distance.MilesName);
        }
    }
}