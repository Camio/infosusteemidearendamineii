﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class CommonTermTests : CommonTests<UnitTerm> {
        [TestMethod] public void ConstructorTest() {
            Assert.AreEqual(Obj.GetType().BaseType, typeof(CommonTerm<Unit>));
        }
        [TestMethod] public void FormulaTest() { }
        [TestMethod] public void GetUnitTest() { }
        [TestMethod] public void EmptyTest() { }
        [TestMethod] public void IsEmptyTest() { }
        [TestMethod] public void UnitTest() { }
        [TestMethod] public void PowerTest() {
            testProperty(() => Obj.Power, x => Obj.Power = x);
        }
        [TestMethod] public void ToPowerTest() {
            var p1 = GetRandom.Int8();
            var p2 = GetRandom.Int8();
            Obj.Power = p1;
            Obj.ToPower(p2);
            Assert.AreEqual(p1 + p2, Obj.Power);
        }
        protected override UnitTerm getRandomObj() { return UnitTerm.Random(); }
    }
}

