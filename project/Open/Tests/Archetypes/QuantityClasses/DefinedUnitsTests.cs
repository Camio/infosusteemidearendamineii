﻿using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.QuantityClasses;
using Open.Archetypes.RuleClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    public class DefinedUnitsTests : BaseTests {
        protected Measure measure;
        protected Unit unit;
        protected void testBaseMeasure(string name) {
            var m = Measures.Find(name);
            Assert.IsInstanceOfType(m, typeof(MeasureBase));
            Assert.IsNotInstanceOfType(m, typeof(MeasureDerived));
            Assert.AreEqual(m, measure);
        }
        protected void testMeasureDerived(string name, MeasureTerms terms) {
            var m = Measures.Find(name);
            Assert.IsInstanceOfType(m, typeof(MeasureDerived));
            Assert.AreEqual(m, measure);
            Assert.AreEqual(m.Formula(), terms.Formula());
        }
        protected void testBaseUnit(Unit u, string name) {
            unit = Units.Find(name);
            Assert.IsTrue(unit.IsBaseUnit());
            Assert.AreEqual(unit, u);
            Assert.AreEqual(measure.Name, u.Measure);
            Assert.AreEqual(measure, u.GetMeasure);
        }
        protected void testFactoredUnit(Unit u, string name, double factor) {
            unit = Units.Find(name);
            Assert.AreEqual(unit, u);
            Assert.IsTrue(unit.IsFactoredUnit());
            Assert.AreEqual(measure.Name, u.Measure);
            Assert.AreEqual(measure, u.GetMeasure);
            Assert.AreEqual(factor, (unit as UnitFactored).Factor);
        }
        protected void testUnitDerived(Unit u, string name, UnitTerms terms,
            double factor) {
            unit = Units.Find(name);
            Assert.AreEqual(unit, u);
            Assert.IsTrue(unit.IsDerivedUnit());
            Assert.AreEqual(measure.Name, u.Measure);
            Assert.AreEqual(measure, u.GetMeasure);
            var f = (unit as UnitDerived).FromBase(1);
            var actual = f.ToString(CultureInfo.InvariantCulture);
            var expected = factor.ToString(CultureInfo.InvariantCulture); 
            Assert.AreEqual(expected, actual);
        }
        protected void testUnitFunctioned(Unit u, string name, Rule toBase,
            Rule fromBase) {
            unit = Units.Find(name);
            Assert.AreEqual(unit, u);
            Assert.IsTrue(unit.IsFunctionedUnit());
            Assert.AreEqual(measure.Name, u.Measure);
            Assert.AreEqual(measure, u.GetMeasure);
            var f = unit as UnitFunctioned;
            Assert.AreEqual(toBase.Name, f.ToBaseRuleId);
            Assert.AreEqual(fromBase.Name, f.FromBaseRuleId);
            Assert.AreNotEqual(string.Empty, f.ToBaseRuleId);
            Assert.AreNotEqual(string.Empty, f.FromBaseRuleId);
            var rFrom = f.GetFromBaseRule();
            var rTo = f.GetToBaseRule();
            Assert.IsNotNull(rFrom);
            Assert.IsNotNull(rTo);
            Assert.AreEqual(rFrom, fromBase);
            Assert.AreEqual(rTo, toBase);
        }
        protected void testUnit(string name)
        {
            var u = Units.Find(name);
            Assert.AreEqual(u.UniqueId, name);
            Assert.AreEqual(u.Name, name);
        }

    }
}
