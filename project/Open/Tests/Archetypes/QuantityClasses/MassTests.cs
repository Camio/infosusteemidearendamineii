﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class MassTests : DefinedUnitsTests {
        [TestInitialize] public override void TestInitialize() {
            type = typeof(Mass);
            measure = Mass.Measure;
        }
        [TestMethod] public void MeasureTest() { testBaseMeasure(Mass.MeasureName); }
        [TestMethod] public void KilogramsTest() {
            testBaseUnit(Mass.Kilograms, Mass.KilogramsName);
        }
        [TestMethod] public void NanogramsTest() {
            testFactoredUnit(Mass.Nanograms, Mass.NanogramsName, Factors.Pico);
        }
        [TestMethod] public void MicrogramsTest() {
            testFactoredUnit(Mass.Micrograms, Mass.MicrogramsName, Factors.Nano);
        }
        [TestMethod] public void MilligramsTest() {
            testFactoredUnit(Mass.Milligrams, Mass.MilligramsName, Factors.Micro);
        }
        [TestMethod] public void CentigramsTest() {
            testFactoredUnit(Mass.Centigrams, Mass.CentigramsName, Mass.CentigramsFactor);
        }
        [TestMethod] public void CentigramsFactorTest() {
            Assert.AreEqual(Mass.CentigramsFactor, Factors.Milli * Factors.Centi);
        }
        [TestMethod] public void DecigramsTest() {
            testFactoredUnit(Mass.Decigrams, Mass.DecigramsName, Mass.DecigramsFactor);
        }
        [TestMethod] public void DecigramsFactorTest() {
            Assert.AreEqual(Mass.DecigramsFactor, Factors.Milli * Factors.Deci);
        }
        [TestMethod] public void GramsTest() {
            testFactoredUnit(Mass.Grams, Mass.GramsName, Factors.Milli);
        }
        [TestMethod] public void DecagramsTest() {
            testFactoredUnit(Mass.Decagrams, Mass.DecagramsName, Factors.Centi);
        }
        [TestMethod] public void HectogramsTest() {
            testFactoredUnit(Mass.Hectograms, Mass.HectogramsName, Factors.Deci);
        }
        [TestMethod] public void MetricTonsTest() {
            testFactoredUnit(Mass.MetricTons, Mass.MetricTonsName, Factors.Kilo);
        }
        [TestMethod] public void DramsTest() {
            testFactoredUnit(Mass.Drams, Mass.DramsName, Mass.DramsFactor);
        }
        [TestMethod] public void GrainsTest() {
            testFactoredUnit(Mass.Grains, Mass.GrainsName, Mass.GrainsFactor);
        }
        [TestMethod] public void TonsTest() {
            testFactoredUnit(Mass.Tons, Mass.TonsName, Mass.TonsFactor);
        }
        [TestMethod] public void LongTonsTest() {
            testFactoredUnit(Mass.LongTons, Mass.LongTonsName, Mass.LongTonsFactor);
        }
        [TestMethod] public void OuncesTest() {
            testFactoredUnit(Mass.Ounces, Mass.OuncesName, Mass.OuncesFactor);
        }
        [TestMethod] public void PoundsTest() {
            testFactoredUnit(Mass.Pounds, Mass.PoundsName, Mass.PoundFactor);
        }
        [TestMethod] public void StonesTest() {
            testFactoredUnit(Mass.Stones, Mass.StonesName, Mass.StonesFactor);
        }
        [TestMethod] public void PoundFactorTest() {
            Assert.AreEqual(0.45359237, Mass.PoundFactor);
        }
        [TestMethod] public void DramsFactorTest() {
            Assert.AreEqual(Mass.PoundFactor / 256, Mass.DramsFactor);
        }
        [TestMethod] public void GrainsFactorTest() {
            Assert.AreEqual(Mass.PoundFactor / 7000, Mass.GrainsFactor);
        }
        [TestMethod] public void TonsFactorTest() {
            Assert.AreEqual(Mass.PoundFactor * 2000, Mass.TonsFactor);
        }
        [TestMethod] public void LongTonsFactorTest() {
            Assert.AreEqual(Mass.PoundFactor * 2240, Mass.LongTonsFactor);
        }
        [TestMethod] public void OuncesFactorTest() {
            Assert.AreEqual(Mass.PoundFactor / 16, Mass.OuncesFactor);
        }
        [TestMethod] public void StonesFactorTest() {
            Assert.AreEqual(Mass.PoundFactor * 14, Mass.StonesFactor);
        }
        [TestMethod] public void MeasureNameTest() { Assert.AreEqual("Mass", Mass.MeasureName); }
        [TestMethod] public void NanogramsNameTest() {
            Assert.AreEqual("Nanograms", Mass.NanogramsName);
        }
        [TestMethod] public void MicrogramsNameTest() {
            Assert.AreEqual("Micrograms", Mass.MicrogramsName);
        }
        [TestMethod] public void MilligramsNameTest() {
            Assert.AreEqual("Milligrams", Mass.MilligramsName);
        }
        [TestMethod] public void CentigramsNameTest() {
            Assert.AreEqual("Centigrams", Mass.CentigramsName);
        }
        [TestMethod] public void DecigramsNameTest() {
            Assert.AreEqual("Decigrams", Mass.DecigramsName);
        }
        [TestMethod] public void GramsNameTest() { Assert.AreEqual("Grams", Mass.GramsName); }
        [TestMethod] public void DecagramsNameTest() {
            Assert.AreEqual("Decagrams", Mass.DecagramsName);
        }
        [TestMethod] public void HectogramsNameTest() {
            Assert.AreEqual("Hectograms", Mass.HectogramsName);
        }
        [TestMethod] public void KilogramsNameTest() {
            Assert.AreEqual("Kilograms", Mass.KilogramsName);
        }
        [TestMethod] public void MetricTonsNameTest() {
            Assert.AreEqual("MetricTons", Mass.MetricTonsName);
        }
        [TestMethod] public void DramsNameTest() { Assert.AreEqual("Drams", Mass.DramsName); }
        [TestMethod] public void GrainsNameTest() { Assert.AreEqual("Grains", Mass.GrainsName); }
        [TestMethod] public void TonsNameTest() { Assert.AreEqual("Tons", Mass.TonsName); }
        [TestMethod] public void LongTonsNameTest() {
            Assert.AreEqual("LongTons", Mass.LongTonsName);
        }
        [TestMethod] public void OuncesNameTest() { Assert.AreEqual("Ounces", Mass.OuncesName); }
        [TestMethod] public void PoundsNameTest() { Assert.AreEqual("Pounds", Mass.PoundsName); }
        [TestMethod] public void StonesNameTest() { Assert.AreEqual("Stones", Mass.StonesName); }
        [TestMethod] public void InitializeTest() {
            Measures.Instance.Clear();
            Units.Instance.Clear();
            Assert.IsTrue(Mass.Initialize());
            Assert.AreEqual(1, Measures.Instance.Count);
            Assert.IsNotNull(Measures.Find(Mass.MeasureName));
            Assert.AreEqual(17, Units.Instance.Count);
            testUnit(Mass.NanogramsName);
            testUnit(Mass.MicrogramsName);
            testUnit(Mass.MilligramsName);
            testUnit(Mass.CentigramsName);
            testUnit(Mass.DecigramsName);
            testUnit(Mass.GramsName);
            testUnit(Mass.DecagramsName);
            testUnit(Mass.HectogramsName);
            testUnit(Mass.KilogramsName);
            testUnit(Mass.MetricTonsName);
            testUnit(Mass.DramsName);
            testUnit(Mass.GrainsName);
            testUnit(Mass.TonsName);
            testUnit(Mass.LongTonsName);
            testUnit(Mass.OuncesName);
            testUnit(Mass.PoundsName);
            testUnit(Mass.StonesName);
        }

    }
}
