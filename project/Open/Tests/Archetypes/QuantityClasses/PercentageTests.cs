﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class PercentageTests : DefinedUnitsTests {
        [TestInitialize] public override void TestInitialize() {
            type = typeof(Percentage);
            measure = Percentage.Measure;
        }
        [TestMethod] public void MeasureTest() { testBaseMeasure(Percentage.MeasureName); }
        [TestMethod] public void UnitTest() {
            testBaseUnit(Percentage.Unit, Percentage.MeasureName);
        }
        [TestMethod] public void MeasureNameTest() {
            Assert.AreEqual("Percentage", Percentage.MeasureName);
        }
        [TestMethod] public void InitializeTest() {
            Measures.Instance.Clear();
            Units.Instance.Clear();
            Assert.IsTrue(Percentage.Initialize());
            Assert.AreEqual(1, Measures.Instance.Count);
            Assert.IsNotNull(Measures.Find(Percentage.MeasureName));
            Assert.AreEqual(1, Units.Instance.Count);
            testUnit(Percentage.MeasureName);
        }
    }
}