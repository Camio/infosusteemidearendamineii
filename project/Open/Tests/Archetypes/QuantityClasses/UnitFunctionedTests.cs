﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.QuantityClasses;
using Open.Archetypes.RuleClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class UnitFunctionedTests : Tests<UnitFunctioned> {
        [TestMethod] public void GetToBaseRuleTest() {
            TestGetRule(() => Obj.GetToBaseRule(), Obj.ToBaseRuleId);
        }
        [TestMethod] public void GetFromBaseRuleTest() {
            TestGetRule(() => Obj.GetFromBaseRule(), Obj.FromBaseRuleId);
        }
        private static void TestGetRule(Func<Rule> f, string s) {
            var r = f();
            Assert.IsNotNull(r);
            Assert.AreEqual(string.Empty, r.Name);
            Assert.AreNotEqual(string.Empty, s);
            r = Rule.Random();
            r.Name = s;
            Rules.Register(r);
            r = f();
            Assert.IsNotNull(r);
            Assert.AreNotEqual(string.Empty, r.Name);
            Assert.AreEqual(s, r.Name);
        }
        [TestMethod] public void ToBaseTest() {
            var rule = new RuleElements {
                new Operand(UnitFunctioned.VariableToken),
                new DoubleVariable("x", 32.0),
                new Operator(Operation.Subtract),
                new DoubleVariable("y", 5.0),
                new DoubleVariable("y", 9.0),
                new Operator(Operation.Divide),
                new Operator(Operation.Multiply)
            };
            var v = new Rule("Fahrenheit to Celsius", rule);
            Obj.ToBaseRuleId = v.Name;
            Rules.Register(v);
            Assert.AreEqual(20, Obj.ToBase(68));
        }
        [TestMethod] public void FromBaseTest() {
            var rule = new RuleElements {
                new Operand(UnitFunctioned.VariableToken),
                new DoubleVariable("y", 9.0),
                new DoubleVariable("y", 5.0),
                new Operator(Operation.Divide),
                new Operator(Operation.Multiply),
                new DoubleVariable("x", 32.0),
                new Operator(Operation.Add),
            };
            var v = new Rule("Celsius to Fahrenheit", rule);
            Obj.FromBaseRuleId = v.Name;
            Rules.Register(v);
            Assert.AreEqual(68, Obj.FromBase(20));
        }
        [TestMethod] public void ToBaseRuleIdTest() {
            Obj.ToBaseRuleId = string.Empty;
            StrPropTest(() => Obj.ToBaseRuleId, x => Obj.ToBaseRuleId = x);
        }
        [TestMethod] public void FromBaseRuleIdTest() {
            Obj.FromBaseRuleId = string.Empty;
            StrPropTest(() => Obj.FromBaseRuleId, x => Obj.FromBaseRuleId = x);
        }
        protected override UnitFunctioned GetRandomObj() {
            return UnitFunctioned.Random();
        }
    }
}

