﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class ManHourTests : DefinedUnitsTests {
        [TestInitialize] public override void TestInitialize() {
            type = typeof(ManHour);
            measure = ManHour.Measure;
        }
        [TestMethod] public void MeasureTest() { testBaseMeasure(ManHour.MeasureName); }
        [TestMethod] public void UnitTest() { testBaseUnit(ManHour.Unit, ManHour.MeasureName); }
        [TestMethod] public void MeasureNameTest() {
            Assert.AreEqual("ManHour", ManHour.MeasureName);
        }
        [TestMethod] public void InitializeTest() {
            Measures.Instance.Clear();
            Units.Instance.Clear();
            Assert.IsTrue(ManHour.Initialize());
            Assert.AreEqual(1, Measures.Instance.Count);
            Assert.IsNotNull(Measures.Find(ManHour.MeasureName));
            Assert.AreEqual(1, Units.Instance.Count);
            testUnit(ManHour.MeasureName);
        }
    }
}
