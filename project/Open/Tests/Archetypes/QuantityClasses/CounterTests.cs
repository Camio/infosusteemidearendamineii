﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class CounterTests : DefinedUnitsTests {
        [TestInitialize] public override void TestInitialize() {
            type = typeof(Counter);
            measure = Counter.Measure;
        }
        [TestMethod] public void MeasureTest() { testBaseMeasure(Counter.MeasureName); }
        [TestMethod] public void UnitsTest() { testBaseUnit(Counter.Units, Counter.UnitName); }
        [TestMethod] public void MeasureNameTest() {
            Assert.AreEqual("Counter", Counter.MeasureName);
        }
        [TestMethod] public void UnitNameTest() { Assert.AreEqual("Units", Counter.UnitName); }
        [TestMethod] public void InitializeTest() {
            Measures.Instance.Clear();
            Units.Instance.Clear();
            Assert.IsTrue(Counter.Initialize());
            Assert.AreEqual(1, Measures.Instance.Count);
            Assert.IsNotNull(Measures.Find(Counter.MeasureName));
            Assert.AreEqual(1, Units.Instance.Count);
            testUnit(Counter.UnitName);
        }
    }
}