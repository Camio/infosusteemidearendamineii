﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.QuantityClasses;
using Open.Archetypes.RuleClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class TemperatureTests : DefinedUnitsTests {
        [TestInitialize] public override void TestInitialize() {
            type = typeof(Temperature);
            measure = Temperature.Measure;
        }
        [TestMethod] public void MeasureTest() { testBaseMeasure(Temperature.MeasureName); }
        [TestMethod] public void KelvinTest() {
            testBaseUnit(Temperature.Kelvin, Temperature.KelvinName);
        }
        [TestMethod] public void CelciusToKelvinTest() {
            Action<double, double> test = (x, y) => {
                var rule = new Variables {new DoubleVariable(UnitFunctioned.VariableToken, x)};
                var c = new RuleContext("A", rule);
                var r = Temperature.CelciusToKelvin.Calculate(c);
                var s = r as DoubleVariable;
                Assert.IsNotNull(s);
                Assert.AreEqual(y, s.Value);
            };
            test(0, Temperature.CelciusFactor);
            test(-Temperature.CelciusFactor, 0);
        }
        [TestMethod] public void KelvinToCelciusTest() {
            Action<double, double> test = (x, y) => {
                var rule = new Variables {new DoubleVariable(UnitFunctioned.VariableToken, x)};
                var c = new RuleContext("A", rule);
                var s = Temperature.KelvinToCelcius.Calculate(c) as DoubleVariable;
                Assert.IsNotNull(s);
                Assert.AreEqual(y, s.Value);
            };
            test(0, -Temperature.CelciusFactor);
            test(Temperature.CelciusFactor, 0);
        }
        [TestMethod] public void KelvinToFahrenheitTest() {
            Action<double, double> test = (x, y) => {
                var rule = new Variables {new DoubleVariable(UnitFunctioned.VariableToken, x)};
                var c = new RuleContext("A", rule);
                var s = Temperature.KelvinToFahrenheit.Calculate(c) as DoubleVariable;
                Assert.IsNotNull(s);
                var r1 = Round.Off(y, 6, 5);
                var r2 = Round.Off(s.Value, 6, 5);
                Assert.AreEqual(r1, r2);
            };
            test(0, -Temperature.FahrenheitFactor);
            test(Temperature.FahrenheitFactor, 367.736);
            test(1, -457.87);
            test(10, -441.67);
            test(100, -279.67);
            test(-100, -639.67);
        }
        [TestMethod] public void FahrenheitToKelvinTest() {
            Action<double, double> test = (x, y) => {
                var rule = new Variables {new DoubleVariable(UnitFunctioned.VariableToken, x)};
                var c = new RuleContext("A", rule);
                var s = Temperature.FahrenheitToKelvin.Calculate(c) as DoubleVariable;
                Assert.IsNotNull(s);
                var r1 = Round.Off(y, 5, 5);
                var r2 = Round.Off(s.Value, 5, 5);
                Assert.AreEqual(r1, r2);
            };
            test(0, 255.3722222);
            test(100, 310.92778);
            test(-100, 199.81667);
        }
        [TestMethod] public void KelvinToRankineTest() {
            Action<double, double> test = (x, y) => {
                var rule = new Variables {new DoubleVariable(UnitFunctioned.VariableToken, x)};
                var c = new RuleContext("A", rule);
                var s = Temperature.KelvinToRankine.Calculate(c) as DoubleVariable;
                Assert.IsNotNull(s);
                Assert.AreEqual(y, s.Value);
            };
            test(0, 0);
            test(Temperature.RankineFactor, 3.24);
            test(1, Temperature.RankineFactor);
            test(10, 10 * Temperature.RankineFactor);
            test(100, 100 * Temperature.RankineFactor);
            test(-100, -100 * Temperature.RankineFactor);
        }
        [TestMethod] public void RankineToKelvinTest() {
            Action<double, double> test = (x, y) => {
                var rule = new Variables {new DoubleVariable(UnitFunctioned.VariableToken, x)};
                var c = new RuleContext("A", rule);
                var r = Temperature.RankineToKelvin.Calculate(c);
                var s = r as DoubleVariable;
                Assert.IsNotNull(s);
                var r1 = Round.Off(y, 5, 5);
                var r2 = Round.Off(s.Value, 5, 5);
                Assert.AreEqual(r1, r2);
            };
            test(0.0, 0.0);
            test(Temperature.RankineFactor, 1.0);
            test(1.0, 0.555555555555556);
            test(10.0, 5.55555555555556);
        }
        [TestMethod] public void CelsiusTest() {
            testUnitFunctioned(Temperature.Celsius, Temperature.CelsiusName,
                Temperature.CelciusToKelvin, Temperature.KelvinToCelcius);
        }
        [TestMethod] public void FahrenheitTest() {
            testUnitFunctioned(Temperature.Fahrenheit, Temperature.FahrenheitName,
                Temperature.FahrenheitToKelvin, Temperature.KelvinToFahrenheit);
        }
        [TestMethod] public void RankineTest() {
            testUnitFunctioned(Temperature.Rankine, Temperature.RankineName,
                Temperature.RankineToKelvin, Temperature.KelvinToRankine);
        }
        [TestMethod] public void MeasureNameTest() {
            Assert.AreEqual("Temperature", Temperature.MeasureName);
        }
        [TestMethod] public void CelsiusNameTest() {
            Assert.AreEqual("Celsius", Temperature.CelsiusName);
        }
        [TestMethod] public void KelvinNameTest() {
            Assert.AreEqual("Kelvin", Temperature.KelvinName);
        }
        [TestMethod] public void FahrenheitNameTest() {
            Assert.AreEqual("Fahrenheit", Temperature.FahrenheitName);
        }
        [TestMethod] public void RankineNameTest() {
            Assert.AreEqual("Rankine", Temperature.RankineName);
        }
        [TestMethod] public void CelciusFactorTest() {
            Assert.AreEqual(273.15, Temperature.CelciusFactor);
        }
        [TestMethod] public void FahrenheitFactorTest() {
            Assert.AreEqual(459.67, Temperature.FahrenheitFactor);
        }
        [TestMethod] public void RankineFactorTest() {
            Assert.AreEqual(1.8000, Temperature.RankineFactor);
        }
        [TestMethod] public void FromCelsiusTest() {
            Assert.AreEqual("Celsius to Kelvin", Temperature.FromCelsius);
        }
        [TestMethod] public void ToCelsiusTest() {
            Assert.AreEqual("Kelvin to Celcius", Temperature.ToCelsius);
        }
        [TestMethod] public void FromFahrenheitTest() {
            Assert.AreEqual("Farenheit to Kelvin", Temperature.FromFahrenheit);
        }
        [TestMethod] public void ToFahrenheitTest() {
            Assert.AreEqual("Kelvin to Farenheit", Temperature.ToFahrenheit);
        }
        [TestMethod] public void FromRankineTest() {
            Assert.AreEqual("Frankine to Kelvin", Temperature.FromRankine);
        }
        [TestMethod] public void ToRankineTest() {
            Assert.AreEqual("Kelvin to Rankine", Temperature.ToRankine);
        }
        [TestMethod]
        public void InitializeTest()
        {
            Measures.Instance.Clear();
            Units.Instance.Clear();
            Assert.IsTrue(Temperature.Initialize());
            Assert.AreEqual(1, Measures.Instance.Count);
            Assert.IsNotNull(Measures.Find(Temperature.MeasureName));
            Assert.AreEqual(4, Units.Instance.Count);
            testUnit(Temperature.CelsiusName);
            testUnit(Temperature.FahrenheitName);
            testUnit(Temperature.KelvinName);
            testUnit(Temperature.RankineName);
        }
    }
}
