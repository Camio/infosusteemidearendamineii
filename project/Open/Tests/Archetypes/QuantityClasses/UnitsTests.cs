﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.QuantityClasses;
using Open.Archetypes.RuleClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class UnitsTests : Tests<Units> {
        private UnitBase unit;
        private string name;
        private string code;
        private string note;
        private MeasureBase measure;
        private SystemOfUnits sOfUnits;
        protected override Units GetRandomObj() { return Units.Random(); }
        [TestInitialize] public override void TestInitialize() {
            base.TestInitialize();
            SetRandomValues();
            unit = new UnitBase(name, measure, sOfUnits, code, note);
            SetRandomValues();
        }
        private void SetRandomValues() {
            name = RandomValue.String();
            code = RandomValue.String();
            note = RandomValue.String();
            measure = MeasureBase.Random();
            sOfUnits = SystemOfUnits.Random();
        }
        [TestMethod] public void CreateTest() {
            Units.Instance.Clear();
            var s = RandomValue.String();
            Units.Create(s, MeasureBase.Random());
            Assert.AreEqual(1, Units.Instance.Count);
            var m = Units.Instance[0];
            Assert.AreEqual(s, m.Name);
            Assert.IsTrue(m.IsBaseUnit());
        }
        [TestMethod] public void CreateFactoredTest() {
            Units.Instance.Clear();
            var s = RandomValue.String();
            var f = RandomValue.Double();
            Units.Create(s, f, MeasureBase.Random());
            Assert.AreEqual(1, Units.Instance.Count);
            var m = Units.Instance[0] as UnitFactored;
            Assert.AreEqual(s, m.Name);
            Assert.AreEqual(f, m.Factor);
            Assert.IsTrue(m.IsFactoredUnit());
        }
        [TestMethod] public void CreateFunctionedTest() {
            Units.Instance.Clear();
            var s = RandomValue.String();
            var rFrom = Rule.Random();
            var rTo = Rule.Random();
            Units.Create(s, rTo, rFrom, MeasureBase.Random());
            Assert.AreEqual(1, Units.Instance.Count);
            var m = Units.Instance[0] as UnitFunctioned;
            Assert.AreEqual(s, m.Name);
            Assert.AreEqual(rFrom.Name, m.FromBaseRuleId);
            Assert.AreEqual(rTo.Name, m.ToBaseRuleId);
            Assert.IsTrue(m.IsFunctionedUnit());
        }
        [TestMethod] public void CreateDerivedTest() {
            Units.Instance.Clear();
            var s = RandomValue.String();
            var t = UnitTerms.Random();
            var c = Units.Instance.Count;
            Units.Create(s, t, MeasureBase.Random());
            Assert.AreEqual(c + 1, Units.Instance.Count);
            var m = Units.Instance[c] as UnitDerived;
            Assert.AreEqual(s, m.Name);
            Assert.AreEqual(t.Formula(), m.Formula());
            Assert.IsTrue(m.IsDerivedUnit());
        }
        [TestMethod] public void FindTest() {
            Assert.AreEqual(Unit.Empty, Units.Find(unit));
        }
        [TestMethod] public void FindByIdTest() { Assert.Inconclusive(); }
        [TestMethod] public void RegisterTest() {
            FindTest();
            Units.Register(unit);
            var x = unit.Clone();
            Assert.AreEqual(unit, Units.Find(x));
            var y = unit.Clone();
            y.Code = code;
            y.Note = note;
            Action<string, string, string> test = (a, b, c) => {
                Units.Register(y);
                Assert.AreEqual(a, unit.Name);
                Assert.AreEqual(b, unit.Code);
                Assert.AreEqual(c, unit.Note);
            };
            test(unit.Name, unit.Code, unit.Note);
            unit.Code = string.Empty;
            unit.Note = string.Empty;
            test(unit.Name, code, note);
        }
        [TestMethod] public void RegisterFactoredTest() {
            Assert.Inconclusive();
        }
        [TestMethod] public void RegisterFunctionedTest() {
            Assert.Inconclusive();
        }
        [TestMethod] public void RegisterDerivedTest() {
            Assert.Inconclusive();
        }
        [TestMethod] public void InstanceTest() {
            IsSingletonTest(() => Units.Instance);
        }
    }
}

