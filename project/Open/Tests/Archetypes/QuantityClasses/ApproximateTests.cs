﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.QuantityClasses;
namespace Open.Tests.Archetypes.QuantityClasses {
    [TestClass] public class ApproximateTests : ClassTests<Approximate> {
        [TestInitialize] public override void TestInitialize() {
            base.TestInitialize();
            type = typeof(Approximate);
        }
        [TestMethod] public void ConstructorTest() {
            Assert.AreEqual(7, GetEnum.Count<Approximate>());
        }
        [TestMethod] public void UpTest() {
            Assert.AreEqual(Approximate.Up, GetEnum.Value<Approximate>(0));
        }
        [TestMethod] public void DownTest() {
            Assert.AreEqual(Approximate.Down, GetEnum.Value<Approximate>(1));
        }
        [TestMethod] public void CommonTest() {
            Assert.AreEqual(Approximate.Common, GetEnum.Value<Approximate>(2));
        }
        [TestMethod] public void UpByStepTest() {
            Assert.AreEqual(Approximate.UpByStep, GetEnum.Value<Approximate>(3));
        }
        [TestMethod] public void DownByStepTest() {
            Assert.AreEqual(Approximate.DownByStep, GetEnum.Value<Approximate>(4));
        }
        [TestMethod] public void TowardsPositiveTest() {
            Assert.AreEqual(Approximate.TowardsPositive, GetEnum.Value<Approximate>(5));
        }
        [TestMethod] public void TowardsNegativeTest() {
            Assert.AreEqual(Approximate.TowardsNegative, GetEnum.Value<Approximate>(6));
        }
    }
}
