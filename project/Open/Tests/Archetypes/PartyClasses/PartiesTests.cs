﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Aids;
using Open.Archetypes.PartyClasses;
namespace Open.Tests.Archetypes.PartyClasses {
    [TestClass]
    public class PartiesTests : CommonTests<Parties> {
        protected override Parties getRandomObj() { return Parties.Random(); }

        [TestMethod]
        public void InstanceTest() {
            testSingleton(() => Parties.Instance);
        }

        [TestMethod]
        public void FindTest() {
            var randomText = GetRandom.String();
            Assert.IsNull(Parties.Find(randomText));
            var person = Person.Random();
            person.UniqueId = randomText;
            Parties.Instance.Add(person);
            Parties.Instance.AddRange(Parties.Random());
            Assert.AreEqual(person, Parties.Find(randomText));
        }
    }
}