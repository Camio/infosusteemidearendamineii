﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.PartyClasses;
namespace Open.Tests.Archetypes.PartyClasses
{
    [TestClass]
    public class PersonTests : CommonTests<Person>
    {
        protected override Person getRandomObj() { return Person.Random(); }

        [TestMethod] public void SetName() {
            const string testName = "Name";
            var person = new Person {Name = testName};
            Assert.AreEqual(person.Name, testName);
        }

        [TestMethod] public void SetEmptyName() {
            var testName = string.Empty;
            var person = new Person {Name = testName};
            Assert.AreEqual(person.Name, testName);
        }

        [TestMethod] public void SetNullName() {
            var person = new Person {Name = null};
            Assert.AreEqual(person.Name, string.Empty);
        }

        [TestMethod] public void GetName() {
            var person = new Person();
            Assert.AreEqual(person.Name, string.Empty);
        }

        [TestMethod]
        public void SetSurname()
        {
            const string testName = "Name";
            var person = new Person { Surname = testName };
            Assert.AreEqual(person.Surname, testName);
        }

        [TestMethod]
        public void SetEmptySurname()
        {
            var testName = string.Empty;
            var person = new Person { Surname = testName };
            Assert.AreEqual(person.Surname, testName);
        }

        [TestMethod]
        public void SetNullSurname()
        {
            var person = new Person { Surname = null };
            Assert.AreEqual(person.Surname, string.Empty);
        }

        [TestMethod]
        public void GetDateOfBirth()
        {
            var person = new Person();
            Assert.AreEqual(person.DateOfBirth, DateTime.MinValue);
        }

        [TestMethod]
        public void SetDateOfBirth()
        {
            var testDate = DateTime.Now - TimeSpan.FromDays(1);
            var person = new Person { DateOfBirth = testDate };
            Assert.AreEqual(person.DateOfBirth, testDate);
        }

        [TestMethod]
        public void SetMinDateOfBirth()
        {
            var testDate = DateTime.MinValue;
            var person = new Person { DateOfBirth = testDate };
            Assert.AreEqual(person.DateOfBirth, testDate);
        }

        [TestMethod]
        public void SetMaxDateOfBirth()
        {
            var testDate = DateTime.MaxValue;
            var person = new Person { DateOfBirth = testDate };
            Assert.AreEqual(person.DateOfBirth, testDate);
        }


        [TestMethod]
        public void SetRegistrationDate()
        {
            var testDate = DateTime.Now - TimeSpan.FromDays(1);
            var person = new Person { DateOfBirth = testDate };
            Assert.AreEqual(person.DateOfBirth, testDate);
        }

        [TestMethod]
        public void SetMinRegistrationDate()
        {
            var testDate = DateTime.MinValue;
            var person = new Person { DateOfBirth = testDate };
            Assert.AreEqual(person.DateOfBirth, testDate);
        }

        [TestMethod]
        public void SetMaxRegistrationDate()
        {
            var testDate = DateTime.MaxValue;
            var person = new Person { DateOfBirth = testDate };
            Assert.AreEqual(person.DateOfBirth, testDate);
        }


    }
}
