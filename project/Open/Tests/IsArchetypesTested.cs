﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Open.Tests {
    [TestClass] public class IsArchetypesTested : IsTestedBase {
        private const string assembly = "Open.Archetypes";
        protected override string Namespace(string name) { return $"{assembly}.{name}Classes"; }
        [TestMethod] public void BaseClasses() { getMembers(assembly, Namespace("Base")); }
        [TestMethod] public void ContactClasses() { getMembers(assembly, Namespace("Contact")); }
        [TestMethod] public void InventoryClasses() { getMembers(assembly, Namespace("Inventory")); }
        [TestMethod] public void MoneyClasses() { getMembers(assembly, Namespace("Money")); }
        [TestMethod] public void OrderClasses() { getMembers(assembly, Namespace("Order")); }
        [TestMethod] public void PartyClasses() { getMembers(assembly, Namespace("Party")); }
        [TestMethod] public void ProcessClasses() { getMembers(assembly, Namespace("Process")); }
        [TestMethod] public void ProductClasses() { getMembers(assembly, Namespace("Product")); }
        [TestMethod] public void QuantityClasses() { getMembers(assembly, Namespace("Quantity")); }
        [TestMethod] public void RelationshipClasses() { getMembers(assembly, Namespace("Relationship")); }
        [TestMethod] public void RoleClasses() { getMembers(assembly, Namespace("Role")); }
        [TestMethod] public void RuleClasses() { getMembers(assembly, Namespace("Rule")); }
        [TestMethod] public void ValueClasses() { getMembers(assembly, Namespace("Value")); }
    }
}
