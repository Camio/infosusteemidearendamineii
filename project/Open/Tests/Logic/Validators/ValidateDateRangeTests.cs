﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Logic.Validators;

namespace Open.Tests.Logic.Validators
{
    [TestClass]
    public class ValidateDateRangeTests : ClassTests<ValidateDateRange>
    {
        private const string tooOld = "Are you older that 200 years?";
        private const string dateInFuture = "Date of birth can't be in future.";
        private ValidateDateRange validator;

        [TestInitialize]
        public override void TestInitialize()
        {
            validator = new ValidateDateRange();
        }

        [TestMethod]
        public void DateIsTooSmall() {
            var date = new DateTime(1800, 1, 1);
            var result = validator.GetValidationResult(date, new ValidationContext(date));
            Assert.AreEqual(tooOld, result.ErrorMessage);
        }

        [TestMethod]
        public void DateIsInFuture()
        {
            var date = new DateTime(8800, 1, 1);
            var result = validator.GetValidationResult(date, new ValidationContext(date));
            Assert.AreEqual(dateInFuture, result.ErrorMessage);
        }

        [TestMethod]
        public void ValidDate()
        {
            var date = DateTime.Now;
            var result = validator.GetValidationResult(date, new ValidationContext(date));
            Assert.AreEqual(ValidationResult.Success, result);
        }
    }
}
