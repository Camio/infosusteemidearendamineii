﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Open.Aids;
using Open.Archetypes.RoleClasses;
using Open.Logic.RoleClasses;
using Open.Data;

namespace Open.Tests.Logic.RoleClasses {
    [TestClass]
    public class RoleEditModelTests : ClassTests<RoleEditModel> {
        private Role role;
        private Mock<RoleEntity> roleEntity;

        [TestInitialize]
        public override void TestInitialize() {
            base.TestInitialize();
            role = Role.Random();
            RoleTypes.Instance.AddRange(RoleTypes.Random());
            RoleTypes.Instance.ToList().ForEach(roleType => roleType.Valid.To = DateTime.MaxValue);
            roleEntity = new Mock<RoleEntity>();
        }

        [TestMethod]
        public void DefaultConstructor() {
            var viewModel = new RoleEditModel();
            Assert.IsNull(viewModel.UniqueId);
            Assert.IsNull(viewModel.Name);
            Assert.IsNull(viewModel.Description);
            Assert.IsNull(viewModel.Type);
            Assert.AreEqual(RoleTypes.Instance.Count, viewModel.ActiveRoleTypes.Count());
            Assert.IsInstanceOfType(viewModel.ActiveRoleTypes, typeof(SelectList));
        }

        [TestMethod]
        public void ConstructorWithRole() {
            var viewModel = new RoleEditModel(role);
            Assert.AreEqual(role.UniqueId, viewModel.UniqueId);
            Assert.IsInstanceOfType(viewModel.UniqueId, typeof(string));
            Assert.AreEqual(role.Name, viewModel.Name);
            Assert.IsInstanceOfType(viewModel.Name, typeof(string));
            Assert.AreEqual(role.Description, viewModel.Description);
            Assert.IsInstanceOfType(viewModel.Description, typeof(string));
            Assert.AreEqual(role.Type.UniqueId, viewModel.Type);
            Assert.IsInstanceOfType(viewModel.Type, typeof(string));
            Assert.AreEqual(RoleTypes.Instance.Count, viewModel.ActiveRoleTypes.Count());
            Assert.IsInstanceOfType(viewModel.ActiveRoleTypes, typeof(SelectList));
        }

        [TestMethod]
        public void ConstructorWithId() {
            Roles.Instance.Add(role);
            var viewModel = new RoleEditModel(role.UniqueId);
            Assert.AreEqual(role.UniqueId, viewModel.UniqueId);
            Assert.AreEqual(role.Name, viewModel.Name);
            Assert.AreEqual(role.Description, viewModel.Description);
            Assert.AreEqual(role.Type.UniqueId, viewModel.Type);
            Assert.AreEqual(RoleTypes.Instance.Count, viewModel.ActiveRoleTypes.Count());
        }

        [TestMethod]
        public void ConstructorWithIdThrows() {
            // ReSharper disable once UnusedVariable
            try { var roleEditModel = new RoleEditModel(role.UniqueId); }
            catch (ValidationException ex) {
                Assert.AreEqual($"Role with id '{role.UniqueId}' not found.", ex.Message);
            }
        }

        [TestMethod]
        public void SomeRoleTypesAreNotActive() {
            RoleTypes.Instance[0].Valid.To = DateTime.Now - TimeSpan.FromDays(2);
            var viewModel = new RoleEditModel(role);
            Assert.AreEqual(RoleTypes.Instance.Count - 1, viewModel.ActiveRoleTypes.Count());
        }

        [TestMethod]
        public void ExistingRoleWithNotActiveRoleType() {
            role.Type.Valid.To = DateTime.Now - TimeSpan.FromDays(2);
            var viewModel = new RoleEditModel(role);
            Assert.AreEqual(RoleTypes.Instance.Count, viewModel.ActiveRoleTypes.Count());
        }

        [TestMethod]
        public void Create() {
            var viewModel = new RoleEditModel (roleEntity.Object) {
                                                  Name = role.Name,
                                                  Description = role.Description,
                                                  Type = role.TypeId
                                              };
            var rolesCountBefore = Roles.Instance.Count;
            viewModel.Create();
            Assert.AreEqual(rolesCountBefore + 1, Roles.Instance.Count);

            var newRole = Roles.Instance[rolesCountBefore];
            Assert.AreEqual(role.Name, newRole.Name);
            Assert.AreEqual(role.Description, newRole.Description);
            Assert.AreEqual(role.TypeId, newRole.TypeId);
            roleEntity.Verify(m => m.Create(newRole), Times.Once);
        }

        [TestMethod]
        public void Update() {
            var viewModel = new RoleEditModel(role, roleEntity.Object);
            var editedRole = Role.Random();
            Roles.Instance.Add(editedRole);
            viewModel.Update(editedRole.UniqueId);
            Assert.AreNotEqual(role.UniqueId, editedRole.UniqueId);
            Assert.IsTrue(StringValue.HasValue(editedRole.UniqueId));
            Assert.AreEqual(role.Name, editedRole.Name);
            Assert.AreEqual(role.Description, editedRole.Description);
            Assert.AreEqual(role.Type.UniqueId, editedRole.TypeId);
            roleEntity.Verify(m => m.Update(editedRole), Times.Once);

        }

        [TestMethod]
        public void UpdateRoleDoesNotExist() {
            try {
                var viewModel = new RoleEditModel();
                viewModel.Update("1111");
            }
            catch (ValidationException ex) {
                Assert.AreEqual("Role with id \'1111\' not found.", ex.Message);
            }
        }

        [TestMethod]
        public void Delete() {
            role.Valid.To = DateTime.MaxValue;
            role.Valid.From = DateTime.MinValue;
            Roles.Instance.Add(role);
            var viewModel = new RoleEditModel(role, roleEntity.Object);
            Assert.IsTrue(role.IsActive);
            viewModel.Delete();
            roleEntity.Verify(m=>m.Delete(role), Times.Once);
        }

        [TestMethod]
        public void DeleteRoleDoesNotExist() {
            try {
                var viewModel = new RoleEditModel(role);
                viewModel.Delete();
            }
            catch (ValidationException ex) {
                Assert.AreEqual($"Role with id '{role.UniqueId}' not found.", ex.Message);
            }
        }
    }
}