﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.PartyClasses;
using Open.Archetypes.RoleClasses;
using Open.Data;
using Open.Logic.RoleClasses;

namespace Open.Tests.Logic.RoleClasses {
    [TestClass]
    public class PersonEditModelTests : ClassTests<PersonEditModel> {
        private Mock<PersonEntity> personEntity;
        private Person person;

        [TestInitialize]
        public override void TestInitialize() {
            base.TestInitialize();
            RoleTypes.Instance.AddRange(RoleTypes.Random());
            Roles.Instance.AddRange(Roles.Random());
            Roles.Instance.ToList().ForEach(role => role.Valid = new Period { From = DateTime.MinValue, To = DateTime.MaxValue });
            person = Person.Random();
            person.Valid.From = DateTime.Now - TimeSpan.FromHours(1);
            person.Valid.To = DateTime.MaxValue;
            personEntity = new Mock<PersonEntity>();
        }

        [TestCleanup]
        public override void TestCleanup() {
            Roles.Instance.Clear();
        }

        [TestMethod]
        public void DefaultConstructor() {
            var viewModel = new PersonEditModel();
            Assert.IsNull(viewModel.UniqueId);
            Assert.IsNull(viewModel.Name);
            Assert.IsNull(viewModel.Surname);
            Assert.IsInstanceOfType(viewModel.PartyRoles, typeof(Roles));
            Assert.AreEqual(viewModel.PartyRoles.Count, 0);
            Assert.IsTrue(viewModel.DateOfBirth == DateTime.MinValue);
        }

        [TestMethod]
        public void ConstructorWithPerson() {
            var viewModel = new PersonEditModel(person);
            Assert.AreEqual(viewModel.UniqueId, person.UniqueId);
            Assert.AreEqual(viewModel.Name, person.Name);
            Assert.AreEqual(viewModel.Surname, person.Surname);
            Assert.IsTrue(viewModel.PartyRoles.SequenceEqual(person.Roles));
            Assert.AreEqual(viewModel.DateOfBirth, person.DateOfBirth);
        }

        [TestMethod]
        public void ConstructorWithPersonId() {
            Parties.Instance.Add(person);
            var viewModel = new PersonEditModel(person.UniqueId);
            Assert.AreEqual(viewModel.UniqueId, person.UniqueId);
            Assert.AreEqual(viewModel.Name, person.Name);
            Assert.AreEqual(viewModel.Surname, person.Surname);
            Assert.IsTrue(viewModel.PartyRoles.SequenceEqual(person.Roles));
            Assert.AreEqual(viewModel.DateOfBirth, person.DateOfBirth);
        }

        [TestMethod]
        public void ConstructorWithIdThrows() {
            // ReSharper disable once UnusedVariable
            try {
                var roleEditModel = new PersonEditModel(person.UniqueId);
                Assert.Fail("Expected exception wasn't thrown.");
            }
            catch (ValidationException ex) {
                Assert.AreEqual($"Party with id '{person.UniqueId}' not found.", ex.Message);
            }
        }

        [TestMethod]
        public void Create()
        {
            var model = new PersonEditModel(person, personEntity.Object);
            var partiesCount = Parties.Instance.Count;
            model.Create();
            Assert.IsTrue(partiesCount+1 == Parties.Instance.Count);
            var newPerson = (Person) Parties.Instance[partiesCount];
            Assert.AreNotEqual(person.UniqueId, newPerson.UniqueId);
            Assert.AreEqual(person.Name, newPerson.Name);
            Assert.AreEqual(person.Surname, newPerson.Surname);
            Assert.AreEqual(person.DateOfBirth, newPerson.DateOfBirth);
            Assert.IsTrue(person.Roles.SequenceEqual(newPerson.Roles));
            personEntity.Verify(m=>m.Create(newPerson, model.PartyRoles), Times.Once);
        }

        [TestMethod]
        public void Update() {
            var secondPerson = Person.Random();
            Parties.Instance.Add(person);
            var model = new PersonEditModel(secondPerson, personEntity.Object);
            model.Update(person.UniqueId);
            Assert.AreNotEqual(person.UniqueId, secondPerson.UniqueId);
            Assert.AreEqual(person.Name, secondPerson.Name);
            Assert.AreEqual(person.Surname, secondPerson.Surname);
            Assert.AreEqual(person.DateOfBirth, secondPerson.DateOfBirth);
            Assert.IsTrue(person.Roles.SequenceEqual(secondPerson.Roles));
            personEntity.Verify(m => m.Update(person, model.PartyRoles), Times.Once);
        }

        [TestMethod]
        public void UpdateThrows()
        {
            try {
                var model = new PersonEditModel();
                model.Update(person.UniqueId);
                Assert.Fail("Expected exception wasn't thrown.");
            }
            catch (ValidationException ex)
            {
                Assert.AreEqual($"Party with id '{person.UniqueId}' not found.", ex.Message);
            }
        }

        [TestMethod]
        public void Delete()
        {
            Parties.Instance.Add(person);
            var model = new PersonEditModel(person, personEntity.Object);
            var roles = person.Roles;
            model.Delete();
            Assert.IsTrue(person.Valid.To <= DateTime.Now);
            personEntity.Verify(m => m.Update(person, roles), Times.Once);
        }

        [TestMethod]
        public void DeleteThrows()
        {
            try
            {
                var model = new PersonEditModel(person);
                model.Delete();
                Assert.Fail("Expected exception wasn't thrown.");
            }
            catch (ValidationException ex)
            {
                Assert.AreEqual($"Party with id '{person.UniqueId}' not found.", ex.Message);
            }
        }

        [TestMethod]
        public void ActiveRolesAllActive()
        {
            Parties.Instance.Add(person);
            var model = new PersonEditModel(person);
            Assert.AreEqual(model.AvailableRoles.Count(), Roles.Instance.Count);
        }

        [TestMethod]
        public void ActiveRolesOneInactive() {
            var role = Role.Random();
            role.Valid = new Period {To = DateTime.Now - TimeSpan.FromMilliseconds(2)};
            Roles.Instance.Add(role);
            Parties.Instance.Add(person);
            var model = new PersonEditModel(person);
            Assert.IsTrue(model.AvailableRoles.Count() == Roles.Instance.Count - 1);
        }

        [TestMethod]
        public void ActiveRolesOneActiveBelongToCurrentPerson()
        {
            var role = Role.Random();
            role.Valid = new Period { To = DateTime.Now - TimeSpan.FromMilliseconds(2) };
            Roles.Instance.Add(role);
            role.PerformerId = person.UniqueId;
            Parties.Instance.Add(person);
            var model = new PersonEditModel(person);
            Assert.AreEqual(Roles.Instance.Count - 1, model.AvailableRoles.Count());
        }
    }
}