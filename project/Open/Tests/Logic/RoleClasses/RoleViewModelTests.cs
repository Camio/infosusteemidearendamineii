﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.RoleClasses;
using Open.Logic.RoleClasses;
using System.ComponentModel.DataAnnotations;

namespace Open.Tests.Logic.RoleClasses {
    [TestClass]
    public class RoleViewModelTests : ClassTests<RoleViewModel> {
        [TestMethod]
        public void DefaultConstructor() {
            var viewModel = new RoleViewModel();
            Assert.IsNull(viewModel.UniqueId);
            Assert.IsNull(viewModel.Name);
            Assert.IsNull(viewModel.Description);
            Assert.IsNull(viewModel.Type);
            Assert.IsFalse(viewModel.IsActive);
        }

        [TestMethod]
        public void ConstructorWithRole() {
            var role = Role.Random();
            var viewModel = new RoleViewModel(role);
            Assert.AreEqual(role.UniqueId, viewModel.UniqueId);
            Assert.IsInstanceOfType(viewModel.UniqueId, typeof(string));
            Assert.AreEqual(role.Name, viewModel.Name);
            Assert.IsInstanceOfType(viewModel.Name, typeof(string));
            Assert.AreEqual(role.Description, viewModel.Description);
            Assert.IsInstanceOfType(viewModel.Description, typeof(string));
            Assert.AreEqual(role.Type.Name, viewModel.Type);
            Assert.IsInstanceOfType(viewModel.Type, typeof(string));
            Assert.AreEqual(true, viewModel.IsActive);
            Assert.IsInstanceOfType(viewModel.IsActive, typeof(bool));
        }

        [TestMethod]
        public void ConstructorWithRoleId() {
            var role = Role.Random();
            Roles.Instance.Add(role);
            var viewModel = new RoleViewModel(role.UniqueId);
            Assert.AreEqual(role.UniqueId, viewModel.UniqueId);
            Assert.AreEqual(role.Name, viewModel.Name);
            Assert.AreEqual(role.Description, viewModel.Description);
            Assert.AreEqual(role.Type.Name, viewModel.Type);
            Assert.AreEqual(true, viewModel.IsActive);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException), "Role with id abcd not found.")]
        public void ConstructorWithRoleIdThrows() {
            var role = Role.Random();
            role.UniqueId = "abcd";
            // ReSharper disable once UnusedVariable
            var roleViewModel = new RoleViewModel(role.UniqueId);
        }
    }
}