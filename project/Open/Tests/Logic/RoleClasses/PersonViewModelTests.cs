﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.PartyClasses;
using Open.Archetypes.RoleClasses;
using Open.Logic.RoleClasses;

namespace Open.Tests.Logic.RoleClasses {
    [TestClass]
    public class PersonViewModelTests : ClassTests<PersonViewModel> {
        private Person person { get; set; }

        [TestInitialize]
        public override void TestInitialize() {
            base.TestInitialize();
            RoleTypes.Instance.AddRange(RoleTypes.Random());
            Roles.Instance.AddRange(Roles.Random());
            person = Person.Random();
        }

        [TestMethod]
        public void DefaultConstructor() {
            var viewModel = new PersonViewModel();
            Assert.IsNull(viewModel.UniqueId);
            Assert.IsNull(viewModel.Name);
            Assert.IsNull(viewModel.Surname);
            Assert.IsNull(viewModel.Roles);
            Assert.IsTrue(viewModel.DateOfBirth == DateTime.MinValue);
            Assert.IsTrue(viewModel.RegistrationDate == DateTime.MinValue);
        }

        [TestMethod]
        public void ConstructorWithPerson() {
            var viewModel = new PersonViewModel(person);
            Assert.AreEqual(viewModel.UniqueId, person.UniqueId);
            Assert.AreEqual(viewModel.Name, person.Name);
            Assert.AreEqual(viewModel.Surname, person.Surname);
            Assert.IsTrue(viewModel.Roles.SequenceEqual(person.Roles));
            Assert.AreEqual(viewModel.DateOfBirth, person.DateOfBirth);
            Assert.AreEqual(viewModel.RegistrationDate, person.Valid.From);
        }

        [TestMethod]
        public void ConstructorWithPersonId() {
            Parties.Instance.Add(person);
            var viewModel = new PersonViewModel(person.UniqueId);
            Assert.AreEqual(viewModel.UniqueId, person.UniqueId);
            Assert.AreEqual(viewModel.Name, person.Name);
            Assert.AreEqual(viewModel.Surname, person.Surname);
            Assert.IsTrue(viewModel.Roles.SequenceEqual(person.Roles));
            Assert.AreEqual(viewModel.DateOfBirth, person.DateOfBirth);
            Assert.AreEqual(viewModel.RegistrationDate, person.Valid.From);
        }


        [TestMethod]
        public void ConstructorWithIdThrows()
        {
            // ReSharper disable once UnusedVariable
            try { var roleEditModel = new PersonViewModel(person.UniqueId); }
            catch (ValidationException ex)
            {
                Assert.AreEqual($"Party with id '{person.UniqueId}' not found.", ex.Message);
            }
        }
    }
}