﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Open.Aids;
using Open.Archetypes.RoleClasses;
using Open.Data;
using Open.Logic.RoleClasses;

namespace Open.Tests.Logic.RoleClasses {
    [TestClass]
    public class RoleTypeEditModelTests : ClassTests<RoleTypeEditModel> {
        private Mock<RoleTypeEntity> roleTypeEntity;
        private RoleType roleType { get; set; }

        [TestInitialize]
        public override void TestInitialize() {
            base.TestInitialize();
            roleType = RoleType.Random();
            roleTypeEntity = new Mock<RoleTypeEntity>();
        }

        [TestMethod]
        public void DefaultConstructor() {
            var viewModel = new RoleTypeEditModel();
            Assert.IsNull(viewModel.UniqueId);
            Assert.IsNull(viewModel.Name);
            Assert.IsNull(viewModel.Description);
            Assert.IsFalse(viewModel.IsActive);
        }

        [TestMethod]
        public void ConstructorWithRoleType() {
            var viewModel = new RoleTypeEditModel(roleType);
            Assert.AreEqual(roleType.UniqueId, viewModel.UniqueId);
            Assert.IsInstanceOfType(viewModel.UniqueId, typeof(string));
            Assert.AreEqual(roleType.Name, viewModel.Name);
            Assert.IsInstanceOfType(viewModel.Name, typeof(string));
            Assert.AreEqual(roleType.Description, viewModel.Description);
            Assert.IsInstanceOfType(viewModel.Description, typeof(string));
            Assert.AreEqual(true, viewModel.IsActive);
            Assert.IsInstanceOfType(viewModel.IsActive, typeof(bool));
        }

        [TestMethod]
        public void ConstructorWithRoleTypeId() {
            RoleTypes.Instance.Add(roleType);
            var viewModel = new RoleTypeEditModel(roleType.UniqueId);
            Assert.AreEqual(roleType.UniqueId, viewModel.UniqueId);
            Assert.AreEqual(roleType.Name, viewModel.Name);
            Assert.AreEqual(roleType.Description, viewModel.Description);
            Assert.AreEqual(true, viewModel.IsActive);
        }

        [TestMethod]
        public void Create() {
            var viewModel =
                new RoleTypeEditModel(roleTypeEntity.Object) {
                                                                 UniqueId = "id",
                                                                 Name = "name",
                                                                 Description = "descr"
                                                             };
            var countBefore = RoleTypes.Instance.Count;
            viewModel.Create();
            var countAfter = RoleTypes.Instance.Count;
            var addedRoleType = RoleTypes.Instance[RoleTypes.Instance.Count - 1];
            Assert.AreEqual(countBefore + 1, countAfter);
            Assert.IsTrue(StringValue.HasValue(addedRoleType.UniqueId));
            Assert.AreNotEqual(addedRoleType.UniqueId, viewModel.UniqueId);
            Assert.AreEqual(addedRoleType.Name, viewModel.Name);
            Assert.AreEqual(addedRoleType.Description, viewModel.Description);
            Assert.AreEqual(false, viewModel.IsActive);
            roleTypeEntity.Verify(m => m.Create(addedRoleType), Times.Once());
        }

        [TestMethod]
        public void Update() {
            var viewModel =
                new RoleTypeEditModel(roleTypeEntity.Object) {
                                                                 UniqueId = "id",
                                                                 Name = "name",
                                                                 Description = "descr"
                                                             };
            RoleTypes.Instance.Add(roleType);
            viewModel.Update(roleType.UniqueId);
            Assert.IsTrue(StringValue.HasValue(roleType.UniqueId));
            Assert.AreNotEqual(roleType.UniqueId, viewModel.UniqueId);
            Assert.AreEqual(roleType.Name, viewModel.Name);
            Assert.AreEqual(roleType.Description, viewModel.Description);
            Assert.AreEqual(false, viewModel.IsActive);
            roleTypeEntity.Verify(m => m.Update(roleType), Times.Once());
        }

        [TestMethod]
        public void UpdateRoleTypeDoesNotExist() {
            try {
                var viewModel = new RoleTypeEditModel(roleType);
                viewModel.Update(roleType.UniqueId);
            }
            catch (ValidationException ex) {
                Assert.AreEqual($"Role type with id '{roleType.UniqueId}' not found.", ex.Message);
            }
        }

        [TestMethod]
        public void Delete() {
            roleType.Valid.To = DateTime.MaxValue;
            roleType.Valid.From = DateTime.MinValue;
            RoleTypes.Instance.Add(roleType);
            var viewModel = new RoleTypeEditModel(roleType.UniqueId, roleTypeEntity.Object);
            Assert.IsTrue(roleType.IsActive);
            viewModel.Delete();
            roleTypeEntity.Verify(m => m.Delete(roleType), Times.Once());
        }

        [TestMethod]
        public void DeleteRoleTypeDoesNotExist() {
            try {
                var viewModel = new RoleTypeEditModel(roleType);
                viewModel.Delete();
            }
            catch (ValidationException ex) {
                Assert.AreEqual($"Role type with id '{roleType.UniqueId}' not found.", ex.Message);
            }
        }
    }
}