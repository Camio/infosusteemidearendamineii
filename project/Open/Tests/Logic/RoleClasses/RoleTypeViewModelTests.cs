﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Open.Archetypes.RoleClasses;
using Open.Logic.RoleClasses;

namespace Open.Tests.Logic.RoleClasses {
    [TestClass]
    public class RoleTypeViewModelTests : ClassTests<RoleTypeViewModel> {
        [TestMethod]
        public void DefaultConstructor() {
            var viewModel = new RoleTypeViewModel();
            Assert.IsNull(viewModel.UniqueId);
            Assert.IsNull(viewModel.Name);
            Assert.IsNull(viewModel.Description);
            Assert.IsFalse(viewModel.IsActive);
        }

        [TestMethod]
        public void ConstructorWithRoleType() {
            var roleType = RoleType.Random();
            var viewModel = new RoleTypeViewModel(roleType);
            Assert.AreEqual(roleType.UniqueId, viewModel.UniqueId);
            Assert.IsInstanceOfType(viewModel.UniqueId, typeof(string));
            Assert.AreEqual(roleType.Name, viewModel.Name);
            Assert.IsInstanceOfType(viewModel.Name, typeof(string));
            Assert.AreEqual(roleType.Description, viewModel.Description);
            Assert.IsInstanceOfType(viewModel.Description, typeof(string));
            Assert.AreEqual(true, viewModel.IsActive);
            Assert.IsInstanceOfType(viewModel.IsActive, typeof(bool));
        }

        [TestMethod]
        public void ConstructorWithRoleTypeId() {
            var roleType = RoleType.Random();
            RoleTypes.Instance.Add(roleType);
            var viewModel = new RoleTypeViewModel(roleType.UniqueId);
            Assert.AreEqual(roleType.UniqueId, viewModel.UniqueId);
            Assert.AreEqual(roleType.Name, viewModel.Name);
            Assert.AreEqual(roleType.Description, viewModel.Description);
            Assert.AreEqual(true, viewModel.IsActive);
        }
    }
}