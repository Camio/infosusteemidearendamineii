namespace Open.Archetypes.QuantityClasses {
    public static class ManHour {
        public const string MeasureName = "ManHour";
        public static Measure Measure => Measures.Add(MeasureName);
        public static Unit Unit => Units.Add(MeasureName, Measure);
        public static bool Initialize() {
            if (Measure == null) return false;
            return Unit != null;
        }
    }
}