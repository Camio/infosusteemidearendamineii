using Open.Aids;
namespace Open.Archetypes.QuantityClasses {
    public static class Area {
        public const string MeasureName = "Area";
        public const string SquareMillimetersName = "SquareMillimeters";
        public const string SquareCentimetersName = "SquareCentimeters";
        public const string SquareDecimetersName = "SquareDecimeters";
        public const string SquareMetersName = "SquareMeters";
        public const string SquareDecametersName = "SquareDecameters";
        public const string SquareHectometersName = "SquareHectometers";
        public const string SquareKilometersName = "SquareKilometers";
        public const string SquareInchesName = "SquareInches";
        public const string SquareFeetName = "SquareFeet";
        public const string SquareYardsName = "SquareYards";
        public const string SquareMilesName = "SquareMiles";
        public const string AcresName = "Acres";
        public const string CentiaresName = "Centares";
        public const string AresName = "Ares";
        public const string HectaresName = "Hectares";
        public const string SquareRodsName = "SquareRods";
        public static Measure Measure => measure();
        public static Unit SquareMeters => unit(Distance.Meters, SquareMetersName);
        public static Unit SquareMillimeters => unit(Distance.Millimeters, SquareMillimetersName) ;
        public static Unit SquareCentimeters => unit(Distance.Centimeters, SquareCentimetersName) ;
        public static Unit SquareDecimeters => unit(Distance.Decimeters, SquareDecimetersName);
        public static Unit SquareDecameters => unit(Distance.Decameters, SquareDecametersName);
        public static Unit SquareHectometers => unit(Distance.Hectometers, SquareHectometersName);
        public static Unit SquareKilometers => unit(Distance.Kilometers, SquareKilometersName);
        public static Unit SquareInches => unit(Distance.Inches, SquareInchesName);
        public static Unit SquareFeet => unit(Distance.Feet, SquareFeetName);
        public static Unit SquareYards => unit(Distance.Yards, SquareYardsName);
        public static Unit SquareMiles => unit(Distance.Miles, SquareMilesName);
        public static Unit Acres => unit();
        public static Unit Centiares => unit(Distance.Meters, CentiaresName);
        public static Unit Ares => unit(Distance.Decameters, AresName);
        public static Unit Hectares => unit(Distance.Hectometers, HectaresName);
        public static Unit SquareRods => unit(Distance.Rods, SquareRodsName);
        private static Measure measure() {
            return Safe.Run(() => {
                var d = Distance.Measure;
                var t = new MeasureTerm(d, 2);
                var terms = new MeasureTerms {t};
                return Measures.Add(terms, MeasureName);
            }, Measure.Empty);
        }
        private static Unit unit(Unit u, string n) {
            return Safe.Run(() => {
                var t = new UnitTerm(u, 2);
            var terms = new UnitTerms {t};
            return Units.Add(n, terms, Measure);
            }, Unit.Empty);
        }
        private static Unit unit() {
            return Safe.Run(() => {
                var t1 = new UnitTerm(Distance.Furlongs, 1);
                var t2 = new UnitTerm(Distance.Chains, 1);
                var terms = new UnitTerms {t1, t2};
                return Units.Add(AcresName, terms, Measure);
            }, Unit.Empty);
        }
        public static bool Initialize() {
            if (Measure == null) return false;
            if (SquareMeters == null) return false;
            if (SquareMillimeters == null) return false;
            if (SquareCentimeters == null) return false;
            if (SquareDecimeters == null) return false;
            if (SquareDecameters == null) return false;
            if (SquareHectometers == null) return false;
            if (SquareKilometers == null) return false;
            if (SquareInches == null) return false;
            if (SquareFeet == null) return false;
            if (SquareYards == null) return false;
            if (SquareMiles == null) return false;
            if (Acres == null) return false;
            if (Centiares == null) return false;
            if (Ares == null) return false;
            if (Hectares == null) return false;
            return SquareRods != null;
        }
    }
}