﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Open.Aids;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.RuleClasses;

namespace Open.Archetypes.QuantityClasses {
    [KnownType(typeof(UnitBase))]
    [KnownType(typeof(UnitDerived))]
    [KnownType(typeof(UnitFactored))]
    [KnownType(typeof(UnitFunctioned))]
    [XmlInclude(typeof(UnitBase))]
    [XmlInclude(typeof(UnitFactored))]
    [XmlInclude(typeof(UnitFunctioned))]
    [XmlInclude(typeof(UnitDerived))]
    public class Units : Archetypes<Unit> {
        public static Units Instance { get; } = new Units();
        public static Units Random() {
            var l = new Units();
            var c = GetRandom.Count();
            for (var i = 0; i < c; i++) l.Add(Unit.Random());
            return l;
        }
        public static Units FindAll(string measureId) {
            var u = new Units();
            var l = Instance.FindAll(x => x.GetMeasure.IsThis(measureId));
            u.AddRange(l);
            return u;
        }
        public static Unit Add(string name, UnitTerms t, Measure m, string symbol = null,
            string definition = null, SystemOfUnits s = null) {
            if (IsNull(t)) return Add(name, m, symbol, definition, s);
            m = m ?? Measure.Empty;
            s = s ?? SystemOfUnits.Empty;
            name = name ?? t.Formula(true);
            symbol = symbol ?? name;
            definition = definition ?? name;
            var u = new UnitDerived(name, t, m, symbol, definition, s);
            return add(u);
        }
        public static Unit Add(string name, double factor, Measure m, string symbol = null,
            string definition = null, SystemOfUnits s = null)
        {
            if (IsSpaces(name)) return Unit.Empty;
            m = m ?? Measure.Empty;
            s = s ?? SystemOfUnits.Empty;
            symbol = symbol ?? name;
            definition = definition ?? name;
            var u = new UnitFactored(name, factor, m, symbol, definition, s);
            return add(u);
        }
        public static Unit Add(string name, Measure m, string symbol = null,
            string definition = null, SystemOfUnits s = null) {
            if (IsSpaces(name)) return Unit.Empty;
            m = m ?? Measure.Empty;
            s = s ?? SystemOfUnits.Empty;
            symbol = symbol ?? name;
            definition = definition ?? name;
            var u = new UnitBase(name, m, symbol, definition, s);
            return add(u);
        }
        private static Unit add(Unit u) {
            var x = Find(u.Name);
            if (!IsNull(x) && !x.IsEmpty()) return x;
            u.UniqueId = u.Name;
            Instance.Add(u);
            return u;
        }
        public static Unit Find(string uniqueId) {
            foreach (var e in Instance) {
                var b = e.IsThis(uniqueId);
                if (b) return e;
            }
            return Unit.Empty;
        }
        public static Unit Add(string name, Rule ruleToBase, Rule ruleFromBase, Measure m, string symbol = null, string definition = null, SystemOfUnits s = null)
        {
            if (IsSpaces(name)) return Unit.Empty;
            m = m ?? Measure.Empty;
            s = s ?? SystemOfUnits.Empty;
            ruleToBase = ruleToBase ?? Rule.Empty;
            ruleFromBase = ruleFromBase ?? Rule.Empty;
            symbol = symbol ?? name;
            definition = definition ?? name;
            var u = new UnitFunctioned(name, ruleToBase, ruleFromBase, m, symbol, definition, s);
            return add(u);
        }
    }
}
