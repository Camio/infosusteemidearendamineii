﻿using Open.Aids;
namespace Open.Archetypes.QuantityClasses {
    public abstract class Measure : Metric {
        protected Measure() : this(string.Empty) { }
        protected Measure(string name, string symbol = null, string definition = null)
            : base(name, symbol, definition) {}
        public static Measure Empty { get; } = new MeasureBase {isReadOnly = true};
        public Units GetUnits() {
            var u = Units.FindAll(Name);
            var l = new Units();
            l.AddRange(u);
            return l;
        }
        public Measure Divide(Measure m, string name = null, string code = null, string note = null) {
            return Multiply(IsNull(m) ? null : m.Inverse(), name, code, note);
        }
        public Measure Inverse(string name = null, string code = null, string note = null) {
            return Power(-1, name, code, note);
        }
        public virtual Measure Multiply(Measure m, string name = null, string code = null,
            string note = null) {
            var l = new MeasureTerms {new MeasureTerm(this, 1)};
            var d = m as MeasureDerived;
            if (IsNull(d)) l.Add(new MeasureTerm(m, 1));
            else l.AddRange(d.Terms);
            return Measures.Add(l, name, code, note);
        }
        public virtual Measure Power(int power, string name = null, string code = null,
            string note = null) {
            var l = new MeasureTerms();
            var t = new MeasureTerm(this, power);
            l.Add(t);
            return Measures.Add(l, name, code, note);
        }
        public override bool IsEmpty() { return Equals(Empty); }
        public static Measure Random() {
            var i = GetRandom.Int32();
            if (i % 2 == 0) return MeasureBase.Random();
            return MeasureDerived.Random();
        }
    }
}
