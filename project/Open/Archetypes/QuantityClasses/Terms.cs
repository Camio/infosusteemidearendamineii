﻿using System.Collections.Generic;
using System.Linq;
using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.QuantityClasses {
    public abstract class Terms<T> : Archetypes<T> where T: ITerm {
        protected string elementPattern => "({0})";
        protected string multiplyPattern => "{0}*{1}";
        protected Terms() : this(null) { }
        protected Terms(IEnumerable<T> elements) : base(elements) { }
        public override void AddRange(IList<T> terms)
        {
            Safe.Run(() => {
                if (IsReadOnly) return;
                isAddRange = true;
                foreach (var e in terms) Add(e);
                isAddRange = false;
                doOnChanged(terms);
            });
        }
        public override void Insert(int index, T term)
        {
            if (IsReadOnly) return;
            if (IsNull(term)) return;
            var o = Find(x => isThis(x, term));
            if (IsNull(o)) base.Insert(index, term);
            else Add(term);
        }
        public string Formula(bool longFormula = false)
        {
            var r = string.Empty;
            var l = new List<string>();
            foreach (var e in this)
            {
                var f = e.Formula(longFormula);
                var s = string.Format(elementPattern, f);
                l.Add(s);
            }
            l.Sort();
            r = l.Aggregate(r,
                (current, e) =>
                    string.IsNullOrEmpty(current)
                        ? e
                        : string.Format(multiplyPattern, current, e));
            return r.Trim();
        }

    }
}
