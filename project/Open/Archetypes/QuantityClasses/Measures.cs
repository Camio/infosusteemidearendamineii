﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.QuantityClasses {
    [KnownType(typeof(MeasureBase))]
    [KnownType(typeof(MeasureDerived))]
    [XmlInclude(typeof(MeasureBase))]
    [XmlInclude(typeof(MeasureDerived))]
    public class Measures : Archetypes<Measure> {
        public static Measures Instance { get; } = new Measures();
        public static Measures Random() {
            var l = new Measures();
            var c = GetRandom.Count();
            for (var i = 0; i < c; i++) l.Add(Measure.Random());
            return l;
        }
        public static Measure Add(MeasureTerms t, string name = null, string symbol = null,
            string definition = null) {
            if (IsNull(t)) return Add(name, symbol, definition);
            name = name ?? t.Formula();
            symbol = symbol ?? name;
            definition = definition ?? name;
            var m = new MeasureDerived(t, name, symbol, definition);
            return add(m);
        }
        public static Measure Add(string name, string symbol = null, string definition = null)
        {
            if (IsSpaces(name)) return Measure.Empty;
            symbol = symbol ?? name;
            definition = definition ?? name;
            var m = new MeasureBase(name, symbol, definition);
            return add(m);
        }
        private static Measure add(Measure m) { return insert(0, m); }
        internal static Measure Insert(byte ind, string name, string symbol = null,
            string definition = null) {
            if (IsSpaces(name)) return Measure.Empty;
            symbol = symbol ?? name;
            definition = definition ?? name;
            var m = new MeasureBase(name, symbol, definition);
            return insert(ind, m);
        }
        public static Measure Find(string id) { return Instance.Find(x => x.IsThis(id))?? Measure.Empty; }
        private static Measure insert(int idx, Measure m) {
            var x = Find(m.Name);
            if (!IsNull(x) && !x.IsEmpty()) return x;
            m.UniqueId = m.Name;
            if (idx < 1) Instance.Add(m);
            else Instance.Insert(idx, m);
            return m;
        }
    }
}

