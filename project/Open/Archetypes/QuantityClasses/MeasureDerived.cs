﻿namespace Open.Archetypes.QuantityClasses {
    public sealed class MeasureDerived : Measure {
        private MeasureTerms terms;
        public MeasureDerived() : this(null) { }
        public MeasureDerived(MeasureTerms t, string name = null,
            string symbol = null, string definition = null) : base(name, symbol, definition) {
            terms = t;
        }
        public MeasureTerms Terms {
            get { return SetDefault(ref terms); }
            set { SetValue(ref terms, value); }
        }
        public override string Formula(bool longFormula = false) {
            return Terms.Formula(longFormula);
        }
        public override Measure Power(int power, string name=null, string code=null,
            string note= null) {
            var list = Terms.Power(power);
            var t = new MeasureTerms();
            t.AddRange(list);
            return Measures.Add(t, name, code, note);
        }
        public override Measure Multiply(Measure m, string name = null,
            string code = null, string note = null) {
            var l = Terms.Multiply(m);
            if (l.IsListEmpty()) return Empty;
            if (l.IsMeasureBase()) return Measures.Add(l[0].Measure);
            var t = new MeasureTerms();
            t.AddRange(l);
            return Measures.Add(t, name, code, note);
        }
        public new static MeasureDerived Random() {
            var b = new MeasureDerived();
            b.setRandomValues();
            return b;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            terms = MeasureTerms.Random();
        }
    }
}
