﻿using System;
using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.QuantityClasses {
    public sealed class Quantity : Value, IComparable {
        public const string DefaultFormat = "{0} {1}";
        private double amount;
        private string unit;
        public Quantity() : this(QuantityClasses.Unit.Empty) { }
        public Quantity(Unit u, double amount = 0) {
            unit = (u ?? QuantityClasses.Unit.Empty).UniqueId;
            this.amount = amount;
        }
        public double Amount { get { return amount; } set { SetValue(ref amount, value); } }
        public string Unit {
            get { return SetDefault(ref unit); }
            set { SetValue(ref unit, value); }
        }
        public Unit GetUnit => Units.Find(Unit);
        public static Quantity Random() {
            var d = GetRandom.Double();
            var u = UnitBase.Random();
            return new Quantity(u, d);
        }
        public static Quantity Empty { get; } = new Quantity(QuantityClasses.Unit.Empty, double.NaN) {
            isReadOnly = true
        };
        public override bool IsEmpty() { return Equals(Empty); }
        public Quantity Round(Rounding policy) {
            var d = round(amount, policy);
            return new Quantity(GetUnit, d);
        }
        private static double round(double d, Rounding p) {
            switch (p.Strategy) {
                case Approximate.Up:
                    return QuantityClasses.Round.Up(d, p.Decimals);
                case Approximate.Down:
                    return QuantityClasses.Round.Down(d, p.Decimals);
                case Approximate.UpByStep:
                    return QuantityClasses.Round.UpByStep(d, p.Step);
                case Approximate.DownByStep:
                    return QuantityClasses.Round.DownByStep(d, p.Step);
                case Approximate.TowardsPositive:
                    return QuantityClasses.Round.TowardsPositive(d, p.Decimals);
                case Approximate.TowardsNegative:
                    return QuantityClasses.Round.TowardsNegative(d, p.Decimals);
                default:
                    return QuantityClasses.Round.Off(d, p.Decimals, p.Digit);
            }
        }
        public Quantity ConvertTo(Unit u) {
            u = u ?? QuantityClasses.Unit.Empty;
            var d = convertTo(amount, u);
            return new Quantity(u, d);
        }
        private double convertTo(double d, Unit u) {
            if (ReferenceEquals(null, u)) return double.NaN;
            if (!isSameMeasure(u)) return double.NaN;
            d = GetUnit.ToBase(d);
            return u.FromBase(d);
        }
        private bool isSameMeasure(Unit u) {
            return GetUnit.GetMeasure.IsSameContent(u.GetMeasure);
        }
        public int CompareTo(Quantity q) {
            if (IsNull(q)) return int.MinValue;
            var u2 = q.GetUnit;
            var u1 = GetUnit;
            var q2 = u2.ToBase(q.Amount);
            var q1 = u1.ToBase(Amount);
            if (!isSameMeasure(u2)) return int.MaxValue;
            return q1.CompareTo(q2);
        }
        public int CompareTo(object o) { return CompareTo(o as Quantity); }
        public Quantity Add(Quantity q) {
            if (IsNull(q)) return Empty;
            if (!isSameMeasure(q.GetUnit)) return Empty;
            var d = GetUnit.ToBase(Amount);
            d = d + q.GetUnit.ToBase(q.Amount);
            return new Quantity(q.GetUnit, q.GetUnit.FromBase(d));
        }
        public Quantity Subtract(Quantity q) {
            if (IsNull(q)) return Empty;
            if (!isSameMeasure(q.GetUnit)) return Empty;
            var d = GetUnit.ToBase(Amount);
            d = d - q.GetUnit.ToBase(q.Amount);
            return new Quantity(q.GetUnit, q.GetUnit.FromBase(d));
        }
        public Quantity Multiply(double d) { return new Quantity(GetUnit, Amount * d); }
        public Quantity Divide(double d) { return new Quantity(GetUnit, Amount / d); }
        public Quantity Multiply(Quantity q) {
            if (ReferenceEquals(null, q)) return Empty;
            var d = GetUnit.ToBase(Amount);
            d = d * q.GetUnit.ToBase(q.Amount);
            var u = GetUnit.Multiply(q.GetUnit);
            return new Quantity(u, u.FromBase(d));
        }
        public Quantity Divide(Quantity q) {
            if (ReferenceEquals(null, q)) return Empty;
            var d = GetUnit.ToBase(Amount);
            d = d / q.GetUnit.ToBase(q.Amount);
            var u = GetUnit.Divide(q.GetUnit);
            return new Quantity(u, u.FromBase(d));
        }
    }
}

