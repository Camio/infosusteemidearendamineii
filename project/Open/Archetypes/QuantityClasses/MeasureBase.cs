﻿using Open.Aids;
namespace Open.Archetypes.QuantityClasses {
    public sealed class MeasureBase : Measure {
        public MeasureBase() : this(string.Empty) { }
        public MeasureBase(string name, string symbol = null, string definition = null)
            : base(name, symbol, definition) {}
        public bool IsValidQuantity(Quantity value) {
            return Safe.Run(() => GetUnits().Contains(value.GetUnit), false);
        }
        public new static MeasureBase Random() {
            var b = new MeasureBase();
            b.setRandomValues();
            return b;
        }
    }
}
