namespace Open.Archetypes.QuantityClasses {
    public static class Percentage { 
        public const string MeasureName = "Percentage";
        public static Measure Measure => Measures.Add(MeasureName);
        public static Unit Unit => QuantityClasses.Units.Add(MeasureName, Measure);
        public static bool Initialize() {
            if (Measure == null) return false;
            return Unit != null;
        }
    }
}