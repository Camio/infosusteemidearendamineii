﻿using Open.Aids;
namespace Open.Archetypes.QuantityClasses {
    public sealed class UnitFactored : Unit {
        private double factor;
        public UnitFactored() : this(string.Empty, 0.0, QuantityClasses.Measure.Empty) { }
        public UnitFactored(string name, double factor, Measure m,
            string symbol = null, string definition = null, SystemOfUnits s = null )
            : base(name, m, symbol, definition, s ) {
            this.factor = factor;
        }
        public double Factor {
            get { return SetDefault(ref factor); }
            set { SetValue(ref factor, value); }
        }
        public override double FromBase(double d) { return d/Factor; }
        public new static UnitFactored Random() {
            var b = new UnitFactored();
            b.setRandomValues();
            return b;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            factor = GetRandom.Double(-300, 300);
        }
        public override double ToBase(double d) { return d*Factor; }
    }
}
