﻿using Open.Aids;
namespace Open.Archetypes.QuantityClasses {
    public abstract class Unit : Metric {
        private string measure;
        private string systemOfUnits;
        protected Unit() : this(string.Empty) { }
        protected Unit(string name) : this(name, QuantityClasses.Measure.Empty) { }
        protected Unit(string name, Measure m, string symbol = null, string definition = null,
            SystemOfUnits s = null) : base(name, symbol, definition) {
            if (!IsNull(m)) measure = m.UniqueId;
            if (!IsNull(s)) systemOfUnits = s.UniqueId;
        }
        protected Unit(string name, string measure, string symbol = null, string definition = null,
            string systemOfUnits = null) : base(name, symbol, definition) {
            this.measure = measure;
            this.systemOfUnits = systemOfUnits;
        }
        public string Measure {
            get { return SetDefault(ref measure); }
            set { SetValue(ref measure, value );}
        }
        public Unit Divide(Unit unit, string name = null, string symbol = null, string definition = null) {
            if (ReferenceEquals(null, unit)) return Multiply(null, name, symbol, definition);
            return Multiply(unit.Inverse(), name, symbol, definition);
        }
        public static Unit Empty { get; } = new UnitBase {isReadOnly = true};
        public abstract double FromBase(double d);
        public Measure GetMeasure => Measures.Find(Measure)?? QuantityClasses.Measure.Empty;
        public SystemOfUnits GetSystemOfUnits() {
            return SystemsOfUnits.Find(SystemOfUnits) ?? QuantityClasses.SystemOfUnits.Empty;
        }
        public Unit Inverse(string name = null, string symbol = null, string definition = null) {
            var u = Power(-1, name, symbol, definition);
            return u;
        }
        public override bool IsEmpty() { return Equals(Empty); }
        public virtual Unit Multiply(Unit u, string name = null, string symbol = null,
            string definition = null) {
            var l = new UnitTerms();
            var t = new UnitTerm(this, 1);
            l.Add(t);
            var m = QuantityClasses.Measure.Empty;
            var s = QuantityClasses.SystemOfUnits.GetCommon(this, u);
            if (!ReferenceEquals(null, u)) {
                m = GetMeasure.Multiply(u.GetMeasure);
                if (m.IsEmpty()) return Empty;
                var d = u as UnitDerived;
                if (d != null) {
                    l.AddRange(d.Terms);
                } else {
                    t = new UnitTerm(u, 1);
                    l.Add(t);
                }
            }
            var r = Units.Add(name, l, m, symbol, definition, s);
            return r;
        }
        public virtual Unit Power(int power, string name = null, string symbol = null,
            string definition = null) {
            var m = GetMeasure.Power(power);
            var t = new UnitTerms();
            var n = new UnitTerm(this, power);
            t.Add(n);
            return Units.Add(name, t, m, symbol, definition, GetSystemOfUnits());
        }
        public static Unit Random() {
            var i = GetRandom.UInt8()%4;
            if (i == 3) return UnitFunctioned.Random();
            if (i == 2) return UnitDerived.Random();
            if (i == 1) return UnitFactored.Random();
            return UnitBase.Random();
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            measure = MeasureBase.Random().Name;
            systemOfUnits = QuantityClasses.SystemOfUnits.Random().Name;
        }
        public string SystemOfUnits {
            get { return SetDefault(ref systemOfUnits, string.Empty); }
            set { SetValue(ref systemOfUnits, value); }
        }
        public abstract double ToBase(double d);
        public bool IsBaseUnit() { return this is UnitBase; }
        public bool IsDerivedUnit() { return this is UnitDerived; }
        public bool IsFactoredUnit() { return this is UnitFactored; }
        public bool IsFunctionedUnit() { return this is UnitFunctioned; }
    }
}
