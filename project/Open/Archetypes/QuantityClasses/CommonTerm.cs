﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.QuantityClasses {
    public abstract class CommonTerm<T> : Archetype, ITerm where T : Metric {
        protected int power;
        protected string metric;
        protected CommonTerm() : this(null) { }
        protected CommonTerm(Metric m, int power = 0) {
            metric = IsNull(m) ? string.Empty : string.IsNullOrEmpty(m.UniqueId) ? m.Formula() : m.UniqueId;
            this.power = power;
        }
        public string Formula(bool longFormula = false) {
            var m = getMetric();
            if (IsNull(m)) return string.Empty;
            var n = longFormula ? m.Name : m.Symbol;
            if (IsSpaces(n)) n = metric;
            return string.Format(powerPattern, n, Power);
        }
        public int Power {
            get { return SetDefault(ref power); }
            set { SetValue(ref power, value); }
        }
        public void ToPower(int i) { power += i; }
        protected abstract T getMetric();
        protected static string powerPattern => "{0}^{1}";
        protected override void setRandomValues() {
            base.setRandomValues();
            power = GetRandom.Int8();
            metric = GetRandom.String(5, 10);
        }
    }
}
