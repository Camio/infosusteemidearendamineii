﻿namespace Open.Archetypes.QuantityClasses {
    public sealed class SystemOfUnits : Metric {
        public SystemOfUnits() : this(null) { }
        public SystemOfUnits(string name, string symbol = null,
            string definition = null) : base(name, symbol, definition) {}
        public static SystemOfUnits Random() {
            var s = new SystemOfUnits();
            s.setRandomValues();
            return s;
        }
        public override string Formula(bool longFormula = false) {
            return longFormula ? Name : Symbol;
        }
        public static SystemOfUnits Empty { get; } = new SystemOfUnits {isReadOnly = true};
        public override bool IsEmpty() { return Equals(Empty); }
        public static SystemOfUnits GetCommon(Unit u1, Unit u2) {
            if (IsNull(u1)) return Empty;
            if (IsNull(u2)) return Empty;
            var s1 = u1.GetSystemOfUnits();
            var s2 = u2.GetSystemOfUnits();
            if (IsNull(s1)) return Empty;
            if (IsNull(s2)) return Empty;
            return s1.IsThis(s2.Name) ? s1: Empty;
        }
    }
}
