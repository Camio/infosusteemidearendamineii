﻿namespace Open.Archetypes.QuantityClasses {
    public enum Approximate {
        Up,
        Down,
        Common,
        UpByStep,
        DownByStep,
        TowardsPositive,
        TowardsNegative
    }
}
