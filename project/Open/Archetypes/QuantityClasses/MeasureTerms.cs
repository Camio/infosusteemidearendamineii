﻿using System.Collections.Generic;
using Open.Aids;
namespace Open.Archetypes.QuantityClasses {
    public class MeasureTerms : Terms<MeasureTerm> {
        public MeasureTerms() : this(null) { }
        public MeasureTerms(IEnumerable<MeasureTerm> elements) : base(elements) {}
        public override void Add(MeasureTerm item) {
            if (IsReadOnly) return;
            if (IsNull(item)) return;
            var t = new MeasureTerm(item.GetMeasure, item.Power);
            MeasureTerm i = null;
            if (Count > 0) i = Find(o => isThis(o, t));
            if (!IsNull(i)) {
                t.ToPower(i.Power);
                Remove(i);
            }
            if (t.Power != 0) base.Add(t);
        }
        public static MeasureTerms Empty { get; } = new MeasureTerms { isReadOnly=true };
        public bool IsMeasureBase() {
            if (IsNull(list)) return false;
            return (list.Count == 1) && (list[0].Power == 1);
        }
        public bool IsListEmpty() {
            return list.Count == 0;
        }
        public override bool IsEmpty() { return Equals(Empty); }
        protected override bool isThis(MeasureTerm x, MeasureTerm y) {
            return Safe.Run(() => x.GetMeasure.IsSameContent(y.GetMeasure),
                false);
        }
        public MeasureTerms Divide(Measure m) { return Multiply(m.Inverse()); }
        public MeasureTerms Inverse() { return Power(-1); }
        private static MeasureTerms multiply(MeasureTerms t, MeasureDerived m) {
            multiplyTersm(t, m.Terms);
            return t;
        }
        public MeasureTerms Multiply(Measure m) {
            var l = new MeasureTerms();
            if (IsNull(m)) return l;
            multiplyTersm(l, this);
            var d = m as MeasureDerived;
            if (d != null) return multiply(l, d);
            if (!IsSpaces(m.Name)) l.Add(new MeasureTerm(m, 1));
            return l;
        }
        private static void multiplyTersm(MeasureTerms to, MeasureTerms from) {
            foreach (var e in from) {
                var x = e.GetMeasure;
                if (string.IsNullOrEmpty(x.Name.Trim())) continue;
                var t = new MeasureTerm(x, e.Power);
                to.Add(t);
            }
        }
        public MeasureTerms Power(int i) {
            var l = new MeasureTerms();
            foreach (var e in this) {
                var t = new MeasureTerm(e.GetMeasure, e.Power*i);
                l.Add(t);
            }
            return l;
        }
        public static MeasureTerms Random() {
            var t = new MeasureTerms();
            for (var i = 0; i < GetRandom.Count(1, 3); i++)
                t.Add(MeasureTerm.Random());
            return t;
        }

    }
}
