using Open.Archetypes.RuleClasses;
namespace Open.Archetypes.QuantityClasses {
    public static class Temperature {
        public const string MeasureName = "Temperature";
        public const string CelsiusName = "Celsius";
        public const string KelvinName = "Kelvin";
        public const string FahrenheitName = "Fahrenheit";
        public const string RankineName = "Rankine";
        public const double CelciusFactor = 273.15;
        public const double FahrenheitFactor = 459.67;
        public const double RankineFactor = 1.8000;
        public const string FromCelsius = "Celsius to Kelvin";
        public const string ToCelsius = "Kelvin to Celcius";
        public const string FromFahrenheit = "Farenheit to Kelvin";
        public const string ToFahrenheit = "Kelvin to Farenheit";
        public const string FromRankine = "Frankine to Kelvin";
        public const string ToRankine = "Kelvin to Rankine";
        public static Measure Measure => Measures.Add(MeasureName);
        private static readonly Measure m = Measure;
        public static Unit Kelvin => Units.Add(KelvinName, m);
        public static Unit Celsius => Units.Add(CelsiusName, CelciusToKelvin, KelvinToCelcius, m);
        public static Unit Fahrenheit
            => Units.Add(FahrenheitName, FahrenheitToKelvin, KelvinToFahrenheit, m);
        public static Unit Rankine => Units.Add(RankineName, RankineToKelvin, KelvinToRankine, m);
        public static Rule CelciusToKelvin {
            get {
                var rule = new RuleElements {
                    new Operand {Name = UnitFunctioned.VariableToken},
                    new DoubleVariable {Name = "x", Value = CelciusFactor},
                    new Operator {Operation = Operation.Add}
                };
                return Rules.Add(FromCelsius, rule);
            }
        }
        public static Rule KelvinToCelcius {
            get {
                var rule = new RuleElements {
                    new Operand {Name = UnitFunctioned.VariableToken},
                    new DoubleVariable {Name = "x", Value = CelciusFactor},
                    new Operator {Operation = Operation.Subtract}
                };
                return Rules.Add(ToCelsius, rule);
            }
        }
        public static Rule KelvinToFahrenheit {
            get {
                var rule = new RuleElements {
                    new Operand {Name = UnitFunctioned.VariableToken},
                    new DoubleVariable {Name = "x", Value = RankineFactor},
                    new Operator {Operation = Operation.Multiply},
                    new DoubleVariable {Name = "y", Value = FahrenheitFactor},
                    new Operator {Operation = Operation.Subtract}
                };
                return Rules.Add(ToFahrenheit, rule);
            }
        }
        public static Rule FahrenheitToKelvin {
            get {
                var rule = new RuleElements {
                    new Operand {Name = UnitFunctioned.VariableToken},
                    new DoubleVariable {Name = "x", Value = FahrenheitFactor},
                    new Operator {Operation = Operation.Add},
                    new DoubleVariable {Name = "y", Value = RankineFactor},
                    new Operator {Operation = Operation.Divide}
                };
                return Rules.Add(FromFahrenheit, rule);
            }
        }
        public static Rule KelvinToRankine {
            get {
                var rule = new RuleElements {
                    new Operand {Name = UnitFunctioned.VariableToken},
                    new DoubleVariable {Name = "x", Value = RankineFactor},
                    new Operator {Operation = Operation.Multiply}
                };
                return Rules.Add(ToRankine, rule);
            }
        }
        public static Rule RankineToKelvin {
            get {
                var rule = new RuleElements {
                    new Operand {Name = UnitFunctioned.VariableToken},
                    new DoubleVariable {Name = "x", Value = RankineFactor},
                    new Operator {Operation = Operation.Divide}
                };
                return Rules.Add(FromRankine, rule);
            }
        }
        public static bool Initialize() {
            if (Measure == null) return false;
            if (Celsius == null) return false;
            if (Fahrenheit == null) return false;
            if (Kelvin == null) return false;
            return Rankine != null;
        }
    }
}