﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
namespace Open.Archetypes.QuantityClasses {
    [KnownType(typeof(UnitBase))]
    [KnownType(typeof(UnitDerived))]
    [KnownType(typeof(UnitFactored))]
    [KnownType(typeof(UnitFunctioned))]
    [XmlInclude(typeof(UnitBase))]
    [XmlInclude(typeof(UnitFactored))]
    [XmlInclude(typeof(UnitFunctioned))]
    [XmlInclude(typeof(UnitDerived))]
    public sealed class UnitTerm : CommonTerm<Unit> {
        public UnitTerm() : this(null) { }
        public UnitTerm(Unit u, int power = 0) : base(u, power) { }
        public string Unit {
            get { return SetDefault(ref metric); }
            set { SetValue(ref metric, value); }
        }
        public Unit GetUnit {
            get {
                var u = Units.Instance.Find(o => o.IsThis(Unit));
                return !IsNull(u) ? u : QuantityClasses.Unit.Empty;
            }
        }
        protected override Unit getMetric() { return GetUnit; }
        public static UnitTerm Random() {
            var t = new UnitTerm();
            t.setRandomValues();
            return t;
        }
        public static UnitTerm Empty { get; } = new UnitTerm {isReadOnly = true};
        public override bool IsEmpty() { return Equals(Empty); }
    }
}