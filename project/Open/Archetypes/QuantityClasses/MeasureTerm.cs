﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
namespace Open.Archetypes.QuantityClasses {
    [KnownType(typeof(MeasureBase))]
    [KnownType(typeof(MeasureDerived))]
    [XmlInclude(typeof(MeasureBase))]
    [XmlInclude(typeof(MeasureDerived))]
    public sealed class MeasureTerm : CommonTerm<Measure> {
        public MeasureTerm() : this(null) { }
        public MeasureTerm(Measure m, int power = 0) : base(m, power) { }
        public string Measure {
            get { return SetDefault(ref metric); }
            set { SetValue(ref metric, value); }
        }
        public Measure GetMeasure {
            get {
                try {
                    if (IsSpaces(Measure)) return QuantityClasses.Measure.Empty;
                    var u = Measures.Instance.Find(o => o.IsThis(Measure));
                    return !IsNull(u) ? u : QuantityClasses.Measure.Empty;
                } catch (Exception) {
                    return null;
                }
            }
        }
        protected override Measure getMetric() { return GetMeasure; }
        public static MeasureTerm Empty { get; } = new MeasureTerm {isReadOnly = true};
        public override bool IsEmpty() { return Equals(Empty); }
        public static MeasureTerm Random() {
            var t = new MeasureTerm();
            t.setRandomValues();
            return t;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            Measure = MeasureBase.Random().Name;
        }
    }
}