﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.QuantityClasses {
    public sealed class SystemsOfUnits : Archetypes<SystemOfUnits>
    {
        public static SystemsOfUnits Instance { get; } = new SystemsOfUnits();
        public static SystemsOfUnits Random()
        {
            var l = new SystemsOfUnits();
            var c = GetRandom.Count(3, 5);
            for (var i = 0; i < c; i++) l.Add(SystemOfUnits.Random());
            return l;
        }
        public static SystemOfUnits Add(string name, string symbol = null,
            string definition = null)
        {
            if (IsSpaces(name)) return SystemOfUnits.Empty;
            symbol = symbol ?? name;
            definition = definition ?? name;
            var s = new SystemOfUnits(name, symbol, definition);
            return add(s);
        }
        private static SystemOfUnits add(SystemOfUnits s) {
            var x = Find(s.Name);
            if (!IsNull(x)) return x;
            s.UniqueId = s.Name;
            Instance.Add(s);
            return s;
        }
        public static SystemOfUnits Find(string anyId) {
            return Instance.Find(x => x.IsThis(anyId));
        }
    }
}


