using Open.Aids;
namespace Open.Archetypes.QuantityClasses {
    public static class Volume {
        public const string MeasureName = "Volume";
        public const string CubicMillimetersName = "CubicMillimeters";
        public const string CubicCentimetersName = "CubicCentimeters";
        public const string CubicDecimetersName = "CubicDecimeters";
        public const string CubicMetersName = "CubicMeters";
        public const string CubicDecametersName = "CubicDecameters";
        public const string CubicHectometersName = "CubicHectometers";
        public const string CubicKilometersName = "CubicKilometers";
        public const string CubicInchesName = "CubicInches";
        public const string CubicFeetName = "CubicFeet";
        public const string CubicYardsName = "CubicYards";
        public const string CubicMilesName = "CubicMiles";
        public const string MilliLitersName = "MilliLiters";
        public const string CentiLitersName = "CentiLiters";
        public const string DeciLitersName = "DeciLiters";
        public const string LitersName = "Liters";
        public const string DecaLitersName = "DecaLiters";
        public const string HectoLitersName = "HectoLiters";
        public const string KiloLitersName = "KiloLiters";
        public static Measure Measure => measure();
        public static Unit CubicMillimeters => unit(Distance.Millimeters, CubicMillimetersName);
        public static Unit CubicCentimeters => unit(Distance.Centimeters, CubicCentimetersName);
        public static Unit CubicDecimeters => unit(Distance.Decimeters, CubicDecimetersName);
        public static Unit CubicMeters => unit(Distance.Meters, CubicMetersName);
        public static Unit CubicDecameters => unit(Distance.Decameters, CubicDecametersName);
        public static Unit CubicHectometers => unit(Distance.Hectometers, CubicHectometersName);
        public static Unit CubicKilometers => unit(Distance.Kilometers, CubicKilometersName);
        public static Unit CubicInches => unit(Distance.Inches, CubicInchesName);
        public static Unit CubicFeet => unit(Distance.Feet, CubicFeetName);
        public static Unit CubicYards => unit(Distance.Yards, CubicYardsName);
        public static Unit CubicMiles => unit(Distance.Miles, CubicMilesName);
        public static Unit MilliLiters => unit(Distance.Centimeters, MilliLitersName);
        public static Unit CentiLiters => unit(Distance.Centimeters, Distance.Decimeters, CentiLitersName);
        public static Unit DeciLiters => unit(Distance.Centimeters, Distance.Meters, DeciLitersName);
        public static Unit Liters => unit(Distance.Decimeters, LitersName);
        public static Unit DecaLiters => unit(Distance.Decimeters, Distance.Meters, DecaLitersName);
        public static Unit HectoLiters => unit(Distance.Meters, Distance.Decimeters, HectoLitersName);
        public static Unit KiloLiters => unit(Distance.Meters, KiloLitersName);
        private static Measure measure() {
            return Safe.Run(() => {
                var d = Distance.Measure;
                var t = new MeasureTerm(d, 3);
                var terms = new MeasureTerms { t };
                return Measures.Add(terms, MeasureName);
            }, Measure.Empty);
        }
        private static Unit unit(Unit u, string n) {
            return Safe.Run(() => {
                var t = new UnitTerm(u, 3);
                var terms = new UnitTerms { t };
                return Units.Add(n, terms, Measure);
            }, Unit.Empty);
        }
        private static Unit unit(Unit u1, Unit u2, string n) {
            return Safe.Run(() => {
                var t1 = new UnitTerm(u1, 2);
                var t2 = new UnitTerm(u2, 1);
                var terms = new UnitTerms { t1, t2 };
                return Units.Add(n, terms, Measure);
            }, Unit.Empty);
        }
        public static bool Initialize() {
        if (Measure ==null) return false;
        if (CubicMillimeters == null) return false;
            if (CubicCentimeters == null) return false;
            if (CubicDecimeters == null) return false;
            if (CubicMeters == null) return false;
            if (CubicDecameters == null) return false;
            if (CubicHectometers == null) return false;
            if (CubicKilometers == null) return false;
            if (CubicInches == null) return false;
            if (CubicFeet == null) return false;
            if (CubicYards == null) return false;
            if (CubicMiles == null) return false;
            if (MilliLiters == null) return false;
            if (CentiLiters == null) return false;
            if (DeciLiters == null) return false;
            if (Liters == null) return false;
            if (DecaLiters == null) return false;
            if (HectoLiters == null) return false;
            return KiloLiters != null;
    }
}
}