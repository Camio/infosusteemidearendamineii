﻿using Open.Archetypes.RuleClasses;
namespace Open.Archetypes.QuantityClasses {
    public sealed class UnitFunctioned : Unit {
        public const string VariableToken = "Variable";
        private string toBaseId;
        private string fromBaseId;
        public Rule GetToBaseRule() { return Rules.Find(ToBaseRuleId); }
        public Rule GetFromBaseRule() { return Rules.Find(FromBaseRuleId); }
        public UnitFunctioned()
            : this(string.Empty, Rule.Empty, Rule.Empty, QuantityClasses.Measure.Empty) {}
        public UnitFunctioned(string name, Rule toBase, Rule fromBase,
            Measure m, string symbol = null,
            string description = null, SystemOfUnits s = null) : base(name, m, symbol, description, s) {
            toBaseId = IsNull(toBase) ? string.Empty : toBase.Name;
            fromBaseId = IsNull(fromBase) ? string.Empty : fromBase.Name;
        }
        public string ToBaseRuleId {
            get { return SetDefault(ref toBaseId); }
            set { SetValue(ref toBaseId, value); }
        }
        public string FromBaseRuleId {
            get { return SetDefault(ref fromBaseId); }
            set { SetValue(ref fromBaseId, value); }
        }
        private static double calculate(double d, Rule s) {
            var c = new RuleContext();
            c.Variables.Add(new DoubleVariable {Name = VariableToken, Value = d});
            var r = s.Calculate(c) as DoubleVariable;
            return IsNull(r) ? double.NaN : r.Value;
        }
        public override double ToBase(double d) {
            return calculate(d, GetToBaseRule());
        }
        public override double FromBase(double d) {
            return calculate(d, GetFromBaseRule());
        }
        public new static UnitFunctioned Random() {
            var b = new UnitFunctioned();
            b.setRandomValues();
            return b;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            toBaseId = Rule.Random().Name;
            fromBaseId = Rule.Random().Name;
        }
    }
}
