﻿namespace Open.Archetypes.QuantityClasses {
    public sealed class UnitBase : Unit {
        public UnitBase() : this(string.Empty) { }
        public UnitBase(string name) : this(name, QuantityClasses.Measure.Empty) { }
        public UnitBase(string name, Measure m, string symbol = null,
            string definition = null, SystemOfUnits s = null) : base(name, m, symbol, definition, s) {}
        public override double FromBase(double d) { return d; }
        public override double ToBase(double d) { return d; }
        public new static UnitBase Random() {
            var b = new UnitBase();
            b.setRandomValues();
            return b;
        }
    }
}
