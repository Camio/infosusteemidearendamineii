﻿using System.Collections.Generic;
using Open.Aids;
namespace Open.Archetypes.QuantityClasses {
    public class UnitTerms : Terms<UnitTerm> {
        public UnitTerms() : this(null) { }
        public UnitTerms(IEnumerable<UnitTerm> elements) : base(elements) { }
        public static UnitTerms Empty { get; } = new UnitTerms {isReadOnly = true};
        public override bool IsEmpty() { return Equals(Empty); }
        public override void Add(UnitTerm item) {
            if (IsReadOnly) return;
            if (IsNull(item)) return;
            var t = new UnitTerm(item.GetUnit, item.Power); 
            UnitTerm i = null;
            if (list.Count > 0) i = list.Find(o => isThis(o, item));
            if (!IsNull(i)) {
                t.Power = t.Power + i.Power;
                Remove(i);
            }
            if (t.Power != 0) base.Add(t);
        }
        protected override bool isThis(UnitTerm x, UnitTerm y) {
            return Safe.Run(() => x.GetUnit.IsSameContent(y.GetUnit), false);
        }
        public UnitTerms Power(int i) {
            var l = new UnitTerms();
            foreach (var e in this)
                l.Add(new UnitTerm(e.GetUnit, e.Power*i));
            return l;
        }
        public UnitTerms Inverse() { return Power(-1); }
        public UnitTerms Multiply(Unit m) {
            var l = new UnitTerms();
            if (ReferenceEquals(null, m)) return l;
            multiplyTerms(l, this);
            var d = m as UnitDerived;
            if (!IsNull(d)) multiplyTerms(l, d.Terms);
            else if (!string.IsNullOrEmpty(m.Name.Trim()))
                l.Add(new UnitTerm(m, 1));
            return l;
        }
        private static void multiplyTerms(UnitTerms to, UnitTerms from) {
            foreach (var e in from) {
                var x = e.GetUnit;
                if (string.IsNullOrEmpty(x.Name.Trim())) continue;
                to.Add(new UnitTerm(x, e.Power));
            }
        }
        public UnitTerms Divide(Unit m) { return Multiply(m.Inverse()); }
        public static UnitTerms Random() {
            var t = new UnitTerms();
            for (var i = 0; i < GetRandom.Count(1,3); i++)
                t.Add(UnitTerm.Random());
            return t;
        }
    }
}
