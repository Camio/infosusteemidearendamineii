﻿
namespace Open.Archetypes.QuantityClasses {
    public interface ITerm {
        int Power { get; }
        string Formula(bool longFormula = false);
    }
}
