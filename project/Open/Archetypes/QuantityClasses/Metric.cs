using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.QuantityClasses {
    public abstract class Metric : UniqueEntity {
        private string symbol;
        private string name;
        private string definition;
        public static string FormulaPattern => "({0}^1)";
        protected Metric() : this(null) { }
        protected Metric(string name, string symbol = null, string definition = null): base(name) {
            this.name = name?? string.Empty;
            this.symbol = symbol?? name;
            this.definition = definition?? name;
        }
        public string Symbol {
            get { return SetDefault(ref symbol); }
            set { SetValue(ref symbol, value); }
        }
        public string Name {
            get { return SetDefault(ref name); }
            set { SetValue(ref name, value); }
        }
        public string Definition {
            get { return SetDefault(ref definition); }
            set { SetValue(ref definition, value); }
        }
        public virtual string Formula(bool longFormula = false) {
            var n = longFormula ? Name : Symbol;
            return IsSpaces(n) ? string.Empty : string.Format(FormulaPattern, n);
        }
        public static bool IsFormula(string s) { return !IsNull(s) && s.Contains("^"); }
        protected override void setRandomValues() {
            base.setRandomValues();
            name = GetRandom.String(5, 10);
            symbol = GetRandom.String(3, 5);
            definition = GetRandom.String();
        }
        public virtual bool IsThis(string id) {
            if (IsSpaces(id)) return false;
            if (UniqueId == id) return true;
            if (Name == id) return true;
            if (Symbol == id) return true;
            if (!IsFormula(id)) return false;
            if (Formula() == id) return true;
            return Formula(true) == id;
        }
        public virtual bool IsSameFormula(Metric m) {
            if (IsNull(m)) return false;
            var f1 = Formula(true);
            var f2 = m.Formula(true);
            return f1 == f2;
        }
    }
}