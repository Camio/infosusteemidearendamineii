﻿
namespace Open.Archetypes.QuantityClasses {
    public static class Counter {
        public const string MeasureName = "Counter";
        public const string UnitName = "Units";
        public static Measure Measure => Measures.Add(MeasureName);
        public static Unit Units => QuantityClasses.Units.Add(UnitName, Measure);
        public static bool Initialize() {
            if (Measure == null) return false;
            return Units != null;
        }
    }
}
