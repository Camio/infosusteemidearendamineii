﻿namespace Open.Archetypes.QuantityClasses {
    public sealed class UnitDerived : Unit {
        private UnitTerms terms;
        public UnitDerived()
            : this(string.Empty, null, null) {}
        public UnitDerived(string name, UnitTerms t, Measure m,
            string symbol = null, string definition = null, SystemOfUnits s = null)
            : base(name, m, symbol, definition, s) {
            terms = t;
        }
        public UnitTerms Terms {
            get { return SetDefault(ref terms); }
            set { SetValue(ref terms, value); }
        }
        public override double ToBase(double d) {
            var result = d;
            foreach (var term in Terms) {
                var unit = term.GetUnit;
                var power = term.Power;
                if (power == 0) continue;
                if (power > 0) {
                    for (var i = 1; i <= power; i++) {
                        var f = unit.ToBase(1);
                        result *= f;
                    }
                    continue;
                }
                if (power < 0) {
                    for (var i = -1; i >= power; i--) {
                        var f = unit.ToBase(1);
                        result /= f;
                    }
                }
            }
            return result;
        }
        public override double FromBase(double d) {
            var result = d;
            foreach (var term in Terms) {
                var unit = term.GetUnit;
                var power = term.Power;
                if (power == 0)
                    continue;
                if (power > 0) {
                    for (var i = 1; i <= power; i++) {
                        result = result * unit.FromBase(1);
                    }
                    continue;
                }
                if (power < 0) {
                    for (var i = -1; i >= power; i--) {
                        result = result / unit.FromBase(1);
                    }
                }
            }
            return result;
        }
        public override Unit Power(int power, string name = null,
            string code = null, string note = null) {
            var l = Terms.Power(power);
            var m = GetMeasure.Power(power);
            return Units.Add(name, l, m, code, note, GetSystemOfUnits());
        }
        public override Unit Multiply(Unit unit, string name = null,
            string code = null, string note = null) {
            var l = Terms.Multiply(unit);
            var m = GetMeasure.Multiply(unit.GetMeasure);
            if (ReferenceEquals(null, l)) return Empty;
            if (l.Count == 0) return Empty;
            if ((l.Count == 1) && (l[0].Power == 1)) {
                var u = l[0].GetUnit;
                if (ReferenceEquals(null, u)) return Empty;
                if (u.IsEmpty()) return u;
                return Units.Add(u.Name, u.GetMeasure,
                    u.Symbol, u.Definition, u.GetSystemOfUnits());
            }
            return Units.Add(name, l, m, code, note, GetSystemOfUnits());
        }
        public new static UnitDerived Random() {
            var b = new UnitDerived();
            b.setRandomValues();
            return b;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            terms = UnitTerms.Random();
        }
        public override string Formula(bool longFormula = false) {
            return Terms.Formula(longFormula);
        }
    }
}
