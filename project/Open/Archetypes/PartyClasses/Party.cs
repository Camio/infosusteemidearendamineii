﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.RoleClasses;

namespace Open.Archetypes.PartyClasses {
    public class Party : UniqueEntity {
        [NotMapped]
        public Roles Roles => Roles.GetPerformerRoles(UniqueId);

        public bool IsActive => Valid.IsValid(DateTime.Now);
    }
}