﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Open.Aids;
using Open.Archetypes.BaseClasses;

namespace Open.Archetypes.PartyClasses {
    [KnownType(typeof(Person))]
    [XmlInclude(typeof(Person))]
    public class Parties : Archetypes<Party> {
        public static Parties Instance { get; } = new Parties();

        public static Party Find(string uniqueId) {
            return Instance.Find(x => x.IsThisUniqueId(uniqueId));
        }

        public static Parties Random() {
            var r = new Parties();
            var c = GetRandom.Count();
            for (var i = 0; i < c; i++) r.Add(Person.Random());
            return r;
        }
    }
}