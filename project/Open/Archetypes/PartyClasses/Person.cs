﻿using System;
using Open.Aids;

namespace Open.Archetypes.PartyClasses {
    public class Person : Party {
        public Person() { }

        public Person(string name, string surname, DateTime dateOfBirth) {
            UniqueId = DateTime.Now.Ticks.ToString();
            Name = name;
            Surname = surname;
            DateOfBirth = dateOfBirth;
            Valid.From = DateTime.Now;
        }

        private string name;
        private string surname;
        private DateTime dateOfBirth;

        public string Name {
            get { return SetDefault(ref name); }
            set { SetValue(ref name, value); }
        }
        public string Surname {
            get { return SetDefault(ref surname); }
            set { SetValue(ref surname, value); }
        }

        public DateTime DateOfBirth {
            get { return SetDefault(ref dateOfBirth); }
            set { SetValue(ref dateOfBirth, value); }
        }

        protected override void setRandomValues() {
            base.setRandomValues();
            name = GetRandom.String(5, 7);
            surname = GetRandom.String(5, 7);
            dateOfBirth = GetRandom.DateTime(DateTime.MinValue, DateTime.Now);
            Valid.From = GetRandom.DateTime(DateTime.Now - TimeSpan.FromDays(3650));
            var roles = RoleClasses.Roles.Instance;
            if (roles.Count == 0) roles.AddRange(RoleClasses.Roles.Random());
        }

        public static Person Random() {
            var e = new Person();
            e.setRandomValues();
            return e;
        }
    }
}