﻿using System;
using Open.Aids;
using Open.Archetypes.BaseClasses;

namespace Open.Archetypes.RoleClasses {
    public class RoleType : BaseType<RoleType> {
        private string name;
        private string description;

        public RoleType() { }

        public RoleType(string name, string description) {
            UniqueId = DateTime.Now.Ticks.ToString();
            Name = name;
            Description = description;
            Valid.From = DateTime.Now;
        }

        public string Name {
            get { return SetDefault(ref name); }
            set { SetValue(ref name, value); }
        }

        public string Description {
            get { return SetDefault(ref description); }
            set { SetValue(ref description, value); }
        }

        public bool IsActive => Valid.IsValid(DateTime.Now);

        public RoleConstraints Constraints => RoleConstraints.GetTypeConstraints(UniqueId);

        public static RoleType Random() {
            var t = new RoleType();
            t.setRandomValues();
            return t;
        }

        protected override void setRandomValues() {
            base.setRandomValues();
            name = GetRandom.String();
            description = GetRandom.String();
        }

        public override RoleType Type => RoleTypes.Find(TypeId);
    }
}