﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Open.Aids;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.PartyClasses;

namespace Open.Archetypes.RoleClasses {
    public class Role : BaseEntity<RoleType> {
        private string name;
        private string description;
        private string performerId;

        public Role() { }

        public Role(string name, string description, string typeId) {
            UniqueId = DateTime.Now.Ticks.ToString();
            Name = name;
            Description = description;
            TypeId = typeId;
            Valid.From = DateTime.Now;
        }

        public string Name {
            get { return SetDefault(ref name); }
            set { SetValue(ref name, value); }
        }

        public string Description {
            get { return SetDefault(ref description); }
            set { SetValue(ref description, value); }
        }

        public string PerformerId {
            get { return SetDefault(ref performerId); }
            set { SetValue(ref performerId, value); }
        }

        public bool IsActive => Valid.IsValid(DateTime.Now);

        public Party Performer => Parties.Find(performerId);

        public bool IsPerformerId(string id) { return !IsSpaces(id) && performerId == id; }

        public static Role Random() {
            var e = new Role();
            e.setRandomValues();
            return e;
        }

        protected override void setRandomValues() {
            base.setRandomValues();
            if (RoleTypes.Instance.Count == 0) { RoleTypes.Instance.Add(RoleType.Random()); }
            Name = GetRandom.String();
            Description = GetRandom.String();
            Valid.To = DateTime.MaxValue;
            Valid.From = DateTime.Now;
            TypeId = RoleTypes.Instance.Get(RoleTypes.Instance.Count - 1).UniqueId;
        }

        [ForeignKey("TypeId")]
        public override RoleType Type => RoleTypes.Find(TypeId);
    }
}