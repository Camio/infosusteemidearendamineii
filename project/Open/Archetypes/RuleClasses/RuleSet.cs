﻿using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.RuleClasses {
    public class RuleSet : NamedItem {
        private Rules rules;
        private RuleOverrides overrides;
        public Rules Rules {
            get { return SetDefault(ref rules); }
            set { SetValue(ref rules, value); }
        }
        public RuleOverrides Overrides {
            get { return SetDefault(ref overrides); }
            set { SetValue(ref overrides, value); }
        }
        public new static RuleSet Empty { get; } = new RuleSet {isReadOnly = true};
        public override bool IsEmpty() { return Equals(Empty); }
        public new static RuleSet Random() {
            var r = new RuleSet();
            r.setRandomValues();
            return r;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            overrides = RuleOverrides.Random();
            rules = Rules.Random();
        }
        public Variables Calculate(RuleContext c) {
            var v = Variables.Empty;
            foreach (var r in Rules) {
                var n = r.Name;
                var o =
                    Overrides.Find(
                        x =>
                            !ReferenceEquals(null, x.Variable) &&
                            (x.Variable.Name == n));
                var e = r.Calculate(c, o);
                v.Add(e);
            }
            return v;
        }
    }
}
