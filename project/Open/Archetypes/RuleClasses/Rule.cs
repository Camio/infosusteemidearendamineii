﻿using System;
using Open.Aids;
using Open.Archetypes.BaseClasses;

namespace Open.Archetypes.RuleClasses {
    public class Rule : NamedItem {
        internal const string ruleIsNotSpecifiedMsg = "Rule is not specified";
        internal const string contextIsNotSpecifiedMsg = "RuleContext is not specified";
        internal const string unknownErrorMsg = "Unknown error";

        public Rule() : this(string.Empty) { }

        public Rule(string name, RuleElements e = null) : base(name) { elements = e; }

        private RuleElements elements;
        public RuleElements Elements {
            get { return SetDefault(ref elements); }
            set { SetValue(ref elements, value); }
        }
        public new static Rule Empty { get; } = new Rule {isReadOnly = true};

        public override bool IsEmpty() { return Equals(Empty); }

        public new static Rule Random() {
            var v = new Rule();
            v.setRandomValues();
            return v;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            elements = RuleElements.Random();
        }

        public virtual Operand Calculate(RuleContext c, RuleOverride o = null) {
            return calculate(c, o, Elements);
        }

        protected Operand calculate(RuleContext c, RuleOverride o, RuleElements spec) {
            bool result;
            var value = overrided(o, out result);
            if (result) return value;
            if (ReferenceEquals(null, c)) return Operand.ToVariable(Name, contextIsNotSpecifiedMsg);
            try {
                var s = new Calculator();
                if (spec.Count == 0) return Operand.ToVariable(Name, ruleIsNotSpecifiedMsg);
                for (var i = 0; i < spec.Count; i++) {
                    var e = spec[i];
                    if (e.IsOperand()) e = c.Find(e as Operand);
                    else if (e.IsOperator()) perform(s, e as Operator);
                    if (e.IsVariable()) setVariable(s, e);
                }
                var r = s.Peek();
                return Operand.ToVariable(Name, r);
            }
            catch (Exception ex) { return Operand.ToVariable(Name, Log.Message(ex)); }
        }
        private static void setVariable(Calculator c, RuleElement e) {
            Safe.Run(() => {
                if (e is BooleanVariable) c.Set((e as BooleanVariable).Value);
                else if (e is DateTimeVariable) c.Set((e as DateTimeVariable).Value);
                else if (e is StringVariable) c.Set((e as StringVariable).Value);
                else if (e is IntegerVariable) c.Set((e as IntegerVariable).Value);
                else if (e is DoubleVariable) c.Set((e as DoubleVariable).Value);
            });
        }

        internal void perform(Calculator c, Operator o) { Safe.Run(() => c.Perform(o.Operation)); }

        internal Operand overrided(RuleOverride o, out bool result) {
            result = false;
            if (ReferenceEquals(null, o))
                return Operand.ToVariable(Name, RuleOverride.NotSpecifiedMsg);
            result = true;
            if (ReferenceEquals(null, o.Variable))
                return Operand.ToVariable(Name, RuleOverride.NoVariableMsg);
            if (!o.Variable.Name.Equals(Name))
                return Operand.ToVariable(Name, RuleOverride.NoRuleMsg);
            if (o.Variable.GetType() == typeof(Operand))
                return Operand.ToVariable(Name, RuleOverride.NoValueMsg);
            if (!o.IsSigned()) return Operand.ToVariable(Name, RuleOverride.NotSignedMsg);
            if (!o.IsValidated()) return Operand.ToVariable(Name, RuleOverride.NotValidatedMsg);
            return o.Variable;
        }

        public bool IsSameRule(Rule r) { return Elements.ToString().Equals(r.Elements.ToString()); }

        public bool IsThis(string id) {
            if (IsSpaces(id)) return false;
            if (id.Equals(UniqueId)) return true;
            if (id.Equals(Name)) return true;
            return id.Equals(Elements.ToString());
        }
    }
}