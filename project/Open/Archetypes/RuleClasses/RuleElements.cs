﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.RuleClasses {
    [KnownType(typeof(Operand))]
    [KnownType(typeof(Operator))]
    [KnownType(typeof(StringVariable))]
    [KnownType(typeof(BooleanVariable))]
    [KnownType(typeof(IntegerVariable))]
    [KnownType(typeof(DoubleVariable))]
    [KnownType(typeof(DateTimeVariable))]
    [KnownType(typeof(DecimalVariable))]
    [XmlInclude(typeof(Operand))]
    [XmlInclude(typeof(Operator))]
    [XmlInclude(typeof(StringVariable))]
    [XmlInclude(typeof(BooleanVariable))]
    [XmlInclude(typeof(IntegerVariable))]
    [XmlInclude(typeof(DoubleVariable))]
    [XmlInclude(typeof(DateTimeVariable))]
    [XmlInclude(typeof(DecimalVariable))]
    public class RuleElements : Archetypes<RuleElement> {
        public static RuleElements Random() {
            var r = new RuleElements();
            var count = GetRandom.Count();
            for (var i = 0; i < count; i++) {
                r.Add(RuleElement.Random());
            }
            return r;
        }
        public static RuleElements Empty { get; } = new RuleElements();
        public override bool IsEmpty() { return Equals(Empty); }

    }
}