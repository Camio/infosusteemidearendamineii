﻿namespace Open.Archetypes.RuleClasses {
    public class ActiveRule : Rule {
        private RuleElements activity;
        public RuleElements Activity {
            get { return SetDefault(ref activity); }
            set { SetValue(ref activity, value); }
        }
        public new static ActiveRule Empty { get; } = new ActiveRule();
        public new bool IsEmpty() { return Equals(Empty); }
        public new static ActiveRule Random() {
            var r = new ActiveRule();
            r.setRandomValues();
            r.Activity = RuleElements.Random();
            return r;
        }
        public override Operand Calculate(RuleContext c, RuleOverride o = null) {
            var r = base.Calculate(c, o);
            if (!r.IsVariable()) return r;
            var b = r as BooleanVariable;
            if (ReferenceEquals(null, b)) return r;
            if (!b.Value) return r;
            return calculate(c, o, activity);
        }
    }
}
