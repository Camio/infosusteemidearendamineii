﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.RuleClasses {
    public class Rules : Archetypes<Rule> {
        public static Rules Instance { get; } = new Rules();
        public static Rules Random() {
            var l = new Rules();
            var c = GetRandom.Count(3, 5);
            for (var i = 0; i < c; i++)
                l.Add(Rule.Random());
            return l;
        }
        private static Rule add(Rule r) { return insert(0, r); }
        private static Rule insert(int idx, Rule r)
        {
            var x = Find(r.Name);
            if (!IsNull(x) && !x.IsEmpty()) return x;
            r.UniqueId = r.Name;
            if (idx < 1) Instance.Add(r);
            else Instance.Insert(idx, r);
            return r;
        }
        public static Rule Find(string id) {
            return Instance.Find(o => o.IsThis(id)) ?? Rule.Empty;
        }
        public static Rule Add(string name, RuleElements r) {
            var m = new Rule(name, r);
            return add(m);
        }
        public static Rule Insert(int idx, string name, RuleElements r)
        {
            var m = new Rule(name, r);
            return insert(idx, m);
        }
    }
}

