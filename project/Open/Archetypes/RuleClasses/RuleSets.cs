﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.RuleClasses {
    public class RuleSets : Archetypes<RuleSet> {
        public static RuleSets Random() {
            var l = new RuleSets();
            var c = GetRandom.UInt8(5, 20);
            for (var i = 0; i < c; i++) {
                var item = RuleSet.Random();
                l.Add(item);
            }
            return l;
        }
    }
}
