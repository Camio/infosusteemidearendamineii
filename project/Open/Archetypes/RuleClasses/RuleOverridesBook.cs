﻿using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.RuleClasses {
    public class RuleOverridesBook : Archetypes<RuleOverride> {
        //private static readonly object lockRoot = new object();
        //private static volatile RuleOverridesBook instance;
        //protected override RuleOverrides CreateItems(
        //    Archetypes<RuleOverride> elements) {
        //    var o = new RuleOverrides();
        //    o.AddRange(elements);
        //    return o;
        //}
        //private RuleOverridesBook() { SetBook(new RuleOverrides()); }
        //public static RuleOverridesBook Instance {
        //    get {
        //        if (instance != null) return instance;
        //        lock (lockRoot) {
        //            if (instance == null)
        //                instance = new RuleOverridesBook();
        //        }
        //        return instance;
        //    }
        //}
        //public override RuleOverride Find(RuleOverride overrideItem) {
        //    overrideItem = overrideItem ?? RuleOverride.Empty;
        //    return Find(overrideItem.Variable.Name, overrideItem.Valid);
        //}
        //public RuleOverride Find(string ruleName, DateTimeInterval p) {
        //    ruleName = StringValue.Trim(ruleName);
        //    Func<RuleOverride, bool> find = x => {
        //        if (!x.Variable.Name.Equals(ruleName)) return false;
        //        if (!x.IsValid(p.From)) return false;
        //        if (!x.IsValid(p.To)) return false;
        //        return true;
        //    };
        //    var r = FindBook(o => find(o));
        //    return ReferenceEquals(null, r) ? RuleOverride.Empty : r;
        //}
        //public RuleOverrides FindAll(params string[] anyAttribute) {
        //    var many = new RuleOverrides();
        //    anyAttribute = anyAttribute ?? new string[] {};
        //    Func<RuleOverride, string, bool> find =
        //        (x, y) => x.Variable.Name.Equals(y);
        //    foreach (
        //        var m in anyAttribute.Select(s => FindAllBook(o => find(o, s))))
        //        many.AddRange(m);
        //    return many;
        //}
        //public RuleOverride Add(string party, string why = null,
        //    DateTime? when = null, Operand variable = null,
        //    DateTimeInterval valid = null, PartySignature authorized = null,
        //    DateTime? lastModified = null, string modifiedBy = null,
        //    bool isReadOnly = false) {
        //    var r = new RuleOverride {
        //        Party = {Identifier = party},
        //        Resolution = why,
        //        Valid = {From = when ?? DateTime.MinValue},
        //        Variable = variable
        //    };
        //    r.Valid.To = valid.To;
        //    r.Authorized = authorized;
        //    return Add(r);
        //}
        //public RuleOverride Change(string party, string why = null,
        //    DateTime? when = null, Operand variable = null,
        //    DateTimeInterval valid = null, PartySignature authorized = null,
        //    DateTime? lastModified = null, string modifiedBy = null,
        //    bool isReadOnly = false) {
        //    var r = new RuleOverride {
        //        Party = {Identifier = party},
        //        Resolution = why,
        //        Valid = {From = when ?? DateTime.MinValue},
        //        Variable = variable
        //    };
        //    r.Valid.To = valid.To;
        //    r.Authorized = authorized;
        //    return Change(r);
        //}
    }
}

