﻿using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.RuleClasses {
    public class RuleContext : NamedItem {
        private Variables variables;
        public Variables Variables {
            get { return SetDefault(ref variables); }
            set { SetValue(ref variables, value); }
        }
        public RuleContext() : this(string.Empty) {}
        public RuleContext(string name, Variables v = null) : base(name) {
            variables = v;
        }
        public new static RuleContext Empty { get; } = new RuleContext {isReadOnly = true};
        public new static RuleContext Random() {
            var c = new RuleContext();
            c.setRandomValues();
            c.variables = Variables.Random();
            return c;
        }
        public RuleElement Find(RuleElement e) {
            if (ReferenceEquals(null, e)) return Operand.Empty;
            var v = Variables.Find(o => o.Name.Equals(e.Name));
            return ReferenceEquals(null, v) ? e : v;
        }
        public override bool IsEmpty() { return Equals(Empty); }
    }
}
