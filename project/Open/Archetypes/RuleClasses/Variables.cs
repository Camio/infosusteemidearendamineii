﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.RuleClasses {
    [KnownType(typeof(StringVariable))]
    [KnownType(typeof(BooleanVariable))]
    [KnownType(typeof(IntegerVariable))]
    [KnownType(typeof(DoubleVariable))]
    [KnownType(typeof(DecimalVariable))]
    [KnownType(typeof(DateTimeVariable))]
    [XmlInclude(typeof(StringVariable))]
    [XmlInclude(typeof(BooleanVariable))]
    [XmlInclude(typeof(IntegerVariable))]
    [XmlInclude(typeof(DoubleVariable))]
    [XmlInclude(typeof(DecimalVariable))]
    [XmlInclude(typeof(DateTimeVariable))]
    public class
        Variables : Archetypes<Operand> {
        public static Variables Empty { get; } = new Variables{ isReadOnly = true };
        public override bool IsEmpty() { return Equals(Empty); }
        public static Variables Random() {
            var v = new Variables();
            var c = GetRandom.Count();
            for (var i = 0; i < c; i++) {
                var item = Operand.GetRandomInherited();
                v.Add(item);
            }
            return v;
        }
    }
}