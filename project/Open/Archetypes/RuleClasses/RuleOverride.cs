﻿using System;
using Open.Archetypes.PartyClasses;
namespace Open.Archetypes.RuleClasses {
    public class RuleOverride : PartySignature {
        private Operand variable;
        private PartySignature authorized;
        public const string NotSpecifiedMsg = "Override is not specified";
        public const string NoVariableMsg = "Override variable is not specified";
        public const string NoValueMsg =
            "Value in override variable is not specified";
        public const string NotSignedMsg = "Override is not signed";
        public const string NoRuleMsg =
            "Rule name in override variable is not specified";
        public const string NotValidatedMsg = "Override is not validated";
        public Operand Variable {
            get { return SetDefault(ref variable); }
            set { SetValue(ref variable, value); }
        }
        public PartySignature Authorized {
            get { return SetDefault(ref authorized); }
            set { SetValue(ref authorized, value); }
        }
        public new static RuleOverride Random() {
            var o = new RuleOverride();
            o.setRandomValues();
            o.variable = Operand.Random();
            o.authorized = PartySignature.Random();
            return o;
        }
        public new static RuleOverride Empty { get; } = new RuleOverride();
        public bool IsValidated() { return Authorized.IsSigned(); }
        public bool IsValid(DateTime? dt = null) {
            DateTime d = dt ?? DateTime.Now;
            return Valid.IsValid(d);
        }
        public override bool IsEmpty() { return Equals(Empty); }
    }
}
