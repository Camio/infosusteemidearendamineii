﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.RuleClasses {
    public class RuleOverrides : Archetypes<RuleOverride> {
        public static RuleOverrides Random() {
            var l = new RuleOverrides();
            var c = GetRandom.UInt8(5, 20);
            for (var i = 0; i < c; i++) {
                var item = RuleOverride.Random();
                l.Add(item);
            }
            return l;
        }
    }
}
