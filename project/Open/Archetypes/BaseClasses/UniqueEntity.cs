﻿using System.ComponentModel.DataAnnotations;
using Open.Aids;
namespace Open.Archetypes.BaseClasses {
    public abstract class UniqueEntity : AttributedEntity {
        private string uniqueId;

        protected UniqueEntity() : this(null) { }

        protected UniqueEntity(string id) { uniqueId = id ?? string.Empty; }

        [Key]
        public string UniqueId {
            get { return SetDefault(ref uniqueId); }
            set { SetValue(ref uniqueId, value); }
        }
        public override Attributes Attributes {
            get {
                var a = new Attributes();
                var l = Attributes.Instance.FindAll(o => o.EntityId == UniqueId);
                a.AddRange(l);
                return a;
            }
        }

        public bool IsThisUniqueId(string id) {
            if (IsSpaces(id)) return false;
            return UniqueId == id;
        }

        protected override void setRandomValues() {
            base.setRandomValues();
            uniqueId = GetRandom.String();
        }
    }
}