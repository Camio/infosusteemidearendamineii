﻿using Open.Aids;
namespace Open.Archetypes.BaseClasses {
    public class NamedItem : UniqueEntity {
        private string code;
        private string name;
        private string note;
        public static NamedItem Empty { get; } = new NamedItem {isReadOnly = true};
        public NamedItem() : this(null) { }
        public NamedItem(string name, string code = null, string note = null) {
            this.name = name;
            this.code = code;
            this.note = note;
        }
        public virtual string Symbol {
            get { return SetDefault(ref code, Name); }
            set { SetValue(ref code, value); }
        }
        public virtual string Name {
            get { return SetDefault(ref name); }
            set { SetValue(ref name, value); }
        }
        public virtual string Description {
            get { return SetDefault(ref note, Name); }
            set { SetValue(ref note, value); }
        }
        public static NamedItem Random() {
            var x = new NamedItem();
            x.setRandomValues();
            return x;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            code = GetRandom.String(5, 10);
            name = GetRandom.String(5, 10);
            note = GetRandom.String(5, 10);
        }
    }
}
