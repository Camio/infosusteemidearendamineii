﻿using System;
using System.Diagnostics;
using Open.Aids;
namespace Open.Archetypes.BaseClasses {
    public abstract class Archetype : Common {
        protected bool isChanged;
        protected internal bool isReadOnly;
        public event EventHandler<ValueChangedEventArgs> OnChanged;
        public void SetValue<T>(ref T variable, T value) {
            if (isReadOnly) return;
            if (IsNull(value)) return;
            if (value.Equals(variable)) return;
            var old = variable;
            variable = value;
            if (old == null) return;
            if (old.Equals(default(T))) return;
            doOnChanged(old, value);
        }
        public void ChangeIfUnvalued<T>(ref T variable, T def, T value) {
            if (IsNull(value)) return;
            if (value.Equals(def)) return;
            if (!IsNull(variable) && !variable.Equals(def)) return;
            SetValue(ref variable, value);
        }
        private void doOnChanged(ValueChangedEventArgs args) {
            isChanged = true;
            OnChanged?.Invoke(this, args);
        }
        protected void doOnChanged<T>(T oldValue, T newValue) {
            var e = new ValueChangedEventArgs {
                MethodName = CallingMethod(),
                OldValue = oldValue,
                NewValue = newValue
            };
            doOnChanged(e);
        }
        protected void doOnChanged<T>(T newValue) {
            var e = new ValueChangedEventArgs {MethodName = CallingMethod(), NewValue = newValue};
            doOnChanged(e);
        }
        protected void doOnChanged<T>(int index, T newValue) {
            var e = new ValueChangedEventArgs {
                MethodName = CallingMethod(),
                Index = index,
                NewValue = newValue
            };
            doOnChanged( e);
        }
        protected void doOnChanged<T>(int index, T oldValue, T newValue) {
            var e = new ValueChangedEventArgs {
                MethodName = CallingMethod(),
                Index = index,
                OldValue = oldValue,
                NewValue = newValue
            };
            doOnChanged( e);
        }
        protected void doOnChanged(int index) {
            var e = new ValueChangedEventArgs {MethodName = CallingMethod(), Index = index};
            doOnChanged(e);
        }
        protected void doOnChanged() {
            var e = new ValueChangedEventArgs {MethodName = CallingMethod()};
            doOnChanged(e);
        }
        public T SetDefault<T>(ref T variable, T value) {
            if (IsNull(variable)) SetValue(ref variable, value);
            return variable;
        }
        public T SetDefault<T>(ref T variable) where T : new() {
            if (IsNull(variable)) SetValue(ref variable, new T());
            return variable;
        }
        public string SetDefault(ref string variable) {
            if (IsNull(variable)) SetValue(ref variable, string.Empty);
            return variable;
        }
        protected virtual void setRandomValues() { }
        public static string CallingMethod() {
            return Safe.Run(() => {
                var stack = new StackTrace();
                var frames = stack.GetFrames();
                int i;
                for (i = 0; i < stack.FrameCount; i++) {
                    var f = frames[i];
                    var m = f.GetMethod();
                    var n = m.Name;
                    if (n == "CallingMethod") break;
                }
                return frames[i + 2].GetMethod().Name;
            }, string.Empty);
        }
    }
}
