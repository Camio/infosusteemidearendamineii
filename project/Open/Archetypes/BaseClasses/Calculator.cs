﻿using System.Collections.Generic;
using Open.Aids;
using Open.Archetypes.RuleClasses;
namespace Open.Archetypes.BaseClasses {
    public class Calculator {
        private readonly Stack<object> stack;
        public Calculator() { stack = new Stack<object>(); }
        public void AddDays() {
            var y = Get();
            Set(ObjectMath.AddDays(Get(), y));
        }
        public void AddHours() {
            var y = Get();
            Set(ObjectMath.AddHours(Get(), y));
        }
        public void AddMinutes() {
            var y = Get();
            Set(ObjectMath.AddMinutes(Get(), y));
        }
        public void AddMonths() {
            var y = Get();
            Set(ObjectMath.AddMonths(Get(), y));
        }
        public void AddSeconds() {
            var y = Get();
            Set(ObjectMath.AddSeconds(Get(), y));
        }
        public void Add() {
            var y = Get();
            Set(ObjectMath.Add(Get(), y));
        }
        public void AddYears() {
            var y = Get();
            Set(ObjectMath.AddYears(Get(), y));
        }
        public void Age() { Set(ObjectMath.GetAge(Get())); }
        public void And() {
            var y = Get();
            Set(ObjectMath.And(Get(), y));
        }
        public void Clear() { stack.Clear(); }
        public void Contains() {
            var y = Get();
            Set(ObjectMath.Contains(Get(), y));
        }
        public void Days() { Set(ObjectMath.GetDays(Get())); }
        public void Divide() {
            var y = Get();
            Set(ObjectMath.Divide(Get(), y));
        }
        public void Dummy() { }
        public void EndsWith() {
            var y = Get();
            Set(ObjectMath.EndsWith(Get(), y));
        }
        public void Equal() {
            var y = Get();
            Set(ObjectMath.IsEqual(Get(),y));
        }
        public object Get() {
            try { return stack.Pop(); } catch {
                return null;
            }
        }
        public void Greater() {
            var y = Get();
            Set(ObjectMath.IsGreater(Get(), y));
        }
        public void Hours() { Set(ObjectMath.GetHours(Get())); }
        public void Interval() {
            var y = Get();
            Set(ObjectMath.GetInterval(Get(), y));
        }
        public void Inverse() { Set(ObjectMath.Inverse(Get())); }
        public void Length() { Set(ObjectMath.GetLength(Get())); }
        public void Less() {
            var y = Get();
            Set(ObjectMath.IsLess(Get(), y));
        }
        public void Minutes() { Set(ObjectMath.GetMinutes(Get())); }
        public void Months() { Set(ObjectMath.GetMonths(Get())); }
        public void Multiply() {
            var y = Get();
            Set(ObjectMath.Multiply(Get(), y));
        }
        public void Not() { Set(ObjectMath.Not(Get())); }
        public void Or() {
            var y = Get();
            Set(ObjectMath.Or(Get(), y));
        }
        public object Peek() {
            try { return stack.Peek(); } catch {
                return null;
            }
        }
        public void Perform(Operation o) {
            switch (o) {
                case Operation.And:And();return;
                case Operation.Or:Or();return;
                case Operation.Xor:Xor();return;
                case Operation.Not:Not();return;
                case Operation.Equal:Equal();return;
                case Operation.Greater:Greater();return;
                case Operation.Less:Less();return;
                case Operation.Month:Months();return;
                case Operation.Year:Year();return;
                case Operation.Interval:Interval();return;
                case Operation.Seconds:Seconds();return;
                case Operation.Minutes:Minutes();return;
                case Operation.Hours:Hours();return;
                case Operation.Days:Days();return;
                case Operation.AddSeconds:AddSeconds();return;
                case Operation.AddMinutes:AddMinutes();return;
                case Operation.AddHours:AddHours();return;
                case Operation.AddDays:AddDays();return;
                case Operation.AddMonths:AddMonths();return;
                case Operation.AddYears:AddYears();return;
                case Operation.Age:Age();return;
                case Operation.Length:Length();return;
                case Operation.ToUpper:ToUpper();return;
                case Operation.ToLower:ToLower();return;
                case Operation.Trim:Trim();return;
                case Operation.Substring:Substring();return;
                case Operation.Contains:Contains();return;
                case Operation.EndsWith:EndsWith();return;
                case Operation.StartsWith:StartsWith();return;
                case Operation.Add:Add();return;
                case Operation.Subtract:Subtract();return;
                case Operation.Multiply:Multiply();return;
                case Operation.Divide:Divide();return;
                case Operation.Power:Power();return;
                case Operation.Inverse:Inverse();return;
                case Operation.Reciprocal:Reciprocal();return;
                case Operation.Square:Square();return;
                case Operation.Sqrt:Sqrt();return;
                default:Dummy();return;
            }
        }
        public void Power() {
            var y = Get();
            Set(ObjectMath.Power(Get(), y));
        }
        public void Reciprocal() { Set(ObjectMath.Reciprocal(Get())); }
        public void Seconds() { Set(ObjectMath.GetSeconds(Get())); }
        public void Set(object x) { stack.Push(x); }
        public void Sqrt() { Set(ObjectMath.Sqrt(Get())); }
        public void Square() { Set(ObjectMath.Square(Get())); }
        public void StartsWith() {
            var y = Get();
            Set(ObjectMath.StartsWith(Get(), y));
        }
        public void Substring() {
            var y = Get();
            var x = Get();
            Set(ObjectValue.IsString(x) ? ObjectMath.Substring(x, y) : ObjectMath.Substring(Get(), x, y));
        }
        public void Subtract() {
            var y = Get();
            Set(ObjectMath.Subtract(Get(), y));
        }
        public void ToLower() { Set(ObjectMath.ToLower(Get())); }
        public void ToUpper() { Set(ObjectMath.ToUpper(Get())); }
        public void Trim() { Set(ObjectMath.Trim(Get())); }
        public void Xor() {
            var y = Get();
            Set(ObjectMath.Xor(Get(), y));
        }
        public void Year() { Set(ObjectMath.GetYears(Get())); }
    }
}
