﻿using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.ContactClasses {
    public class Country: NamedItem {
        public new static Country Random() {
            var x = new Country();
            x.setRandomValues();
            return x;
        }
    }
}
