﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.ContactClasses
{
    public class Registration: TemporalEntity {
        private string equipmentId;
        private string addressId;
        public string PhoneId {
            get { return SetDefault(ref equipmentId); }
            set { SetValue(ref equipmentId, value);}
        }
        public string AddressId {
            get { return SetDefault(ref addressId); }
            set { SetValue(ref addressId, value);}
        }
        public Phone Phone => Phones.Find(PhoneId);
        public Address Address => Addresses.Find(AddressId);
        public static Registration Random() {
            var r = new Registration();
            r.setRandomValues();
            return r;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            addressId = GetRandom.String();
            equipmentId = GetRandom.String();
        }
    }
}
