﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.ContactClasses {
    public class Registrations : Archetypes<Registration> {
        public static Registrations Instance { get; } = new Registrations();
        public static Phones GetPhones(string addressId) {
            var a = new Phones();
            var l = Instance.FindAll(x => x.AddressId == addressId);
            foreach (var e in l) a.Add(e.Phone);
            return a;
        }
        public static Addresses GetAddresses(string telecomId) {
            var a = new Addresses();
            var l = Instance.FindAll(x => x.PhoneId == telecomId);
            foreach (var e in l) a.Add(e.Address);
            return a;
        }
        public static Registrations Random() {
            var l = new Registrations();
            for (var i = 0; i < GetRandom.Count(); i++)
                l.Add(Registration.Random());
            return l;
        }
    }
}
