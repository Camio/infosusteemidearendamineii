﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.ContactClasses {
    public class AddressLines : Strings {
        public new static AddressLines Random() {
            var x = new AddressLines();
            var c = GetRandom.Count(1, 2);
            for (var i = 0; i < c; i++) x.Add(GetRandom.String(5, 7));
            return x;
        }
    }
}
