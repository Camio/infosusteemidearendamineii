﻿using Open.Aids;
namespace Open.Archetypes.ContactClasses {
    public class Address : Contact {
        private AddressLines addressLines;
        private string location;
        private string region;
        private string zipCode;
        private Country country;
        public AddressLines AddressLines {
            get { return SetDefault(ref addressLines); }
            set { SetValue(ref addressLines, value); }
        }
        public string Location {
            get { return SetDefault(ref location); }
            set { SetValue(ref location, value); }
        }
        public string Region {
            get { return SetDefault(ref region); }
            set { SetValue(ref region, value); }
        }
        public string ZipCode {
            get { return SetDefault(ref zipCode); }
            set { SetValue(ref zipCode, value); }
        }
        public Country Country {
            get { return SetDefault(ref country); }
            set { SetValue(ref country, value); }
        }
        public Phones RegisteredEquipments => Phones.GetEquipment(UniqueId);
        public new static Address Random() {
            var x = new Address();
            x.setRandomValues();
            return x;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            addressLines = AddressLines.Random();
            location = GetRandom.String(5, 7);
            region = GetRandom.String(4, 5);
            zipCode = GetRandom.UInt8().ToString();
            country = Country.Random();
        }
        public override string Content => $"{AddressLines.Content} {ZipCode} {Location} {Region} {Country.Name}".Trim();
    }
}
