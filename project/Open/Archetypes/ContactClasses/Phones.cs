﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.ContactClasses {
    public class Phones : Archetypes<Phone> {
        public static Phones Instance { get; } = new Phones();

        public static Phones GetEquipment(string addressId)
        {
            return Registrations.GetPhones(addressId);
        }
        public static Phones Random() {
            var x = new Phones();
            var c = GetRandom.Count();
            for (var i = 0; i < c; i++) x.Add(Phone.Random());
            return x;
        }
        public static Phone Find(string uniqueId) {
            return Instance.Find(x => x.IsThisUniqueId(uniqueId));
        }
    }
}
