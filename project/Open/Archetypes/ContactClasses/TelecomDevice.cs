﻿namespace Open.Archetypes.ContactClasses {
    public enum TelecomDevice {
        Other,
        Phone,
        Mobile,
        Fax,
        Pager
    }
}
