﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.ContactClasses {
    public class Addresses : Archetypes<Address> {
        public static Addresses Instance { get; } = new Addresses();
        public static Addresses Random() {
            {
                var x = new Addresses();
                var c = GetRandom.Count(3, 5);
                for (var i = 0; i < c; i++) x.Add(Address.Random());
                return x;
            }
        }
        public static Address Find(string uniqueId) {
            return Instance.Find(x => x.IsThisUniqueId(uniqueId));
        }
        public static Addresses GetRegistrations(string telecomId) {
            return Registrations.GetAddresses(telecomId);
        }
    }
}
