﻿using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.ContactClasses
{
    public class Countries: Archetypes<Country> {
        public static Countries Instance { get; } = new Countries();

        public static Country Add(string id, string name, string symbol, 
            string description, string officialName, string threeCharacterCode, int numericCode)
        {
            var c = Instance.Find(x=> x.UniqueId == id);
            if (c != null) return c;
            var i = new IsoCountry {
                Name = name,
                UniqueId = id,
                OfficialName = officialName,
                NumericCode = numericCode,
                CharacterCode = threeCharacterCode,
                Description = description,
                Symbol = symbol
            };
            Instance.Add(i);
            return i;
        }
    }
}
