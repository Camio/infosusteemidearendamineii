﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.ContactClasses {
    [KnownType(typeof(Phone))]
    [KnownType(typeof(Address))]
    [KnownType(typeof(WebAddress))]
    [KnownType(typeof(EmailAddress))]
    [XmlInclude(typeof(Phone))]
    [XmlInclude(typeof(Address))]
    [XmlInclude(typeof(WebAddress))]
    [XmlInclude(typeof(EmailAddress))]
    public class AddressUsage : UniqueEntity {
        private AddressUses uses;
        private Contact address;
        public Contact Address {
            get { return SetDefault(ref address); }
            set { SetValue(ref address, value); }
        }
        public AddressUses Uses {
            get { return SetDefault(ref uses); }
            set { SetValue(ref uses, value); }
        }
        public static AddressUsage Random() {
            var x = new AddressUsage();
            x.setRandomValues();
            return x;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            uses = AddressUses.Random();
            address = Contact.randomInherited();
        }
    }
}
