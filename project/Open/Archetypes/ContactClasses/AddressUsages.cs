﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.ContactClasses {
    public class AddressUsages : Archetypes<AddressUsage> {
        public static AddressUsages Instance { get; } = new AddressUsages();
        public static AddressUsages Random(byte min= 3, byte max = 10) {
            var r = new AddressUsages();
            var c = GetRandom.Count(min, max);
            for (var i = 0; i < c; i++)
                r.Add(AddressUsage.Random());
            return r;
        }
    }
}
