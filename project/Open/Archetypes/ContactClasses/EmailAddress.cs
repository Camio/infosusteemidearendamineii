﻿
using Open.Aids;
namespace Open.Archetypes.ContactClasses {
    public class EmailAddress : Contact {
        private string address;
        public string Address {
            get { return SetDefault(ref address); }
            set { SetValue(ref address, value); }
        }
        public new static EmailAddress Random() {
            var x = new EmailAddress();
            x.setRandomValues();
            return x;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            address = GetRandom.String(3,5);
            address += '.';
            address += GetRandom.String(3, 5);
            address += '@';
            address += GetRandom.String(3, 5);
            address += '.';
            address += GetRandom.String(3, 3);
        }
        public override string Content => Address;
    }
}
