﻿using Open.Aids;
namespace Open.Archetypes.ContactClasses {
    public class IsoCountry : Country {
        private string officialName;
        private string characterCode;
        private int numericCode;
        public string OfficialName {
            get { return SetDefault(ref officialName); }
            set { SetValue(ref officialName, value); }
        }
        public string CharacterCode {
            get { return SetDefault(ref characterCode); }
            set { SetValue(ref characterCode, value); }
        }
        public int NumericCode {
            get { return SetDefault(ref numericCode); }
            set { SetValue(ref numericCode, value); }
        }
        public new static IsoCountry Random() {
            var c = new IsoCountry();
            c.setRandomValues();
            return c;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            officialName = GetRandom.String();
            characterCode = GetRandom.String();
            numericCode = GetRandom.Int32(0);
        }
    }
}
