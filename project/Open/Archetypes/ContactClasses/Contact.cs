﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.ContactClasses {
    public class Contact: UniqueEntity {
        internal static Contact randomInherited() {
            var i = GetRandom.UInt32();
            var c = i%4;
            if (c == 0) return Address.Random();
            if (c == 1) return WebAddress.Random();
            if (c == 2) return EmailAddress.Random();
            return Phone.Random();
        }
        public static Contact Random() {
            var r = new Contact();
            r.setRandomValues();
            return r;
        }
        public virtual string Content => string.Empty;
    }
}
