﻿using Open.Aids;
namespace Open.Archetypes.ContactClasses {
    public class Phone : Contact {
        private uint countryCode;
        private uint directDialingPrefix;
        private uint areaCode;
        private uint number;
        private uint extension;
        private TelecomDevice device;
        public override string Content =>
            $"+{CountryCode} ({DirectDialingPrefix}){AreaCode} {Number} ext.{Extension}";
        public uint CountryCode {
            get { return SetDefault(ref countryCode); }
            set { SetValue(ref countryCode, value); }
        }
        public uint DirectDialingPrefix {
            get { return SetDefault(ref directDialingPrefix); }
            set { SetValue(ref directDialingPrefix, value); }
        }
        public uint AreaCode {
            get { return SetDefault(ref areaCode); }
            set { SetValue(ref areaCode, value); }
        }
        public uint Number {
            get { return SetDefault(ref number); }
            set { SetValue(ref number, value); }
        }
        public uint Extension {
            get { return SetDefault(ref extension); }
            set { SetValue(ref extension, value); }
        }
        public TelecomDevice Device {
            get { return SetDefault(ref device, TelecomDevice.Other); }
            set { SetValue(ref device, value); }
        }
        public Addresses RegisteredIn => Addresses.GetRegistrations(UniqueId);
        public new static Phone Random() {
            var x = new Phone();
            x.setRandomValues();
            return x;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            countryCode = GetRandom.UInt32(111, 999);
            directDialingPrefix = GetRandom.UInt32(0,9);
            areaCode = GetRandom.UInt32(11, 99);
            number = GetRandom.UInt32(1111111, 9999999);
            extension = GetRandom.UInt32(111, 999);
            device = GetRandom.Enum<TelecomDevice>();
        }
    }
}
