﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.ContactClasses {
    public class AddressUses : Strings {
        public new static AddressUses Random() {
            var r = new AddressUses();
            var c = GetRandom.Count(1, 2);
            for (var i = 0; i < c; i++)
                r.Add(GetRandom.String(5, 7));
            return r;
        }
    }
}
