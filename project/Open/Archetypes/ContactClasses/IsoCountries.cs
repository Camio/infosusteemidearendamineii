﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
namespace Open.Archetypes.ContactClasses
{
    public static class IsoCountries
    {
        public static List<RegionInfo> Get() {
            var l = new List<RegionInfo>();
            foreach (
                var country in
                    CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                        .Select(i => new RegionInfo(i.LCID))
                        .Where(country => l.Count(p => p.Name == country.Name) == 0)) {
                l.Add(country);
            }
            return l.OrderBy(p => p.EnglishName).ToList();
        }
        public static void Register() {
            var countries = Get();
            foreach (var c in countries) {
                var id = c.ThreeLetterISORegionName;
                var threeCharacterCode = c.ThreeLetterISORegionName;
                var name = c.EnglishName;
                var description = c.NativeName;
                var officialName = c.DisplayName;
                var code = c.TwoLetterISORegionName;
                var numericCode = c.GeoId;
                numericCode = fixNumericCodeForBotswana(numericCode, id);
                Countries.Add(id, name, code, description,
                   officialName, threeCharacterCode, numericCode);
            }
        }

        private static int fixNumericCodeForBotswana(int numericCode, string id) {
            return id == "BWA" ? 999 : numericCode;
        }
    }
}
