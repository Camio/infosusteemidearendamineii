﻿
using Open.Aids;
namespace Open.Archetypes.ContactClasses {
    public class WebAddress : Contact {
        private string url;
        public string Url {
            get { return SetDefault(ref url, string.Empty); }
            set { SetValue(ref url, value); }
        }
        public new static WebAddress Random() {
            var x = new WebAddress();
            x.setRandomValues();
            return x;
        }
        protected override void setRandomValues() {
            base.setRandomValues();
            url = "https://www."; 
            url += GetRandom.String(3, 5);
            url += '@';
            url += GetRandom.String(3, 5);
            url += '.';
            url += GetRandom.String(3, 3);
        }
        public override string Content => Url;
    }
}
