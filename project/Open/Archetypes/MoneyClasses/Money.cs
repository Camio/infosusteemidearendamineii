﻿using Open.Archetypes.QuantityClasses;
namespace Open.Archetypes.MoneyClasses {
    public sealed class Money : CommonMoney {
        public static Money Random() {
            var m = new Money();
            m.setRandomValues();
            return m;
        }
        public Money Round(Rounding policy)
        {
            var d = QuantityClasses.Round.Off(Amount, policy);
            var m = new Money
            {
                date = Date,
                currency = Currency,
                amount = d
            };
            return m;
        }
        public Money ConvertTo(Currency c)
        {
            var d = ConvertTo(Amount, c);
            var m = new Money
            {
                date = Date,
                currency = c,
                amount = d
            };
            return m;
        }
        public CommonMoney Add(CommonMoney v)
        {
            if (ReferenceEquals(null, v)) return Empty;
            var d = Currency.ToNominal(Amount, Date);
            d = d + v.Currency.ToNominal(v.Amount, v.Date);
            var m = new Money
            {
                date = v.Date,
                currency = v.Currency,
                amount = v.Currency.FromNominal(d, Date)
            };
            return m;
        }
        public CommonMoney Subtract(CommonMoney v)
        {
            if (ReferenceEquals(null, v)) return Empty;
            var d = Currency.ToNominal(Amount, Date);
            d = d - v.Currency.ToNominal(v.Amount, v.Date);
            var m = new Money
            {
                date = v.Date,
                currency = v.Currency,
                amount = v.Currency.FromNominal(d, Date)
            };
            return m;
        }
        public CommonMoney Multiply(decimal d)
        {
            var m = new Money
            {
                date = Date,
                currency = Currency,
                amount = Amount * d
            };
            return m;
        }
        public CommonMoney Divide(decimal d)
        {
            var m = new Money {
                date = Date,
                currency = Currency,
                amount = d == decimal.Zero ? d : Amount/d
            };
            return m;
        }
        public override bool IsEmpty() {
            return Equals(Empty) || Equals(new Money());
        }

    }
}
