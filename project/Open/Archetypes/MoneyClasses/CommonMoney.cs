﻿using System;
using Open.Aids;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.MoneyClasses {
    public abstract class CommonMoney : Value, IComparable {
        public const string DefaultFormat = "{0} {1}";
        protected decimal amount;
        protected Currency currency;
        protected DateTime date;
        public decimal Amount {
            get { return amount; }
            set { SetValue(ref amount, value); }
        }
        public Currency Currency {
            get { return currency; }
            set { SetValue(ref currency, value); }
        }
        public DateTime Date {
            get { return date; }
            set { SetValue(ref date, value); }
        }
        protected override void setRandomValues()
        {
            base.setRandomValues();
            amount = GetRandom.UInt32();
            currency = Currency.Random();
            date = GetRandom.DateTime();
        }
        public static CommonMoney Empty => new EmptyMoney();
        public override bool IsEmpty() { return Equals(Empty); }
        protected decimal ConvertTo(decimal d, Currency c) {
            if (ReferenceEquals(null, c)) return decimal.Zero;
            d = currency.ToNominal(d, date);
            return c.FromNominal(d, date);
        }
        public int CompareTo(CommonMoney v) {
            if (ReferenceEquals(null, v)) return int.MinValue;
            var c1 = Currency.ToNominal(Amount, date);
            var c2 = v.Currency.ToNominal(v.Amount, v.date);
            var r = c1.CompareTo(c2);
            return r;
        }
        public int CompareTo(object o) { return CompareTo(o as CommonMoney); }
        public decimal Divide(CommonMoney v) {
            if (ReferenceEquals(null, v)) return decimal.Zero;
            var a1 = Currency.ToNominal(amount, date);
            var a2 = v.Currency.ToNominal(v.amount, v.date);
            if (a2 == decimal.Zero) return decimal.Zero;
            return a1/a2;
        }
    }
}
