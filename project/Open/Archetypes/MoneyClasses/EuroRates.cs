﻿using System;
using System.IO;
using System.Xml;
using Open.Aids;
namespace Open.Archetypes.MoneyClasses {
    public static class EuroRates {
        internal const string example =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
            "<gesmes:Envelope xmlns=\"http://www.ecb.int/vocabulary/2002-08-01/eurofxref\" " +
            "xmlns:gesmes=\"http://www.gesmes.org/xml/2002-08-01\">" +
            "<gesmes:subject>Reference rates</gesmes:subject><gesmes:Sender>" +
            "<gesmes:name>European Central Bank</gesmes:name></gesmes:Sender>" +
            "<Cube><Cube time=\"2015-03-05\"><Cube rate=\"1.1069\" currency=\"USD\"/>" +
            "<Cube rate=\"133.10\" currency=\"JPY\"/><Cube rate=\"1.9558\" currency=\"BGN\"/>" +
            "<Cube rate=\"27.422\" currency=\"CZK\"/><Cube rate=\"7.4542\" currency=\"DKK\"/>" +
            "<Cube rate=\"0.72510\" currency=\"GBP\"/><Cube rate=\"305.36\" currency=\"HUF\"/>" +
            "<Cube rate=\"4.1397\" currency=\"PLN\"/><Cube rate=\"4.4453\" currency=\"RON\"/>" +
            "<Cube rate=\"9.2140\" currency=\"SEK\"/><Cube rate=\"1.0697\" currency=\"CHF\"/>" +
            "<Cube rate=\"8.5460\" currency=\"NOK\"/><Cube rate=\"7.6585\" currency=\"HRK\"/>" +
            "<Cube rate=\"67.6095\" currency=\"RUB\"/><Cube rate=\"2.8663\" currency=\"TRY\"/>" +
            "<Cube rate=\"1.4205\" currency=\"AUD\"/><Cube rate=\"3.3009\" currency=\"BRL\"/>" +
            "<Cube rate=\"1.3770\" currency=\"CAD\"/><Cube rate=\"6.9382\" currency=\"CNY\"/>" +
            "<Cube rate=\"8.5847\" currency=\"HKD\"/><Cube rate=\"14363.07\" currency=\"IDR\"/>" +
            "<Cube rate=\"4.4270\" currency=\"ILS\"/><Cube rate=\"68.9098\" currency=\"INR\"/>" +
            "<Cube rate=\"1218.84\" currency=\"KRW\"/><Cube rate=\"16.6566\" currency=\"MXN\"/>" +
            "<Cube rate=\"4.0416\" currency=\"MYR\"/><Cube rate=\"1.4777\" currency=\"NZD\"/>" +
            "<Cube rate=\"48.848\" currency=\"PHP\"/><Cube rate=\"1.5156\" currency=\"SGD\"/>" +
            "<Cube rate=\"35.885\" currency=\"THB\"/><Cube rate=\"13.0163\" currency=\"ZAR\"/>" +
            "</Cube></Cube></gesmes:Envelope>";
        private const string historyUrl =
            "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist.xml";
        private const string dailyUrl =
            "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
        private const string cubeString = "Cube";
        private const string timeString = "time";
        private const string rateString = "rate";
        private const string currencyString = "currency";
        public static void LoadHistory(bool fromEcb = false) {
            var s = Utils.IsTesting(fromEcb) ? example : WebService.Load(historyUrl);
            ReadXml(s);
        }
        public static void LoadDaily(bool fromEcb = false) {
            var s = Utils.IsTesting(fromEcb) ? example : WebService.Load(dailyUrl);
            ReadXml(s);
        }
        public static void ReadXml(string s) {
            var doc = toXmlDocument(s);
            foreach (XmlNode node in doc.ChildNodes) {
                foreach (XmlNode cube in node.ChildNodes) {
                    if (cube.Name != cubeString) continue;
                    foreach (XmlNode time in cube.ChildNodes) {
                        var date = getDate(time);
                        foreach (XmlNode rate in time.ChildNodes) {
                            decimal value;
                            if (!getRate(rate, out value)) continue;
                            var currency = getCurrency(rate);
                            ExchangeRates.Add(currency, value, date);
                        }
                    }
                }
            }
        }
        private static Currency getCurrency(XmlNode node) {
            var v = getValue(node, currencyString);
            return Currencies.Add(v);
        }
        private static DateTime getDate(XmlNode node) {
            var date = getValue(node, timeString);
            return DateTime.ParseExact(date, "yyyy-MM-dd", UseCulture.Invariant);
        }
        private static bool getRate(XmlNode node, out decimal rate) {
            var v = getValue(node, rateString);
            return DecimalValue.TryParse(v, out rate);
        }
        private static XmlDocument toXmlDocument(string s) {
            var stream = new StringReader(s);
            var r = XmlReader.Create(stream);
            var d = new XmlDocument();
            d.Load(r);
            return d;
        }
        internal static string getValue(XmlNode n, string name) {
            return Safe.Run(() => {
                var a = n.Attributes[name];
                return a.Value;
            }, string.Empty);
        }
    }
}
