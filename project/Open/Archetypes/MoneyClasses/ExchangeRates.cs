﻿using System;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.MoneyClasses {
    public class ExchangeRates : Archetypes<ExchangeRate> {
        public static ExchangeRates Instance { get; } = new ExchangeRates();
        public static ExchangeRate Find(string id) {
            return Instance.Find(x => x.IsThis(id));
        }
        public static void Add(Currency c, decimal rate = 1.0M, DateTime? d = null)
        {
            var e = new ExchangeRate(c, rate, d);
            Instance.Add(e);
        }
    }
}
