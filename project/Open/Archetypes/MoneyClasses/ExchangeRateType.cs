﻿using Open.Archetypes.BaseClasses;
using Open.Archetypes.RuleClasses;
namespace Open.Archetypes.MoneyClasses {
    public class ExchangeRateType: NamedItem {
        private string exchangeRate;
        private string rule;
        public string ExchangeRate {
            get { return SetDefault(ref exchangeRate); }
            set { SetValue(ref exchangeRate, value);}
        }
        public string Rule
        {
            get { return SetDefault(ref rule); }
            set { SetValue(ref rule, value); }
        }
        public Rule GetRule => Rules.Find(Rule);
        public ExchangeRate GetExchengeRate => ExchangeRates.Find(ExchangeRate);
        public new static ExchangeRateType Empty { get; } = new ExchangeRateType(); 
        public override bool IsEmpty() { return Equals(Empty); }
    }
}
