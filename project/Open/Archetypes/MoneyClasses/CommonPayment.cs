﻿
using System;
namespace Open.Archetypes.MoneyClasses
{
    public abstract class CommonPayment: CommonMoney
    {
        public PaymentMethod PaymentMethod { get; internal set; }
        public DateTime Made { get; internal set; }
        public DateTime Received { get; internal set; }
        public DateTime Due { get; internal set; }
        public DateTime Cleared { get; internal set; }
    }
}
