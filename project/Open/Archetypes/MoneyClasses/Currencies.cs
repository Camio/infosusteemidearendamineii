﻿using Open.Aids;
using Open.Archetypes.BaseClasses;
using Open.Archetypes.ContactClasses;
namespace Open.Archetypes.MoneyClasses {
    public class Currencies : Archetypes<Currency> {
        public static Currencies Instance { get; } = new Currencies();
        public static Currencies Random() {
            var l = new Currencies();
            var c = GetRandom.UInt8(5, 20);
            for (var i = 0; i < c; i++) {
                var item = Currency.Random();
                l.Add(item);
            }
            return l;
        }
        public static Currency Add(string name = null, string symbol = null) {
            if (IsSpaces(name)) return Currency.Empty; 
            var c = Instance.Find(x => x.IsThis(name));
            if (! IsNull(c))return c;
            symbol = symbol ?? name;
            c = new Currency(name, symbol);
            Instance.Add(c);
            return c;
        }
        public static void Register()
        {
            foreach (var c in IsoCountries.Get()) Add(c.CurrencyEnglishName, c.ISOCurrencySymbol);
        }
    }
}

//private static readonly object syncRoot = new object();
//private static readonly object lockRoot = new object();
//private static volatile CurrenciesBook instance;
//private static readonly CurrencyRates rates = new CurrencyRates();
//public Currencies Currencies { get { return Items; } }
//private CurrenciesBook() { SetBook( new Currencies()); }
//public static CurrenciesBook Instance {
//    get {
//        if (instance != null) return instance;
//        lock (lockRoot) {
//            if (instance == null)
//                instance = new CurrenciesBook();
//        }
//        return instance;
//    }
//}
//public ReadOnlyDictionary<string, ReadOnlyDictionary<string, decimal>> NominalRates { get { return rates.ReadOnly(); } }
//public ExchangeRates ExchangeRates { get { throw new NotImplementedException(); } }
//public ExchangeRateTypes ExchangeRateTypes { get { throw new NotImplementedException(); } }
//public decimal GetNominalRate(Currency currency, DateTime dt)
//{
//    if (ReferenceEquals(null, currency)) return decimal.Zero;
//    if (currency.Code == "EUR") return decimal.One;
//    Dictionary<string, decimal> d;
//    if (!rates.TryGetValue(currency.Code, out d)) return decimal.Zero;
//    var k = DateTimeValue.ToLongDateString(dt);
//    decimal rate;
//    if (d.TryGetValue(k, out rate)) return rate;
//    var l = d.Keys.ToList();
//    if (l.Count == 0) return decimal.Zero;
//    l.Add(k);
//    l.Sort();
//    var i = l.IndexOf(k);
//    if (i == 0) k = l[1];
//    else if (i == l.Count - 1) k = l[i - 1];
//    else {
//        var k1 = l[i - 1];
//        var k2 = l[i + 1];
//        var dt1 = DateTime.ParseExact(k1, "yyyy-MM-dd", CultureInfoFacade.Invariant);
//        var dt2 = DateTime.ParseExact(k2, "yyyy-MM-dd", CultureInfoFacade.Invariant);
//        var r1 = DateTimeMath.Subtract(dt, dt1);
//        var r2 = DateTimeMath.Subtract(dt2, dt);
//        k = DateTimeMath.IsLess(r1, r2) ? k1 : k2;
//    }
//    if (d.TryGetValue(k, out rate)) return rate;
//    return decimal.Zero;
//}
//public void SetNominalRate(Currency currency, DateTime dt, decimal rate) {
//    lock (syncRoot) {
//        if (ReferenceEquals(null, currency)) return;
//        Dictionary<string, decimal> d;
//        if (!rates.TryGetValue(currency.Name, out d)) {
//            d = new Dictionary<string, decimal>();
//            rates.Add(currency.Name, d);
//        }
//        var k = DateTimeValue.ToLongDateString(dt);
//        if (d.ContainsKey(k)) d.Remove(k);
//        d.Add(k, rate);
//    }
//}
//protected override Currencies CreateItems(Archetypes<Currency> elements) {
//    var l =  new Currencies();
//    l.AddRange(elements);
//    return l;
//}
//protected override void DoClear() {
//    base.DoClear();
//    rates.Clear();
//}
//protected override Currencies CreateEmptySet() {
//    return new Currencies();
//}
//public Currency Change(string id, string name = null, string code = null,
//    string description = null, DateTimeInterval valid = null, DateTime? lastModified = null,
//    string modifiedBy = null, bool isReadOnly = false) {
//    var u = new Currency {
//        Name = name,
//        Code = code,
//        Description = description,
//    };
//    return Change(u);
//}
//}


