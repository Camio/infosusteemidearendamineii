﻿using Open.Archetypes.BaseClasses;

namespace Open.Archetypes.MoneyClasses
{
    public class ExchangeRateTypes: Archetypes<ExchangeRateType> {
        public static ExchangeRateTypes Instance { get; } = new ExchangeRateTypes();
    }
}
