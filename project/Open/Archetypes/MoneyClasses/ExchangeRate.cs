﻿using System;
using Open.Archetypes.BaseClasses;
namespace Open.Archetypes.MoneyClasses
{
    public class ExchangeRate: UniqueEntity {
        private decimal rate;
        private string currency;
        public ExchangeRate() : this(null) { }
        public ExchangeRate(Currency c, decimal rate = 1M, DateTime? d = null ) {
            c = c ?? MoneyClasses.Currency.Empty;
            DateTime dt = d ?? DateTime.Now;
            UniqueId = dt.ToString("yyyy-MM-dd-") + c.UniqueId;
            Valid.From = dt.Date;
            Valid.To = dt.Date.AddSeconds(24*60*60-1);
            currency = c.UniqueId;
            this.rate = rate;
        }
        public override bool IsEmpty() { throw new NotImplementedException(); }
        public decimal Rate {
            get { return SetDefault(ref rate); }
            set { SetValue(ref rate, value);}
        }
        public string Currency
        {
            get { return SetDefault(ref currency); }
            set { SetValue(ref currency, value); }
        }
        public ExchangeRateTypes ExchangeRateTypes {
            get {
                var e = new ExchangeRateTypes();
                var l = ExchangeRateTypes.Instance.FindAll(x=> x.ExchangeRate == UniqueId);
                e.AddRange(l);
                return e;
            }
        }
        public bool IsThis(string id) {
            if (IsSpaces(id)) return false;
            if (UniqueId == id) return true;
            return Currency == id;
        }
    }
}
