﻿using System;
using Open.Archetypes.ContactClasses;
using Open.Archetypes.QuantityClasses;
namespace Open.Archetypes.MoneyClasses {
    public class Currency : Metric {
        public Currency() : this(null) {}
        public Currency(string name, string symbol= null) : base(symbol, symbol, name) { }
        public Country AcceptedIn { get { throw new NotImplementedException(); } }

        public bool IsIsoCurrency {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
        public decimal ToNominal(decimal d, DateTime dt) {
            throw new NotImplementedException();
            //var rate = CurrenciesBook.Instance.GetNominalRate(this, dt);
            //if (rate == decimal.Zero) return decimal.Zero;
            //return d/rate;
        }
        public decimal FromNominal(decimal d, DateTime dt) {
            throw new NotImplementedException();
            //var rate = CurrenciesBook.Instance.GetNominalRate(this, dt);
            //return d*rate;
        }
        public decimal ToLocal(decimal d, ExchangeRate er) {
            if (ReferenceEquals(null, er)) return ToNominal(d, DateTime.Now);
            var rate = er.Rate;
            if (rate == decimal.Zero) return decimal.Zero;
            return d/rate;
        }
        public decimal FromLocal(decimal d, ExchangeRate er) {
            if (ReferenceEquals(null, er)) return FromNominal(d, DateTime.Now);
            var rate = er.Rate;
            return d*rate;
        }
        public static Currency Random() {
            var c = new Currency();
            c.setRandomValues();
            return c;
        }
        public static Currency Empty { get; } = new Currency(); 
        public override bool IsEmpty() { return Equals(Empty); }
        public override string Formula(bool longFormula = false) { return longFormula ? Name : Symbol; }
    }
}
